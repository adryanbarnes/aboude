# Requirements
	

	PHP >= 7.1.3
	OpenSSL PHP Extension
	PDO PHP Extension
	Mbstring PHP Extension
	Tokenizer PHP Extension
	XML PHP Extension
	Ctype PHP Extension
	JSON PHP Extension
	MCrypt PHP Extension
	MySQL ver >= 5.7	
	
## How to install

#### Use Composer to install dependencies
##### Option 1: Composer is not installed globally

	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
	php composer-setup.php
	php -r "unlink('composer-setup.php');"
	
##### Composer is installed globally

	composer install

If you haven't already, you might want to make [composer be installed globally](https://getcomposer.org/doc/03-cli.md#global) for future ease of use.


## Getting Started

Open you comand line application tool and navigate to the directory in your web directory, **htdocs** or **www** you have the application cloned to. 
Run the following commands to install and initialize your this application:

	composer install


## Environment Configurations

#### .env file
Rename the ***.env.example*** in your root to ***.env***

#### .local.env file

In the ***.local.env*** file edit the ***DB_USERNAME*** and ***DB_PASSWORD*** to match your local database settings.


### Database 

#### Schema

Next you have to create a new database schema that is named ***aboude***.


#### Setup Database Tables
Run the following command in the same root directory of your application to create the various database tables needed:

	php artisan migrate

### Installing Packages
Run the following command in the same root directory of your application:

	yarn run dev

If you don't have **yarn** package manager installed on your localhost, please visit [here](https://yarnpkg.com/lang/en/docs/install/) for installation instruction.


## For 'Unix' Like Systems
### Make sure storage and bootstrap/cache is writable by your web server.

	Run the following command to set the permissions correctly:

	    sudo chgrp -R www-data storage bootstrap/cache

	    sudo chmod -R ug+rwx storage bootstrap/cache
		

	Should work, if not try

		chmod -R 775 app/storage

		    or

		chmod -R 777 app/storage

#### Create the Symbolic Link
Run the following command in the same root directory of your application to make files stored in storage/app/public accesible from the web.

	php artisan storage:link

Please visit [here](https://laravel.com/docs/5.4/filesystem#the-public-disk) for more information.