<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('supplier');
            $table->string('purchase_no');
            $table->date('purchase_date');
            $table->longText('description')->nullable();
            $table->integer('qty')->index()->unsigned();
            $table->decimal('unit_price', 15, 2);  
            $table->decimal('tax_amt', 15, 2);
            $table->decimal('total_amt', 15, 2);
            $table->decimal('sub_total', 15, 2);
            $table->decimal('total_purchase', 15, 2);            
            $table->integer('purchase_item_id')->index()->unsigned();
            $table->foreign('purchase_item_id')->references('id')->on('purchase_items')->ondelete('restrict'); 
            $table->integer('tax_term_id')->index()->unsigned();
            $table->foreign('tax_term_id')->references('id')->on('tax_terms')->ondelete('set null');  
            $table->integer('statuses_id')->index()->unsigned();
            $table->foreign('statuses_id')->references('id')->on('statuses')->ondelete('set null');                        
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
