<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('users', function (Blueprint $table) {
            $table->integer('auth_level_id')->index()->unsigned()->default(3)->nullable();
            $table->foreign('auth_level_id')->references('id')->on('auth_levels')->ondelete('set null'); 

            $table->boolean('active')->default(false);
            $table->string('phone_number', 13)->unique()->nullable()->after('password');
            $table->string('email')->nullable()->change();
            $table->renameColumn('name', 'other_name');
            $table->string('profile_picture')->nullable();
            $table->string('surname');
            $table->date('dob')->default(date('Y-m-d'));
            $table->enum('gender', ['M', 'F']);
            $table->integer('pincode')->unsigned()->nullable();   
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_auth_level_id_foreign');
            $table->renameColumn('other_name', 'name');
            $table->dropColumn(['auth_level_id', 'active', 'phone_number', 'pincode', 'profile_picture', 'surname', 'dob', 'gender', 'pincode']);   
        });


    }
}
