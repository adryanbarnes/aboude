<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->decimal('price', 15, 2);
            $table->integer('meal_categories_id')->index()->unsigned();
            $table->foreign('meal_categories_id')->references('id')->on('meal_categories')->ondelete('restrict'); 
            $table->integer('meal_tag_id')->index()->unsigned();
            $table->foreign('meal_tag_id')->references('id')->on('meal_tags')->ondelete('restrict');
            $table->integer('pack_size_id')->index()->unsigned()->nullable();
            $table->foreign('pack_size_id')->references('id')->on('pack_sizes')->ondelete('restrict'); 
            $table->boolean('available')->default(true);                                   
            $table->softDeletes();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_menus');
    }
}
