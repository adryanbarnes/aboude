<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoPrivilegesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('privileges')->insert( array(
              ['name'=>'View', 'auth_level' => 1],
              ['name'=>'Create', 'auth_level' => 2],
              ['name'=>'Edit', 'auth_level' => 3],
              ['name'=>'Process', 'auth_level' => 4],
              ['name'=>'Delete', 'auth_level' => 5]
         ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('privileges')->truncate();
    }
}
