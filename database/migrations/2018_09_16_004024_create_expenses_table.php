<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('comment')->nullable();
            $table->string('receipt')->nullable();
            $table->decimal('tax', 15, 2);
            $table->decimal('tax_paid', 15, 2);
            $table->date('expense_date', 15, 2);
            $table->integer('statuses_id')->index()->unsigned();
            $table->foreign('statuses_id')->references('id')->on('statuses')->ondelete('set null');  
            $table->integer('expense_categories_id')->index()->unsigned();
            $table->foreign('expense_categories_id')->references('id')->on('expense_categories')->ondelete('set null'); 
            $table->integer('user_id')->index()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->ondelete('set null');

            $table->softDeletes();                      
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
