<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_no')->index()->unsigned();
            $table->string('description');
            $table->integer('qty')->index()->unsigned();
            $table->decimal('unit_price', 15, 2);
            $table->decimal('total_amt', 15, 2);
            $table->integer('order_type_id')->index()->unsigned();
            $table->foreign('order_type_id')->references('id')->on('order_types')->ondelete('restrict'); 
            $table->integer('order_status_id')->index()->unsigned();
            $table->foreign('order_status_id')->references('id')->on('order_status')->ondelete('restrict');             
            $table->integer('user_id')->index()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->ondelete('restrict');
            $table->integer('branches_id')->index()->unsigned()->nullable();
            $table->foreign('branches_id')->references('id')->on('branches')->ondelete('restrict');
            $table->string('cancel_reason')->nullable();
            $table->boolean('delayed')->default(false);
            $table->time('delayed_start')->nullable();
            $table->time('delayed_end')->nullable();
            $table->boolean('refund')->default(false);                                     
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
