<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_terms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('term');
        });

        DB::table('tax_terms')->insert( array(             
              ['term'=>'Tax Exclusive'],
              ['term'=>'Tax Inclusive']
         ));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_terms');
    }
}
