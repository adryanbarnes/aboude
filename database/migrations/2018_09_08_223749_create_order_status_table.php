<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateOrderStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('color_indicator');
        });

        DB::table('order_status')->insert([
           ['name'=> 'Queued', 'color_indicator'=> '#4F67D1'],
           ['name'=> 'In Kitchen', 'color_indicator'=> '#6C7695'],
           ['name'=> 'Delivered', 'color_indicator'=> '#6EC7AD'],
           ['name'=> 'Delayed', 'color_indicator'=> '#4F67D1'],
           ['name'=> 'Cancelled', 'color_indicator'=> '#EE5C90'],
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_status');
    }
}
