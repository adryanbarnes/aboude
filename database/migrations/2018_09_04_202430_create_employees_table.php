<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->date('start_date');
            $table->decimal('salary', 15, 2);
            $table->enum('salary_type', ['Wages']);
            $table->integer('user_id')->index()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->ondelete('restrict');            
            $table->integer('job_role_id')->index()->unsigned()->nullable();
            $table->foreign('job_role_id')->references('id')->on('job_roles')->ondelete('set null');   

            $table->integer('branches_id')->index()->unsigned()->nullable();
            $table->foreign('branches_id')->references('id')->on('branches')->ondelete('restrict'); 
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
