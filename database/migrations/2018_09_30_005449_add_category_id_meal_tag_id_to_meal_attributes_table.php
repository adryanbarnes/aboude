<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryIdMealTagIdToMealAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_attributes', function (Blueprint $table) {
            $table->integer('meal_tag_id')->index()->unsigned()->after('name')->default(1);
            $table->foreign('meal_tag_id')->references('id')->on('meal_tags')->onupdate('cascade')->ondelete('restrict');  

            $table->integer('meal_categories_id')->index()->unsigned()->default(1)->after('name');
            $table->foreign('meal_categories_id')->references('id')->on('meal_categories')->ondelete('restrict'); 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_attributes', function (Blueprint $table) {
             $table->dropForeign(['meal_categories_id', 'meal_tag_id']);
             $table->dropColumn(['meal_categories_id', 'meal_tag_id']);
        });
    }
}
