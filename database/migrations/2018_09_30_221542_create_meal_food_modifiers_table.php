<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealFoodModifiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_food_modifiers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('meal_tag_id')->index()->unsigned();
            $table->foreign('meal_tag_id')->references('id')->on('meal_tags')->ondelete('set restrict'); 
            $table->integer('food_modifier_id')->index()->unsigned();
            $table->foreign('food_modifier_id')->references('id')->on('food_modifiers')->ondelete('set restrict');
            $table->decimal('price', 15, 2);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_food_modifiers');
    }
}
