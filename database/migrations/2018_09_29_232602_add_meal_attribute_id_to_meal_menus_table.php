<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMealAttributeIdToMealMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_menus', function (Blueprint $table) {
            $table->integer('meal_attributes_id')->index()->unsigned()->after('meal_tag_id')->nullable();
            $table->foreign('meal_attributes_id')->references('id')->on('meal_attributes')->ondelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_menus', function (Blueprint $table) {
             $table->dropForeign(['meal_attributes_id']);
             $table->dropColumn(['meal_attributes_id']);
        });
    }
}
