<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReplaceDescriptionWithMealClassificationOnOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('meal_menu_id')->index()->unsigned()->after('qty');
            $table->foreign('meal_menu_id')->references('id')->on('meal_menus')->ondelete('set restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['meal_menu_id']);
            $table->dropColumn(['meal_menu_id']);
        });
    }
}
