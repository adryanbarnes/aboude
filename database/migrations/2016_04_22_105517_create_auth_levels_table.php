<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
        });

        DB::table('auth_levels')->insert([
           ['id'=>1, 'name'=>'Viewer','description'=>'View-only priveleges'],
           ['id'=>2, 'name'=>'Editor','description'=>'Edit priveleges'],
           ['id'=>3, 'name'=>'Admin','description'=>'Administor priveleges'],
           ['id'=>4, 'name'=>'Systems Admin','description'=>'Will have control over all models of the application'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('auth_levels');
    }
}
