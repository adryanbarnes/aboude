<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Module;

class InsertIntoModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      $modules = [
        // ['name'=>'Users Management','description'=>'Manage users','route_segment'=>'users','type_id'=>1,'minimun_auth_level'=>1],
        ['name'=>'Administrator Management','description'=>'manage Account api integrations','route_segment'=>'admin','type_id'=>1,'minimun_auth_level'=>2],
        ['name'=>'Inventory Management','description'=>'module for creating disbursement transactions','route_segment'=>'inventory','type_id'=>1,'minimun_auth_level'=>1],
        ['name'=>'Staff Management','description'=>'module for creating disbursement transactions','route_segment'=>'staff','type_id'=>1,'minimun_auth_level'=>1],
        ['name'=>'Finance Management','description'=>'module for creating collection transactions','route_segment'=>'finance','type_id'=>1,'minimun_auth_level'=>1]        
        ];

        $privileges = [
            [1,2,3,5],
            [1,2,3,5],
            [1,2,3,5],
            [1,2,3,4,5],
            [1,2,3,4,5]
        ];


         DB::table('modules')->insert($modules);


         foreach (Module::all() as $module) {

            $module->privileges()->sync($privileges[$module->id - 1]);
             $columnName = 'module_'.$module->id;
             if (!Schema::hasColumn("users_modules_privileges", $columnName)) {

                Schema::table('users_modules_privileges', function($table) use ($columnName) {
                    $table->integer($columnName)->nullable();
                });
              }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        

        foreach (Module::all() as $module) {

            $columnName = 'module_'.$module->id;
            if (!Schema::hasColumn("users_modules_privileges", $columnName)) {

               Schema::table('users_modules_privileges', function($table) use ($columnName) {
                   $table->dropColumn($columnName)->nullable();
               });
            }
        }

        // DB::table('modules')->truncate();

    }
}
