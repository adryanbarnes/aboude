<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_no_id')->index()->unsigned();
            $table->foreign('order_no_id')->references('order_no')->on('orders')->ondelete('restrict'); 
            $table->integer('vat_number_id')->index()->unsigned();
            $table->foreign('vat_number_id')->references('id')->on('vat_numbers')->ondelete('restrict');                         
            $table->decimal('sub_total', 15, 2);
            $table->string('vat_nhil_rate');
            $table->decimal('vat_nhil_amt', 15, 2);
            $table->string('tourism_levy');
            $table->decimal('tourism_amt', 15, 2);
            $table->decimal('total', 15, 2);
            $table->decimal('amt_paid', 15, 2);
            $table->decimal('balance', 15, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_receipts');
    }
}
