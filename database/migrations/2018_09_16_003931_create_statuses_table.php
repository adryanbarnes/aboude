<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('color_indicator');
        });

        DB::table('statuses')->insert([
           ['name'=> 'New', 'color_indicator'=> '#FBBC2D'],
           ['name'=> 'Draft', 'color_indicator'=> '#6C7695'],
           ['name'=> 'Paid', 'color_indicator'=> '#6EC7AD'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
    }
}
