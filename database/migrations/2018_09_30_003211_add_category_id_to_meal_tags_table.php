<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryIdToMealTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_tags', function (Blueprint $table) {
            $table->integer('meal_categories_id')->index()->unsigned()->default(1)->after('name');
            $table->foreign('meal_categories_id')->references('id')->on('meal_categories')->ondelete('restrict'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_tags', function (Blueprint $table) {
             $table->dropForeign(['meal_categories_id']);
             $table->dropColumn(['meal_categories_id']);
        });
    }
}
