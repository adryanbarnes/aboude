<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersModifiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_modifiers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_no_id')->index()->unsigned();
            $table->foreign('order_no_id')->references('order_no')->on('orders')->ondelete('restrict'); 

            $table->integer('order_id')->index()->unsigned();
            $table->foreign('order_id')->references('id')->on('orders')->ondelete('restrict'); 

            $table->integer('meal_menu_id')->index()->unsigned();
            $table->foreign('meal_menu_id')->references('id')->on('meal_menus')->ondelete('set restrict');

            $table->integer('food_modifier_id')->index()->unsigned();
            $table->foreign('food_modifier_id')->references('id')->on('food_modifiers')->ondelete('restrict');             
            $table->integer('qty')->index()->unsigned();
            $table->decimal('unit_price', 15, 2);
            $table->decimal('total_amt', 15, 2);            
            $table->softDeletes();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_modifiers');
    }
}
