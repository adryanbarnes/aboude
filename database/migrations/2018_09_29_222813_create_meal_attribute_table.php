<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('meal_tags_id')->index()->unsigned();
            $table->foreign('meal_tags_id')->references('id')->on('meal_tags')->onupdate('cascade')->ondelete('restrict');            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_attributes');
    }
}
