<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealPackSizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_pack_sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('meal_menu_id')->index()->unsigned();
            $table->foreign('meal_menu_id')->references('id')->on('meal_menus')->ondelete('set restrict'); 
            $table->integer('pack_size_id')->index()->unsigned();
            $table->foreign('pack_size_id')->references('id')->on('pack_sizes')->ondelete('set restrict');                         
            $table->decimal('price', 15, 2);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_pack_sizes');
    }
}
