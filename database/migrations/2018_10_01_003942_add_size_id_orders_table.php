<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSizeIdOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('meal_pack_size_id')->index()->unsigned()->default(1)->after('meal_menu_id');
            $table->foreign('meal_pack_size_id')->references('id')->on('meal_pack_sizes')->ondelete('restrict'); 
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
           $table->dropForeign(['meal_pack_size_id']);
           $table->dropColumn(['meal_pack_size_id']);
        });
    }
}
