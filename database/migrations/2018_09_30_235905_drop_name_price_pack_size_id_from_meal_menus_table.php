<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropNamePricePackSizeIdFromMealMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_menus', function (Blueprint $table) {
           $table->dropForeign(['pack_size_id']);
           $table->dropColumn(['name', 'price', 'pack_size_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_menus', function (Blueprint $table) {
            $table->string('name')->nullable();
            $table->string('price')->nullable();
            $table->integer('pack_size_id')->index()->unsigned()->nullable();
            $table->foreign('pack_size_id')->references('id')->on('pack_sizes')->ondelete('restrict');             
        });
    }
}
