<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Carbon\Carbon;

class CreateExpenseCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();            
            $table->timestamps();
        });

        DB::table('expense_categories')->insert([
           ['name'=> 'Transport', 'created_at' => Carbon::now()->toDateString(), 'updated_at' => Carbon::now()->toDateString()],
           ['name'=> 'Kitchen Supplies', 'created_at' => Carbon::now()->toDateString(), 'updated_at' => Carbon::now()->toDateString()],
           ['name'=> 'Utilities', 'created_at' => Carbon::now()->toDateString(), 'updated_at' => Carbon::now()->toDateString()],
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_categories');
    }
}
