{
    "status": "success",
    "status_code": "1001",
    "status_msg": "Meal Menu Successfully retrieved",
    "meals": [
        {
            "id": 1,
            "name": "Main Dish",
            "category_items": [
                {
                    "id": 1,
                    "name": "Charcoal & Grilled Chicken",
                    "picture": null,
                    "meals": [
                        {
                            "id": 1,
                            "name": "Jollof Rice with Chicken",
                            "sizes": [
                                {
                                    "id": 1,
                                    "name": "Large Pack",
                                    "price": "25.00"
                                }
                            ],
                            "meal_picture": "http://aboude.local/storage/uploads/mealmenu/MealMenu-hot-ginger-drink2018-09-09-6Hp50.png"
                        },
                        {
                            "id": 4,
                            "name": "Jollof Rice with Fried Fish",
                            "sizes": [
                                {
                                    "id": 3,
                                    "name": "Large Pack",
                                    "price": "20.00"
                                }
                            ],
                            "meal_picture": "http://aboude.local/storage/uploads/mealmenu/MealMenu-hot-ginger-drink2018-09-09-6Hp50.png"
                        }
                    ],
                    "modifiers": [
                        {
                            "id": 1,
                            "name": "Tomatoes",
                            "price": "0.00"
                        },
                        {
                            "id": 2,
                            "name": "Letuce",
                            "price": "0.00"
                        }
                    ]
                },
                {
                    "id": 4,
                    "name": "Rice",
                    "picture": null,
                    "meals": [
                        {
                            "id": 6,
                            "name": "Fried rice",
                            "sizes": [
                                {
                                    "id": 4,
                                    "name": "Large Pack",
                                    "price": "15.00"
                                }
                            ],
                            "meal_picture": "http://aboude.local/storage/uploads/mealmenu/MealMenu-hot-ginger-drink2018-09-09-6Hp50.png"
                        }
                    ],
                    "modifiers": []
                }
            ]
        },
        {
            "id": 2,
            "name": "Drinks & Ice Creams",
            "category_items": [
                {
                    "id": 2,
                    "name": "Drink",
                    "picture": null,
                    "sizes": [
                        {
                            "id": 2,
                            "name": "1L",
                            "price": "5.00"
                        }
                    ],
                    "modifiers": []
                }
            ]
        }
    ]
}
