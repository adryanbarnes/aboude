<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdersModifier extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];    

    protected $guarded = [];  

    public function foodModifier(){
    	return $this->belongsTo('App\FoodModifier');
    }

    public function mealMenu(){
        return $this->belongsTo('App\MealMenu');
    }   

}
