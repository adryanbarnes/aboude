<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MealTag extends Model
{
	use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function mealMenu(){
    	return $this->hasMany('App\MealMenu');
    } 

    public function foodModifier(){
        return $this->belongsToMany('App\FoodModifier', 'meal_food_modifiers', 'meal_tag_id', 'food_modifier_id')->withPivot('price','id');
    } 

}
