<?php 

if (! function_exists('acronym')) {
    function acronym($words)
    {
        $words = explode(" ", trim($words));
        $acronym = "";

        foreach ($words as $w) {
          $acronym .= $w ?  $w[0] : '';
        }

        strtoupper($acronym);

        return $acronym;
    }
}

if (! function_exists('vatCalculator')) {
    function vatCalculator($amt, $rate, $addTax = true, $dp = 2)
    {

        if($addTax){
            $taxAmt = ($amt * $rate) / 100;

            $netPrice = $amt + $taxAmt;
        }else{

            $taxAmt = $amt - ( $amt * (100 / ( 100 + $rate)));

            $netPrice = $amt - $taxAmt;
        }

        return ['taxAmt' =>number_format((float) $taxAmt, $dp, '.', '') , 'netPrice' => number_format((float) $netPrice, $dp, '.', '')];
    }
}

if (! function_exists('levyCalculator')) {
    function levyCalculator($amt, $rate, $dp = 2)
    {

        $levyAmt = $amt * ($rate/100); 

        return number_format((float) $levyAmt, $dp, '.', '');
    }
}
