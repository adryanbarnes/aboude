<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

use Log;
use Carbon\Carbon;
use App\Exceptions\Handler;
// Use Image;
use DB;
use Validator;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Propaganistas\LaravelPhone\PhoneNumber;
use DataTables;

use App\Branches;
use App\Employee;
use App\JobRole;
use App\User;

class EmployeeController extends Controller
{
    //use DataTables;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $branches = Branches::pluck('name','id')->prepend('All Locations', 'all');
        return view('admin.employee.index')->withBranches($branches);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Log::debug($request);

        try{

            DB::beginTransaction(); 
                   
            $input = $request->all();

            $path = 'uploads/employees/';

            if(isset($input['profile_picture'])){

                $imageFileName = "Employee-" . $input['other_name'] .'-'. $input['surname'] . '-'. str_random(5) . "-".Carbon::now()->format('Y-m-d') .'.'.$input['profile_picture']->getClientOriginalExtension();
    
                Storage::disk('public')->putFileAs($path, new File($input['profile_picture'] ), $imageFileName);
            }

            $user = new User;

            $user->other_name = $input['other_name'];
            $user->surname = $input['surname'];
            $user->dob = Carbon::parse($input['dob'])->format('Y-m-d');
            $user->phone_number = (string) PhoneNumber::make($input['phone_number'])->ofCountry('GH');;
            $user->email = isset($input['email']) ? $input['email'] : null;
            $user->profile_picture = isset($imageFileName) ? $imageFileName : null;
            $user->password = Hash::make($input['pincode']);

            $user->save();

            $getBranch = Branches::find($input['branch']);
            $getJobRole = JobRole::firstOrCreate(['title' => $input['job_role']]);

            $userEmployee = new Employee;

            $userEmployee->jobRole()->associate($getJobRole);  
            $userEmployee->branches()->associate($getBranch); 
            $userEmployee->user_id = $user->id;

            $userEmployee ->salary = $input['salary'];
            $userEmployee ->salary_type = $input['salary_type'];
            $userEmployee ->start_date = Carbon::parse($input['start_date'])->format('Y-m-d');


            $userEmployee->save();        


            DB::commit();

            return response()->json([
                'status' => 'success',
                'status_code' => '1001',
                'status_msg' => ' Employee Successfully created'
            ]); 

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'status_code' => '1002',
                'status_msg' => ' unexpected error encountered creating employee'
            ]);             
        }            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = User::findOrFail($id);
        return view('admin.employee.show')->withEmployee($employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        Log::debug($request);

        try{

            DB::beginTransaction(); 
                   
            $input = $request->all();

            $path = 'uploads/employees/';

            if(isset($input['profile_picture'])){

                $imageFileName = "Employee-" . $input['other_name'] .'-'. $input['surname'] . '-'. str_random(5) . "-".Carbon::now()->format('Y-m-d') .'.'.$input['profile_picture']->getClientOriginalExtension();
    
                Storage::disk('public')->putFileAs($path, new File($input['profile_picture'] ), $imageFileName);
            }

            $user = User::find($id);

            $user->other_name = $input['other_name'];
            $user->surname = $input['surname'];
            $user->dob = Carbon::parse($input['dob'])->format('Y-m-d');
            $user->phone_number = (string) PhoneNumber::make($input['phone_number'])->ofCountry('GH');;
            $user->email = isset($input['email']) ? $input['email'] : null;
            $user->profile_picture = isset($imageFileName) ? $imageFileName : null;

            $user->save();

            $getBranch = Branches::find($input['branch']);
            $getJobRole = JobRole::firstOrCreate(['title' => $input['job_role']]);

            $userEmployee = Employee::whereUserId($id)->first();

            $userEmployee->jobRole()->associate($getJobRole);  
            $userEmployee->branches()->associate($getBranch); 
            $userEmployee->user_id = $user->id;

            $userEmployee ->salary = $input['salary'];
            $userEmployee ->salary_type = $input['salary_type'];
            $userEmployee ->start_date = Carbon::parse($input['start_date'])->format('Y-m-d');


            $userEmployee->save();        


            DB::commit();

            return response()->json([
                'status' => 'success',
                'status_code' => '1001',
                'status_msg' => ' Employee profile Successfully updated'
            ]); 

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'status_code' => '1002',
                'status_msg' => ' unexpected error encountered updating employee'
            ]);             
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::find($id)->delete();

        return response()->json([
            'status' => 'success',
            'status_code' => '1001',
            'status_msg' => ' Employee Successfully deleted'
        ]); 
    }

    public function getEmployees(Request $request){

        $input = $request->all();

        $model = Employee::with(['user', 'jobRole', 'branches']);

        $location = !empty($input['location']) && strtolower($input['location']) != 'all' ? $input['location'] : null;

        if($location){

            $model->whereHas('branches', function ($query) use ($location) {
                $query->whereId($location);
            });         
        }
        $i = 1;
        return DataTables::collection($model->get())
                ->addColumn('No', function ($employee) use(&$i) {
                    return $i++;

                })        
                ->addColumn('Employee', function ($employee) {
                    $fullname = $employee->user->other_name . ' '. $employee->user->surname;
                    if($employee->user->profile_picture){
                        $employeeRow = "<span class='employee-initials'><img src='". asset('storage/uploads/employees/'. $employee->user->profile_picture) ."'></span>";
                    }else{
                        $employeeRow = "<span class='employee-initials'>". acronym($fullname) ."</span>";
                    }

                    return "<a href='employees/".$employee->user->id. "'>".$employeeRow . $fullname . "</a>";

                })
                ->addColumn('Phone Number', function ($employee) {
                    return ltrim(PhoneNumber::make($employee->user->phone_number)->formatRFC3966(), "tel:");

                })
                ->addColumn('Role', function ($employee) {
                    return $employee->jobRole->title;

                }) 
                ->addColumn('Branch', function ($employee) {
                    return $employee->branches->name;

                })
                ->addColumn('Last Login', function ($employee) {
                    return Carbon::parse($employee->user->update_at)->format('d/m/Y - h:i a ');

                }) 

                ->addColumn('Action', function ($employee) {

                    return '<a href="#" class="edit-employee" data-editid="'.$employee->user->id.'"><i class="fas fa-edit"></i></a> <a href="#" class="delete-employee" data-deleteid="'.$employee->user->id.'"><i class="far fa-trash-alt"></i></a></td>
                            </tr>';

                })                               

                ->escapeColumns([])->smart(true)->make(true);
    }

    public function employeePartial($id, $editId = null){
        switch ($id) {
            case 1:
                $branches = Branches::pluck('name','id')->prepend('All Locations', 'all');
                return view('admin.employee.partials.add-employee')->withBranches($branches);
                break;

            case 2:
                $employee = User::find($editId);

                $branches = Branches::pluck('name','id')->prepend('All Locations', 'all');
                return view('admin.employee.partials.edit-employee')->withEmployee($employee)->withBranches($branches)->withEditId($editId);
                break;  

            default:
                # code...
                break;
        }
    }


    public function resetPincode($id = null){

/*$reset_token = strtolower(str_random(64));
DB::table('password_resets')->insert([
    'email' => $request->email,
    'token' => $reset_token,
    'created_at' => Carbon::now(),
]);
*/
        if(!empty($id)){
            $user = User::find($id);
            $token = Password::getRepository()->create($user);

            Log::debug($token);
            
            return response()->json([
                'status' => 'success',
                'status_code' => '1001',
                'status_msg' => ' Employee pin reset successful'
            ]);                       
        }

        $user = User::where('email', request()->input('email'))->first();
        $token = Password::getRepository()->create($user);

        return response()->json([
            'status' => 'success',
            'status_code' => '1001',
            'status_msg' => ' Employee pin reset successful'
        ]);

    }
}
