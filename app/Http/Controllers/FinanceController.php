<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExpenseCategory;
use App\Expense;
use App\Status;
use App\Branches;
use App\Employee;
use App\PurchaseOrder;
use App\TaxTerm;
use App\PurchaseItem;
use App\OrderReceipt;
use App\Order;
use App\OrderStatus;
use Log;
use Carbon\Carbon;
use App\Exceptions\Handler;
// Use Image;
use DB;
use Validator;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Propaganistas\LaravelPhone\PhoneNumber;
use DataTables;
use Toastr;

class FinanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branches::pluck('name','id')->prepend('All Locations', 'all');

        return view('finance.index')->withBranches($branches);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Log::debug($request);

        try{

            DB::beginTransaction(); 
                   
            $input = $request->all();

            $path = 'uploads/receipts/';

            if(isset($input['receipt'])){
                $path.= Carbon::now()->format('Y-m');

                $imageFileName = "receipt-" .  str_random(5) . "-".Carbon::now()->format('Y-m-d') .'.'.$input['receipt']->getClientOriginalExtension();
    
                Storage::disk('public')->putFileAs($path, new File($input['receipt'] ), $imageFileName);

                $imageFileName = Carbon::now()->format('Y-m') .'/'.$imageFileName;
            }

            $saveExpense = new Expense;

            $getCategory = ExpenseCategory::find($input['category']);
            $getStatus = Status::find($input['status']);


            $saveExpense->expenseCategories()->associate($getCategory);  
            $saveExpense->statuses()->associate($getStatus); 
            $saveExpense->user_id = $request->user()->id;
            $saveExpense->name = $input['name'];
            $saveExpense->expense_date = Carbon::parse($input['date'])->format('Y-m-d');

            $saveExpense->comment = $input['comment'];
            $saveExpense->receipt = $imageFileName;
            $saveExpense->tax = $input['tax'];
            $saveExpense->tax_paid = $input['tax_paid'];


            $saveExpense->save();        


            DB::commit();

            return response()->json([
                'status' => 'success',
                'status_code' => '1001',
                'status_msg' => ' Expense Successfully created'
            ]); 

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'status_code' => '1002',
                'status_msg' => ' unexpected error encountered creating expense'
            ]);             
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function createPurchaseOrder(){

        $taxTerms = TaxTerm::pluck('term', 'id');
        $purchaseOrderNo = PurchaseOrder::orderBy('created_at', 'DESC')->first();

        if($purchaseOrderNo){
            preg_match('~\/(\d+)\/~', $purchaseOrderNo->purchase_no, $matches);  
            $matches =  $matches[1] + 1;
            $purchaseOrderNo = 'PO/'. $matches . '/'. Carbon::now()->format('y'); 

       }else{
        $purchaseOrderNo = 'PO/1/'. Carbon::now()->format('y');
       }
        

        return view('finance.purchase-order.add-purchase')->withPurchaseOrderNo($purchaseOrderNo)->withTaxTerms($taxTerms);
    }

    public function storePurchaseOrder(Request $request)
    {
        Log::debug($request);

        try{

            DB::beginTransaction(); 
                   
            $input = $request->all();     

            $getTaxTerms = TaxTerm::find($input['tax_term']);

            $subTotal =  array_sum($input['amt']);

            $totalTaxAmt = array_sum($input['tax_amt']);

            $totalPurchase = $input['tax_term'] = 1 ? $subTotal + $totalTaxAmt : $subTotal - $totalTaxAmt;

            for ($i=0; $i < count($input['item']); $i++) { 
                $savePurchase = new PurchaseOrder;

                $getItem = PurchaseItem::firstOrCreate( ['name' => $input['item'][$i]]);
                
                $savePurchase->purchaseItem()->associate($getItem);  
                $savePurchase->taxTerm()->associate($getTaxTerms);
                $savePurchase->supplier = $input['supplier'];
                $savePurchase->purchase_date = Carbon::parse($input['purchase_date'])->format('Y-m-d');
                $savePurchase->purchase_no = $input['purchase_no'];
                $savePurchase->description = $input['description'][$i];
                $savePurchase->qty = $input['qty'][$i];
                $savePurchase->unit_price = $input['unit_price'][$i];
                $savePurchase->tax_amt = $input['tax_amt'][$i];
                $savePurchase->total_amt = $input['amt'][$i];
                $savePurchase->sub_total = $subTotal;
                $savePurchase->total_purchase = $totalPurchase; 
                $savePurchase->statuses_id = 1;               

                $savePurchase->save(); 
            }

            

            DB::commit();

            Toastr::success("Purchase Order Successfully created");
            return redirect()->route('purchase-order.index');

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            Toastr::error("unexpected error encountered creating purchase");
            return redirect()->back()->withInput($input);

        }  
    }    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::debug($request);

        try{

            DB::beginTransaction(); 
                   
            $input = $request->all();

            $path = 'uploads/receipts/';

            if(isset($input['receipt'])){
                $path.= Carbon::now()->format('Y-m');

                $imageFileName = "receipt-" .  str_random(5) . "-".Carbon::now()->format('Y-m-d') .'.'.$input['receipt']->getClientOriginalExtension();
    
                Storage::disk('public')->putFileAs($path, new File($input['receipt'] ), $imageFileName);

                $imageFileName = Carbon::now()->format('Y-m') .'/'.$imageFileName;
            }else{
                $imageFileName = null;
            }

            $saveExpense = Expense::find($id);

            $getCategory = ExpenseCategory::find($input['category']);
            $getStatus = Status::find($input['status']);


            $saveExpense->expenseCategories()->associate($getCategory);  
            $saveExpense->statuses()->associate($getStatus); 
            $saveExpense->user_id = $request->user()->id;
            $saveExpense->name = $input['name'];
            $saveExpense->expense_date = Carbon::parse($input['date'])->format('Y-m-d');

            $saveExpense->comment = $input['comment'];
            $saveExpense->receipt = $imageFileName;
            $saveExpense->tax = $input['tax'];
            $saveExpense->tax_paid = $input['tax_paid'];


            $saveExpense->save();        


            DB::commit();

            return response()->json([
                'status' => 'success',
                'status_code' => '1001',
                'status_msg' => ' Expense Successfully updated'
            ]); 

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'status_code' => '1002',
                'status_msg' => ' unexpected error encountered updating expense'
            ]);             
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Expense::find($id)->delete();

        return response()->json([
            'status' => 'success',
            'status_code' => '1001',
            'status_msg' => ' Expense Successfully deleted'
        ]);         
    }

    public function financePartial($type, $period = null, Request $request){

        $input = $request->all();

        $cancelled = OrderStatus::where('name', '=', 'Cancelled')->first();

        switch ($type) {

            case 1:
                $miniwidget = [];

                $miniwidget['Sales Today with VAT'] =  'GHS '. Order::whereDate('created_at', Carbon::now()->format('Y-m-d'))->where('order_status_id', '!=', $cancelled->id)->sum('total_amt');

                $miniwidget['Income Today'] =  'GHs 0';

                $miniwidget['Expenses Today'] = 'GHS '. Expense::whereDate('expense_date', Carbon::now()->format('Y-m-d'))->sum('tax_paid');


                $vat = OrderReceipt::whereDate('created_at', Carbon::now()->format('Y-m-d'))->sum('vat_nhil_amt');

                $miniwidget['VAT Due'] = 'GHS '. (str_replace('GHS ', '', $miniwidget['Sales Today with VAT']) - $vat);



                return view('widgets.mini-widget')->withMiniWidgets($miniwidget)->withCol(2);
            break; 

            case 2:

                $chart = array();

                $chartName = 'income expenses and sales';
             
                $category = array();

                $ordersQuery = Order::where('order_status_id', '!=', $cancelled->id);
                $orders = Order::where('order_status_id', '!=', $cancelled->id);

                $expenseQuery = new Expense;  


                switch ($period) {
                  case 1:

                    $date =Carbon::now()->startOfDay();
                    $end = Carbon::now()->endOfDay();
                    $period = array();

                    while ($date <= $end) {

                        $period[] = $date->copy()->format('H:i');

                        $date->addHours(1);
                    }


                  foreach ($period  as $labels) {
                      $chart['labels'][] = $labels;


                      $category[] = $labels;
                  }                    

                    for ($i=0; $i < count($category); $i++) { 

                      $chart['dataset']['Sales'][] = with(clone $ordersQuery)
                        ->where(DB::raw('HOUR(created_at)'), '=', $category[$i])
                        ->whereDate('created_at', Carbon::now())
                        ->sum('total_amt'); 

                      $chart['dataset']['Expenses'][] = with(clone $expenseQuery)
                        ->where(DB::raw('HOUR(expense_date)'), '=', $category[$i])
                        ->whereDate('expense_date', Carbon::now())
                        ->sum('tax_paid');                      
                    } 

                    $chartTitle = 'Today Income, Expenses and Sales';
                    $chartxAxes = 'Hourly Income, Expenses and Sales';              

                    break;

                  case 2:

                    $date =Carbon::now()->subDays(13);
                    $end = Carbon::now();
                    $period = array();
                    while ($date <= $end) {

                        $period[] = $date->copy()->format('Y-m-d');

                        $date->addDays(1);
                    }


                  foreach ($period  as $labels) {
                    $chart['labels'][] = Carbon::parse($labels)->copy()->format('d M');


                    $category[] = Carbon::parse($labels)->format('Y-m-d');
                  }  

                    for ($i=0; $i < count($category); $i++) { 

                      $chart['dataset']['Sales'][] = with(clone $ordersQuery)
                        ->whereDate('created_at', $category[$i])
                        ->sum('total_amt'); 

                      $chart['dataset']['Expenses'][] = with(clone $expenseQuery)
                        ->whereDate('expense_date', $category[$i])
                        ->sum('tax_paid');                       
                    }  

                    $chartTitle = 'Daily Income, Expenses and Sales for the Last 14 days';
                    $chartxAxes = 'Daily Income, Expenses and Sales';
                                                   

                  break;

                  case 3:
                    $getLabels = $orders->select(DB::raw('DATE_FORMAT(created_at, "%M %y") as labels'))
                    ->orderBy('labels')
                    ->groupBy('labels');

                    $data = $getLabels->pluck('labels');

                    foreach ($data as $labels) {

                      $chart['labels'][] = Carbon::parse($labels)->copy()->format('M-Y');


                      $category[] = Carbon::parse($labels)->format('Y-m-d');

                    }

                    for ($i=0; $i < count($category); $i++) { 

                      $chart['dataset']['Sales'][] = with(clone $ordersQuery)
                      ->whereMonth('created_at', '=', Carbon::parse($category[$i])->copy()->format('m'))
                      ->whereYear('created_at', '=', Carbon::parse($category[$i])->copy()->format('Y'))
                      ->sum('total_amt');

                      $chart['dataset']['Expenses'][] = with(clone $expenseQuery)
                      ->whereMonth('expense_date', '=', Carbon::parse($category[$i])->copy()->format('m'))
                      ->whereYear('expense_date', '=', Carbon::parse($category[$i])->copy()->format('Y'))
                      ->sum('tax_paid');                     
                    }  

                    $chartTitle = 'Monthly Income, Expenses and Sales';
                    $chartxAxes = 'Income, Expenses and Sales This Month';
                    $chartyAxes = 'Monthly Income, Expenses and Sales';

                  break;

                  default:
                    $date =Carbon::now()->subDays(13);
                    $end = Carbon::now();
                    $period = array();
                    while ($date <= $end) {

                        $period[] = $date->copy()->format('Y-m-d');

                        $date->addDays(1);
                    }


                  foreach ($period  as $labels) {
                    $chart['labels'][] = Carbon::parse($labels)->copy()->format('d M');


                    $category[] = Carbon::parse($labels)->format('Y-m-d');
                  }  

                    for ($i=0; $i < count($category); $i++) { 

                      $chart['dataset']['Sales'][] = with(clone $ordersQuery)
                        ->whereDate('created_at', $category[$i])
                        ->sum('total_amt'); 

                      $chart['dataset']['Expenses'][] = with(clone $expenseQuery)
                        ->whereDate('expense_date', $category[$i])
                        ->sum('tax_paid');                       
                    }  

                    $chartTitle = 'Daily Income, Expenses and Sales for the Last 14 days';
                    $chartxAxes = 'Daily Income, Expenses and Sales';
                    
                    break;              
                }

                  $chartyAxes = 'Amount in Ghs';
                  $label1 = 'Sales';
                  $label2 = "Expenses";
                  $label3 = "Income";               

                  if(!empty($chart['dataset']) && count($chart['dataset']) > 0){
                      $chart['type'] = 'line';

                      $chart["datasetColor"][$label1]["bgcolor"] = '#4F67D1';

                      $chart["datasetColor"][$label1]["bordercolor"] = '#4F67D1'; 


                      $chart["datasetColor"][$label2]["bgcolor"] = '#EE5C90';

                      $chart["datasetColor"][$label2]["bordercolor"] = '#EE5C90';

                      $chart["datasetColor"][$label3]["bgcolor"] = '#FBBC2D';

                      $chart["datasetColor"][$label3]["bordercolor"] = '#FBBC2D';                                                     
                      
                      $chart['config']['title_display'] = true;
                      $chart['config']['title'] = $chartTitle;

                      $chart['config']['legend_display'] = true;
                      $chart['config']['legend_position'] = 'bottom';

                      $chart['config']['scales_xAxes']['display'] = true;
                      $chart['config']['scales_xAxes']['label'] = $chartxAxes;

                      $chart['config']['scales_yAxes']['display'] = false;
                      $chart['config']['scales_yAxes']['label'] = $chartyAxes;                                
                  }

                return view('widgets.chart-widget')->withChart($chart)->withChartName($chartName);       
            break;


            case 3:

                $chart = array();

                $chartName = 'Expenses by bill category';

                $startDate = isset($input['from_date']) ? Carbon::parse($input['from_date'])->format('Y-m-d') : null;
                $endDate = isset($input['to_date']) ? Carbon::parse($input['to_date'])->format('Y-m-d') : Carbon::now();
                


                $getLabel = Expense::groupBy('name');

                if(!empty($startDate)){
                    $getLabel->whereBetween('expense_date',[$startDate, $endDate]);
                }                  

                $location = !empty($input['location']) && strtolower($input['location']) != 'all' ? $input['location'] : null;

        /*        if($location){

                    $getLabel->whereHas('branches', function ($query) use ($location) {
                        $query->whereId($location);
                    });         
                }*/

                $getLabel = $getLabel->pluck('name');

                foreach ($getLabel as $labels) {
                    $chart['labels'][] = $labels;

                }   



                $query = Expense::groupBy('name');

                $location = !empty($input['location']) && strtolower($input['location']) != 'all' ? $input['location'] : null;

        /*        if($location){

                    $model->whereHas('branches', function ($query) use ($location) {
                        $query->whereId($location);
                    });         
                }*/

                if(!empty($startDate)){
                    $query->whereBetween('expense_date',[$startDate, $endDate]);
                }                  


                  for ($i=0; $i < count($chart['labels']); $i++) { 

                    $chart['dataset']['Expenses'][] = with(clone $query)
                      ->whereName($chart['labels'][$i])
                        ->sum('tax_paid');

                  } 


                  if(!empty($chart['dataset']) && count($chart['dataset']) > 0){
                      $chart['type'] = 'bar';

                      $chart["datasetColor"]['Expenses']["bgcolor"] = '#4F67D1';

                      $chart["datasetColor"]['Expenses']["bordercolor"] = '#4F67D1';             
                      
                      $chart['config']['title_display'] = true;
                      $chart['config']['title'] = 'Expenses by Bill Categories';

                      $chart['config']['legend_display'] = false;
                      $chart['config']['legend_position'] = 'bottom';

                      $chart['config']['scales_xAxes']['display'] = true;
                      $chart['config']['scales_xAxes']['label'] = 'Expenses Category';

                      $chart['config']['scales_yAxes']['display'] = true;
                      $chart['config']['scales_yAxes']['label'] = 'Amount in GHS';                                
                  }                                            

                return view('widgets.chart-widget')->withChart($chart)->withChartName($chartName);
              break;

            case 4:
                
                $model = PurchaseOrder::with(['purchaseItem', 'taxTerm', 'statuses', 'branches']);

                $location = !empty($input['location']) && strtolower($input['location']) != 'all' ? $input['location'] : null;

        /*        if($location){

                    $model->whereHas('branches', function ($query) use ($location) {
                        $query->whereId($location);
                    });         
                }*/

                $startDate = isset($input['from_date']) ? Carbon::parse($input['from_date'])->format('Y-m-d') : null;
                $endDate = isset($input['to_date']) ? Carbon::parse($input['to_date'])->format('Y-m-d') : Carbon::now();
                
                if(!empty($startDate)){
                    $model->whereBetween('purchase_date',[$startDate, $endDate]);
                }

             
                $i = 1;
                return DataTables::collection($model->get())
                        ->addColumn('No', function ($purchase) use(&$i) {
                            return $i++;

                        })
                        ->addColumn('Item', function ($purchase) {
                            return $purchase->purchaseItem->name;

                        })                                
                        ->addColumn('Bill Number', function ($purchase) {
                            return $purchase->purchase_no;

                        })
                        ->addColumn('Date of Purchase', function ($purchase) {
                            return Carbon::parse($purchase->purchase_date)->format('d/m/Y');

                        }) 
                        ->addColumn('Supplier', function ($purchase) {
                            return $purchase->supplier;

                        }) 

                        ->addColumn('Qty', function ($purchase) {
                            return $purchase->qty;

                        })                         
                        ->addColumn('Tax', function ($purchase) {
                            return $purchase->tax_amt;

                        })
                        ->addColumn('Amount Paid', function ($purchase) {
                            return $purchase->total_amt;

                        })                                                         

                        ->escapeColumns([])->smart(true)->make(true);
                break;
            
            default:
                # code...
                break;
        }
    }

    public function expenses($id = null){


        if(!empty($id)){

                $date =Carbon::now()->startOfWeek();
                $end = Carbon::now()->endOfWeek();
                while ($date <= $end) {

                    $period[] = $date->copy()->format('Y-m-d');

                    $date->addDays(1);
                }

                $query = Expense::groupBy('name');

                foreach ($period as $label) {
                  $chart['labels'][] = Carbon::parse($label)->format('l');

                }  

                for ($i=0; $i < 7; $i++) { 

                  $chart['dataset']['This Week'][] = with(clone $query)
                    ->whereBetween('expense_date', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                    ->where(DB::raw('WEEKDAY(expense_date)'), '=', $i)
                    ->sum('tax_paid'); 

                  $chart['dataset']['Last Week'][] = with(clone $query)
                    ->whereBetween('expense_date', [Carbon::now()->subWeek()->startOfWeek(), Carbon::now()->subWeek()->endOfWeek()])
                    ->where(DB::raw('WEEKDAY(expense_date)'), '=', $i)
                    ->sum('tax_paid');                     
                } 


               $chartName = 'Week Expenses';

                $chartTitle = 'This Week’s Expenses As compared to last week’s';
                $chartxAxes = 'Day of Week';
                $chartyAxes = 'Amount in Ghana';
                $current = 'This Week';
                $previous = "Last Week";                           


              if(!empty($chart['dataset']) && count($chart['dataset']) > 0){
                  $chart['type'] = 'bar';

                  $chart["datasetColor"][$current]["bgcolor"] = '#FBBC2D';

                  $chart["datasetColor"][$current]["bordercolor"] = '#FBBC2D'; 


                  $chart["datasetColor"][$previous]["bgcolor"] = '#DDDDDD';

                  $chart["datasetColor"][$previous]["bordercolor"] = '#DDDDDD';                               
                  
                  $chart['config']['title_display'] = true;
                  $chart['config']['title'] = $chartTitle;

                  $chart['config']['legend_display'] = false;
                  $chart['config']['legend_position'] = 'bottom';

                  $chart['config']['scales_xAxes']['display'] = true;
                  $chart['config']['scales_xAxes']['label'] = $chartxAxes;

                  $chart['config']['scales_yAxes']['display'] = false;
                  $chart['config']['scales_yAxes']['label'] = $chartyAxes;                                
              }

            return view('widgets.chart-widget')->withChart($chart)->withChartName($chartName);              
        }

        $branches = Branches::pluck('name','id')->prepend('All Locations', 'all');

        $topExpenses = Expense::groupBy('name')->select(DB::raw('sum(tax_paid) as tax_paid'), 'name')->take(3)->orderBy('tax_paid','desc')->get();

        return view('finance.expense')->withBranches($branches)->withTopExpenses($topExpenses);;
    }

    public function expensesList(Request $request){

        $input = $request->all();

        $model = Expense::with(['user', 'expenseCategories', 'statuses']);

        $location = !empty($input['location']) && strtolower($input['location']) != 'all' ? $input['location'] : null;

        if($location){

            // $model->whereHas('branches', function ($query) use ($location) {
            //     $query->whereId($location);
            // });         
        }
        $i = 1;
        return DataTables::collection($model->get())
                ->addColumn('No', function ($expense) use(&$i) {
                    return $i++;

                })    
                ->addColumn('Item', function ($expense) {
                    return $expense->name;

                })                    
                ->addColumn('Spent On', function ($expense) {
                    return Carbon::parse($expense->expense_date)->format('d/m/Y');

                })
                ->addColumn('Category', function ($expense) {
                    return $expense->expenseCategories->name;

                }) 
                ->addColumn('Tax', function ($expense) {
                    return $expense->tax;

                })

                ->addColumn('Amount Paid', function ($expense) {
                    return $expense->tax_paid;

                })                
                ->addColumn('Status', function ($expense) {

                    return "<span style='background:".$expense->statuses->color_indicator."' class='table-indicator'>". $expense->statuses->name ."</span>";

                })

                ->addColumn('Action', function ($expense) {
                    return '<a href="#" class="edit-expense" data-editid="'.$expense->id.'"><i class="fas fa-edit"></i></a> <a href="#" class="delete-expense" data-deleteid="'.$expense->id.'"><i class="far fa-trash-alt"></i></a></td>
                            </tr>';

                })                               

                ->escapeColumns([])->smart(true)->make(true);       
    }

    public function expensePartial($id, $editId = null){
        switch ($id) {
            case 1:
                $create = true;
                $category = ExpenseCategory::pluck('name','id')->prepend('Please select a category', 'none');
                $status = Status::pluck('name','id')->prepend('Please select a status', 'none');
                return view('finance.expense-partials.add-edit-expense')->withCategory($category)->withStatus($status)->withCreate($create);
                break;

            case 2:
                $create = false;
                $category = ExpenseCategory::pluck('name','id')->prepend('Please select a category', 'none');
                $status = Status::pluck('name','id')->prepend('Please select a status', 'none');

                $editExpense = Expense::find($editId);
                return view('finance.expense-partials.add-edit-expense')->withCategory($category)->withStatus($status)->withEditId($editId)->withCreate($create)->withEditExpense($editExpense);
                break;  

            default:
                # code...
                break;
        }
    }


    public function purchaseOrder(){
        $branches = Branches::pluck('name','id')->prepend('All Locations', 'all');
    
        return view('finance.purchase-order.index')->withBranches($branches);
    }

    public function showPayroll(){
        $branches = Branches::pluck('name','id')->prepend('All Locations', 'all');       
        return view('finance.payroll')->withBranches($branches);
    }

    public function getEmployeesPayroll(Request $request){

        $input = $request->all();

        $model = Employee::with(['user', 'jobRole', 'branches']);

        $location = !empty($input['location']) && strtolower($input['location']) != 'all' ? $input['location'] : null;

        if($location){

            $model->whereHas('branches', function ($query) use ($location) {
                $query->whereId($location);
            });         
        }
        $i = 1;
        return DataTables::collection($model->get())
                ->addColumn('No', function ($employee) use(&$i) {
                    return $i++;

                })        
                ->addColumn('Employee', function ($employee) {
                    $fullname = $employee->user->other_name . ' '. $employee->user->surname;
                    if($employee->user->profile_picture){
                        $employeeRow = "<span class='employee-initials'><img src='". asset('storage/uploads/employees/'. $employee->user->profile_picture) ."'></span>";
                    }else{
                        $employeeRow = "<span class='employee-initials'>". acronym($fullname) ."</span>";
                    }

                    return "<a href='employees/".$employee->user->id. "'>".$employeeRow . $fullname . "</a>";

                })
                ->addColumn('Month', function ($employee) {
                    return Carbon::parse($employee->start_date)->format('m/Y');

                }) 
                ->addColumn('Total Salaries Paid', function ($employee) {
                    return 'GHS '. $employee->salary;

                })
                ->addColumn('Total Allowance', function ($employee) {
                    return 'GHS 0';

                }) 

                ->addColumn('Total Deductions', function ($employee) {

                    return 'GHS 0';

                })                               

                ->escapeColumns([])->smart(true)->make(true);
    } 

    public function getPurchaseOrders(Request $request){

        $input = $request->all();

        $model = PurchaseOrder::with(['purchaseItem', 'taxTerm', 'statuses', 'branches'])->groupBy('purchase_no');

        $location = !empty($input['location']) && strtolower($input['location']) != 'all' ? $input['location'] : null;

/*        if($location){

            $model->whereHas('branches', function ($query) use ($location) {
                $query->whereId($location);
            });         
        }*/

        $startDate = isset($input['from_date']) ? Carbon::parse($input['from_date'])->format('Y-m-d') : null;
        $endDate = isset($input['to_date']) ? Carbon::parse($input['to_date'])->format('Y-m-d') : Carbon::now();
        
        if(!empty($startDate)){
            $model->whereBetween('purchase_date',[$startDate, $endDate]);
        }        
        $i = 1;
        return DataTables::collection($model->get())
                ->addColumn('No', function ($purchase) use(&$i) {
                    return $i++;

                })        
                ->addColumn('Bill Number', function ($purchase) {
                    return $purchase->purchase_no;

                })
                ->addColumn('Date Raised', function ($purchase) {
                    return Carbon::parse($purchase->purchase_date)->format('d/m/Y');

                }) 
                ->addColumn('Supplier', function ($purchase) {
                    return $purchase->supplier;

                }) 
                ->addColumn('Tax', function ($purchase) {
                    return $purchase->tax_amt;

                })
                ->addColumn('Amount Paid', function ($purchase) {
                    return $purchase->total_purchase;

                })

                ->addColumn('Status', function ($purchase) {

                    return "<span style='background:".$purchase->statuses->color_indicator."' class='table-indicator'>". $purchase->statuses->name ."</span>";

                })                              

                ->addColumn('Action', function ($purchase) {

                    return '<a href="#" class="edit-purchase" data-editid="'.$purchase->id.'"><i class="fas fa-edit"></i></a> <a href="#" class="delete-purchase" data-deleteid="'.$purchase->id.'"><i class="far fa-trash-alt"></i></a></td>
                            </tr>';

                })                               

                ->escapeColumns([])->smart(true)->make(true);
    }   

    public function destroyPurchaseOrder($id)
    {
        $purchase = PurchaseOrder::find($id);

        PurchaseOrder::wherePurchaseNo($purchase->purchase_no)->delete();

        return response()->json([
            'status' => 'success',
            'status_code' => '1001',
            'status_msg' => ' Purchase Order Successfully deleted'
        ]);         
    }  

    public function financeReport(){

        $branches = Branches::pluck('name','id')->prepend('All Locations', 'all');

        return view('finance.report')->withBranches($branches);
    }

    public function reportPartial($reportType, $type){
        switch ($reportType) {
            case 'sales':
                # code...
                break;
            
            case 'order':
                # code...
                break;
        }
    }  

}
