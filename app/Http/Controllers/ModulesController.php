<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;

use App\Http\Requests;

use App\Module;
use App\Privilege;
use App\ModulePrivileges;
use App\User;

use Session;
use Log;


class ModulesController extends Controller
{
    

    public function index()
    {
    	return view('settings.modules.index')
    	         ->with('privileges',Privilege::pluck('name','id'))
    	         ->with('modules',Module::all());
    }


    public function edit($moduleId)
    {

        $selectedPrivs = [];
        foreach (Module::find($moduleId)->privileges()->pluck('privilege_id') as $id) {
            $selectedPrivs[] .= $id; 
        }

    	return view('settings.modules.edit')->withModule(Module::find($moduleId))
               ->with('privileges',Privilege::pluck('name','id'))
               ->with('module_privileges',$selectedPrivs);
    }



    public function store(Requester $request)
    {		


    	    $validate = $this->validate($request, ['name' => 'required', 'description' => 'required', 'route_segment' => 'required']);

	    	$module =  Module::create(
	    		 ['name'=>$request->get('name'),
	    		 'description'=>$request->get('description'),
	    		 'route_segment'=>$request->get('route_segment'),
	    		 'type_id'=>$request->get('type'),
                 'minimun_auth_level'=>$request->get('auth_level')]);

	    	$module->privileges()->sync($request->get('privileges'));
            $module->createModulePrivilegeColumn();

            foreach (User::all() as $user) {
                $modulePrivilege = ModulePrivileges::whereUserId($user->id)->first();
                if($modulePrivilege){
                    $modulePriv = [1=>1,2=>3,3=>5,4=>5];
                    $modulePrivilege->update(['module_'.$module->id => $modulePriv[$user->auth_level_id]]);
                }
            }
	    	Session::flash('flash_message', 'Module added!');

	    	return redirect('admin/modules');
	        	
    }


    public function update(Requester $request)
    {

        $module = Module::find($request->get('module_id'));

    	$module->update(
                 ['name'=>$request->get('name'),
                 'description'=>$request->get('description'),
                 'route_segment'=>$request->get('route_segment'),
                 'type_id'=>$request->get('type'),
                 'minimun_auth_level'=>$request->get('auth_level')]);

            $module->privileges()->sync($request->get('privileges'));

            Session::flash('flash_message', 'Module Updated!');

            return redirect('admin/modules');
    }


}
