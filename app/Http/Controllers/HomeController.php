<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Branches;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function deleteModal($route,$msg, $callback, $param)
    {   
        $route = str_replace('-', '.', $route);
        return view('general.delete-modal')->withRoute($route)->withParam($param)->withMsg($msg)->withCallback($callback);
    }

}
