<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use Carbon\Carbon;
use App\Exceptions\Handler;
//Use Image;
use DB;
use Validator;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use App\MealMenu;
use App\MealCategory;
use App\MealTag;
use App\MealAttribute;
use App\PackSize;
use App\Order;
use App\OrderType;
use App\OrderStatus;
use App\Branches;
use App\User;
use App\OrderReceipt;
use App\VatNumber;
use App\MealPackSize;
use App\OrdersModifier;
use App\FoodModifier;

class KitchenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $getMealCategory = MealCategory::firstOrCreate(['name' => $input['meal_category']]);
        $getMealTag = MealTag::firstOrCreate(['name' => $input['meal_tag']]);

        if(!empty($input['meal_attribute'])){
          $getMealAttribute = MealAttribute::firstOrCreate(['name' => $input['meal_attribute'], 'meal_categories_id' => $getMealCategory->id, 'meal_tag_id' =>  $getMealTag->id])->id;
        }else{
          $getMealAttribute = null;
        }
        

        $getPackSize = PackSize::firstOrCreate(['name' => $input['pack_size']]);

        $path = 'uploads/mealmenu/';


        if(!empty($input['tag_picture'])){

          $imageTagFileName = "Meal-Menu-Tag-". kebab_case($input['meal_tag']) . '-'.Carbon::now()->format('Y-m-d'). "-". str_random(5).'.'.$input['tag_picture']->getClientOriginalExtension();

          Storage::disk('public')->putFileAs($path, new File($input['tag_picture']), $imageTagFileName); 

          $getMealTag->update(['tag_picture' => $imageTagFileName]); 

        }

        if(!empty($input['meal_attribute'])){
          $imageFileName = "Meal-Menu-". kebab_case($input['meal_attribute']) . '-'.Carbon::now()->format('Y-m-d'). "-". str_random(5).'.'.$input['meal_picture']->getClientOriginalExtension();

          Storage::disk('public')->putFileAs($path, new File($input['meal_picture']), $imageFileName);   

        }else{
          $imageFileName = $imageTagFileName; 
        }

                     

        $saveMealMenu = new MealMenu;

        $saveMealMenu->mealCategories()->associate($getMealCategory); 
        $saveMealMenu->mealTag()->associate($getMealTag); 

        $saveMealMenu->meal_attributes_id = $getMealAttribute;
        $saveMealMenu->meal_picture = $imageFileName;

        $saveMealMenu->save();  

        $saveMealPackSize = new MealPackSize;
        $saveMealPackSize->meal_menu_id = $saveMealMenu->id;
        $saveMealPackSize->pack_size_id  = $getPackSize->id;
        $saveMealPackSize->price  = $input['price'];
        $saveMealPackSize->save();

        return response()->json([
            'status' => 'success',
            'status_code' => '1001',
            'status_msg' => ' Meal Successfully added'
        ]);            

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getMealMenu(){

     $mealCat = MealCategory::pluck('name','id');
        $category = array();

        $mealMenu = array();
        $i = 0;
        foreach ($mealCat as $catKey => $catValue) {
          
          $category[$i]['id'] = $catKey;
          $category[$i]['name'] = $catValue;

          $mealTags = MealTag::where('meal_categories_id', $catKey)->get();

          $tag = array();
          $j = 0;
          foreach ($mealTags as $mealTag) {

            $tag[$j]['id'] = $mealTag->id;
            $tag[$j]['name'] = $mealTag->name;
            $tag[$j]['picture'] =  !empty($mealTag->tag_picture) ? asset('storage/uploads/mealmenu/'. $mealTag->tag_picture) : null;

            $meals = MealMenu::where('meal_tag_id', '=',  $mealTag->id)->whereNull('meal_attributes_id')->get();

            $k =0;
            foreach ($meals as $meal) {
              $packSizes = array();

              foreach ($meal->packSize as $packSize) {
                  $packSizes[] =  array('id'=> $packSize->pivot->id, 'name' => $packSize->name, 'price' => $packSize->pivot->price);
              }

              $tag[$j]['id'] = $meal->id;
              $tag[$j]['sizes'] = $packSizes;

              $k++;

            }

            $meals = MealMenu::where('meal_tag_id', '=', $mealTag->id)->whereNotNull('meal_attributes_id')->get();

            $k =0;
            foreach ($meals as $meal) {
              $attribute = MealAttribute::whereId($meal->meal_attributes_id)->where('meal_tag_id', '=', $mealTag->id)->first();


                $packSizes = array();

                foreach ($meal->packSize as $packSize) {
                    $packSizes[] =  array('id'=> $packSize->pivot->id, 'name' => $packSize->name, 'price' => $packSize->pivot->price);
                }

              $tag[$j]['meals'][] = array(

                  'id' => $meal->id,
                  'name' => $attribute->name,
                  'sizes' => $packSizes,
                  'meal_picture' => asset('storage/uploads/mealmenu/'. $meal->meal_picture)
                );

              $k++;

            }          


            $modifiers = array();

            foreach ($mealTag->foodModifier as $foodModifier) {
                $modifiers[] =  array('id'=> $foodModifier->pivot->id, 'name' => $foodModifier->name, 'price' => $foodModifier->pivot->price);
            }
            $tag[$j]['modifiers'] = $modifiers;

            $j++;

          }

          $category[$i]['category_items'] = $tag;

          $mealMenu = $category;

          $i++;
        }

        return response()->json([
            'status' => 'success',
            'status_code' => '1001',
            'status_msg' => 'Meal Menu Successfully retrieved',
            'meals' => $mealMenu
        ]);        
    }    

    public function placeOrder(Request $request){
        $input = $request->all();

        Log::debug($input);

        try{

            DB::beginTransaction();

            
            $getUser = User::findOrFail($input['user_id']);        
            $getBranch = Branches::findOrFail($input['branch_id']);

            $orderNo = Order::orderBy('created_at', 'DESC')->first();

            $orderNo = $orderNo ? $orderNo->order_no + 1 : 1;


            $orders = $input['orders'];

            $subTotal = 0;

            foreach ($orders as $order) {
                $getOrderType = OrderType::find($order['order_type']);
                $saveOrder = new Order;

                $getMealSize = MealPackSize::findOrFail($order['size_id']);
                $getMealMenu = MealMenu::findOrFail($order['meal_id']);

                $saveOrder->orderType()->associate($getOrderType); 
                $saveOrder->user()->associate($getUser); 
                $saveOrder->branches()->associate($getBranch);
                $saveOrder->mealMenu()->associate($getMealMenu);

                $saveOrder->order_no = $orderNo;
                $saveOrder->order_status_id = 1;
                $saveOrder->qty = $order['qty'];
                $saveOrder->meal_pack_size_id = $order['size_id']; 
                $saveOrder->unit_price = $order['unit_price'];
                $saveOrder->total_amt = $order['total_amt'];

                $subTotal += $saveOrder->total_amt;

                $saveOrder->save(); 

                $modifiers = !empty($order['modifiers']) ? $order['modifiers'] : [];

                foreach ($modifiers as $modifier) {
                  $getModifier = FoodModifier::findOrFail($modifier['id']);
                  $saveModifier = new OrdersModifier;

                  $saveModifier->order_no_id = $saveOrder->order_no;
                  $saveModifier->order_id = $saveOrder->id;
                  $saveModifier->foodModifier()->associate($getModifier);
                  $saveModifier->mealMenu()->associate($getMealMenu);
                  $saveModifier->qty = $modifier['qty'];
                  $saveModifier->unit_price = $modifier['unit_price'];
                  $saveModifier->total_amt = $modifier['total_amt'];

                  $subTotal += $saveModifier->total_amt;

                  $saveModifier->save();

                }
                
            }  

            $vatCalc = vatCalculator( $subTotal, 17.5);

            $tourLevy = levyCalculator($subTotal, 1);

            $genReceipt =  new OrderReceipt;

            $vatNo = VatNumber::orderBy('created_at', 'DESC')->first();

            $vatNo = $vatNo ? VatNumber::create(['number' => $vatNo->number + 1]): VatNumber::create(['number' => 1]);


            $genReceipt->vatNumber()->associate($vatNo);


           $genReceipt->order_no_id = $orderNo;
           
           $genReceipt->sub_total = $subTotal; 

           $genReceipt->vat_nhil_rate = "17.5%"; 

           $genReceipt->vat_nhil_amt = $vatCalc['taxAmt']; 
           $genReceipt->tourism_levy = '1%'; 
           $genReceipt->tourism_amt = $tourLevy; 
           $genReceipt->total = $subTotal + $vatCalc['taxAmt'] + $tourLevy; 
           $genReceipt->amt_paid = $input['amount_paid']; 
           $genReceipt->balance =  $input['amount_paid'] - $genReceipt->total; 


           $genReceipt->save();


           $receipt = OrderReceipt::whereId($genReceipt->id)->with(['vatNumber', 'order'])->get();

            $receipt->map(function ($name, $key) {
              foreach ($name->order as $key => $value) {
                
               $value->user = $value->user()->first();
                $value->branch = $value->branches()->first();

                $value->modifiers = OrdersModifier::where('order_no_id',$value->order_no)->where('meal_menu_id',$value->meal_menu_id)->where('order_id',$value->id)->select('id','qty','unit_price','total_amt')->get();
              }       


              unset($name->vat_number_id);
              unset($name->user_id);
              unset($name->branches_id);
              unset($name->order_no_id);

              return $name;
            });           

                    
            DB::commit();

            return response()->json([
                'status' => 'success',
                'status_code' => '1001',
                'status_msg' => 'Order Successfully placed',
                'order_receipt' => $receipt
            ]); 

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'status_code' => '1002',
                'status_msg' => ' unexpected error encountered placing order'
            ]);             
        }        

    }

    public function editOrder($orderNo, Request $request){
        $input = $request->all();

        Log::debug($input);

        try{

            DB::beginTransaction();

            $orderDetail = Order::where('order_no', $orderNo)->first();

            if(!$orderDetail){
                return response()->json([
                    'status' => 'error',
                    'status_code' => '1002',
                    'status_msg' => 'Order Details not founds'
                ]);                 
            }

            $getUser = User::find($input['user_id']);        
            $getBranch = Branches::find($input['branch_id']);


            $orders = $input['orders'];

            $subTotal = 0;

            foreach ($orders as $order) {
                $getOrderType = OrderType::find($order['order_type']);
                $saveOrder = new Order;

                $getMealSize = MealPackSize::findOrFail($order['size_id']);
                $getMealMenu = MealMenu::findOrFail($order['meal_id']);

                $saveOrder->orderType()->associate($getOrderType); 
                $saveOrder->user()->associate($getUser); 
                $saveOrder->branches()->associate($getBranch);
                $saveOrder->mealMenu()->associate($getMealMenu);

                $saveOrder->description = $order['description'];
                $saveOrder->order_status_id = 1;
                $saveOrder->qty = $order['qty']; 
                $saveOrder->meal_pack_size_id = $order['size_id']; 
                $saveOrder->unit_price = $order['unit_price'];
                $saveOrder->total_amt = $order['total_amt'];


                $subTotal += $saveOrder->total_amt;

                $saveOrder->save(); 

                $modifiers = !empty($order['modifiers']) ? $order['modifiers'] : [];

                foreach ($modifiers as $modifier) {
                  $getModifier = FoodModifier::findOrFail($modifier['id']);
                  $saveModifier = new OrdersModifier;

                  $saveModifier->order_no_id = $saveOrder->order_no;
                  $saveModifier->order_id = $saveOrder->id;
                  $saveModifier->foodModifier()->associate($getModifier);
                  $saveModifier->mealMenu()->associate($getMealMenu);
                  $saveModifier->qty = $modifier['qty'];
                  $saveModifier->unit_price = $modifier['unit_price'];
                  $saveModifier->total_amt = $modifier['total_amt'];

                  $subTotal += $saveModifier->total_amt;

                  $saveModifier->save();

                }

            }  

            $vatCalc = vatCalculator( $subTotal, 17.5);

            $tourLevy = levyCalculator($subTotal, 1);

            $genReceipt =  new OrderReceipt;

           $genReceipt->order_no_id = $orderNo;
           
           $genReceipt->sub_total = $subTotal; 

           $genReceipt->vat_nhil_rate = "17.5%"; 

           $genReceipt->vat_nhil_amt = $vatCalc['taxAmt']; 
           $genReceipt->tourism_levy = '1%'; 
           $genReceipt->tourism_amt = $tourLevy; 
           $genReceipt->total = $subTotal + $vatCalc['taxAmt'] + $tourLevy; 
           $genReceipt->amt_paid = $input['amount_paid']; 
           $genReceipt->balance =  $input['amount_paid'] - $genReceipt->total; 


           $genReceipt->save();


           $receipt = OrderReceipt::whereId($genReceipt->id)->with(['vatNumber', 'order'])->get();


            $receipt->map(function ($name, $key) {
              foreach ($name->order as $key => $value) {
                
               $value->user = $value->user()->first();
                $value->branch = $value->branches()->first();

                $value->modifiers = OrdersModifier::where('order_no_id',$value->order_no)->where('meal_menu_id',$value->meal_menu_id)->where('order_id',$value->id)->select('id','qty','unit_price','total_amt')->get();
              }       


              unset($name->vat_number_id);
              unset($name->user_id);
              unset($name->branches_id);
              unset($name->order_no_id);

              return $name;
            });          

                    
            DB::commit();

            return response()->json([
                'status' => 'success',
                'status_code' => '1001',
                'status_msg' => 'Order Successfully placed',
                'order_receipt' => $receipt
            ]); 

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'status_code' => '1002',
                'status_msg' => ' unexpected error encountered placing order'
            ]);             
        }        

    }    

    public function getOrders(){

        $orders = Order::all();

        $orders->map(function ($name, $key) {

          $name->order_type = $name->orderType()->first();
          $name->order_status = $name->orderStatus()->first();
          $name->user = $name->user()->first();
          $name->branch = $name->branches()->first();

          $name->modifiers = OrdersModifier::where('order_no_id',$name->order_no)->where('meal_menu_id',$name->meal_menu_id)->where('order_id',$name->id)->select('id','qty','unit_price','total_amt')->get();
          $mealDetail = MealMenu::whereId($name->meal_menu_id)->first();

            $mealCat = MealCategory::whereId($mealDetail->meal_categories_id)->pluck('name','id');
            $category = array();

            $mealMenu = array();
            $i = 0;
            foreach ($mealCat as $catKey => $catValue) {
              
              $category[$i]['id'] = $catKey;
              $category[$i]['name'] = $catValue;

              $mealTags = MealTag::where('meal_categories_id', $catKey)->whereId($mealDetail->meal_tag_id)->get();

              $tag = array();
              $j = 0;
              foreach ($mealTags as $mealTag) {

                $tag[$j]['id'] = $mealTag->id;
                $tag[$j]['name'] = $mealTag->name;
                $tag[$j]['picture'] =  !empty($mealTag->tag_picture) ? asset('storage/uploads/mealmenu/'. $mealTag->tag_picture) : null;

                $meals = MealMenu::where('meal_tag_id', '=',  $mealTag->id)->where('meal_categories_id', '=', $mealDetail->meal_categories_id)->whereId($mealDetail->id)->whereNull('meal_attributes_id')->get();

                $k =0;
                foreach ($meals as $meal) {
                  $packSizes = array();
                  foreach ($meal->packSize as $packSize) {
                      $packSizes[] =  array('id'=> $packSize->pivot->id, 'name' => $packSize->name, 'price' => $packSize->pivot->price);
                  }

                  $tag[$j]['id'] = $meal->id;
                  $tag[$j]['sizes'] = $packSizes;

                  $k++;

                }

                $meals = MealMenu::where('meal_tag_id', '=', $mealTag->id)->where('meal_categories_id', '=', $mealDetail->meal_categories_id)->whereNotNull('meal_attributes_id')->whereId($mealDetail->id)->get();

                $k =0;
                foreach ($meals as $meal) {
                  $attribute = MealAttribute::whereId($meal->meal_attributes_id)->where('meal_tag_id', '=', $mealTag->id)->where('meal_categories_id', '=', $mealDetail->meal_categories_id)->first();


                    $packSizes = array();

                    foreach ($meal->packSize as $packSize) {
                        $packSizes[] =  array('id'=> $packSize->pivot->id, 'name' => $packSize->name, 'price' => $packSize->pivot->price);
                    }

                  $tag[$j]['meals'][] = array(

                      'id' => $meal->id,
                      'name' => $attribute->name,
                      'sizes' => $packSizes,
                      'meal_picture' => asset('storage/uploads/mealmenu/'. $meal->meal_picture)
                    );

                  $k++;

                }          


                $modifiers = array();

                foreach ($mealTag->foodModifier as $foodModifier) {
                    $modifiers[] =  array('id'=> $foodModifier->pivot->id, 'name' => $foodModifier->name, 'price' => $foodModifier->pivot->price);
                }
                $tag[$j]['modifiers'] = $modifiers;

                $j++;

              }

              $category[$i]['category_items'] = $tag;

              $mealMenu = $category;

              $i++;
            }

            $name->meal_menu = $mealMenu;          

          unset($name->meal_menu_id);
          unset($name->meal_pack_size_id);
          unset($name->modifiers);
          unset($name->order_type_id);
          unset($name->order_status_id);
          unset($name->user_id);
          unset($name->branches_id);

          return $name;
        });          

        return response()->json([
            'status' => 'success',
            'status_code' => '1001',
            'status_msg' => 'Orders Successfully retrieved',
            'orders' => $orders
        ]);

    }

    public function getUserOrders($id){

        $orders = Order::where('user_id', '=', $id)->get();

        $orders->map(function ($name, $key) {

          $name->order_type = $name->orderType()->first();
          $name->order_status = $name->orderStatus()->first();
          $name->branch = $name->branches()->first();

          $name->modifiers = OrdersModifier::where('order_no_id',$name->order_no)->where('meal_menu_id',$name->meal_menu_id)->where('order_id',$name->id)->select('id','qty','unit_price','total_amt')->get();

          $mealDetail = MealMenu::whereId($name->meal_menu_id)->first();

            $mealCat = MealCategory::whereId($mealDetail->meal_categories_id)->pluck('name','id');
            $category = array();

            $mealMenu = array();
            $i = 0;
            foreach ($mealCat as $catKey => $catValue) {
              
              $category[$i]['id'] = $catKey;
              $category[$i]['name'] = $catValue;

              $mealTags = MealTag::where('meal_categories_id', $catKey)->whereId($mealDetail->meal_tag_id)->get();

              $tag = array();
              $j = 0;
              foreach ($mealTags as $mealTag) {

                $tag[$j]['id'] = $mealTag->id;
                $tag[$j]['name'] = $mealTag->name;
                $tag[$j]['picture'] =  !empty($mealTag->tag_picture) ? asset('storage/uploads/mealmenu/'. $mealTag->tag_picture) : null;

                $meals = MealMenu::where('meal_tag_id', '=',  $mealTag->id)->where('meal_categories_id', '=', $mealDetail->meal_categories_id)->whereId($mealDetail->id)->whereNull('meal_attributes_id')->get();

                $k =0;
                foreach ($meals as $meal) {
                  $packSizes = array();
                  foreach ($meal->packSize as $packSize) {
                      $packSizes[] =  array('id'=> $packSize->pivot->id, 'name' => $packSize->name, 'price' => $packSize->pivot->price);
                  }

                  $tag[$j]['id'] = $meal->id;
                  $tag[$j]['sizes'] = $packSizes;

                  $k++;

                }

                $meals = MealMenu::where('meal_tag_id', '=', $mealTag->id)->where('meal_categories_id', '=', $mealDetail->meal_categories_id)->whereNotNull('meal_attributes_id')->whereId($mealDetail->id)->get();

                $k =0;
                foreach ($meals as $meal) {
                  $attribute = MealAttribute::whereId($meal->meal_attributes_id)->where('meal_tag_id', '=', $mealTag->id)->where('meal_categories_id', '=', $mealDetail->meal_categories_id)->first();


                    $packSizes = array();

                    foreach ($meal->packSize as $packSize) {
                        $packSizes[] =  array('id'=> $packSize->pivot->id, 'name' => $packSize->name, 'price' => $packSize->pivot->price);
                    }

                  $tag[$j]['meals'][] = array(

                      'id' => $meal->id,
                      'name' => $attribute->name,
                      'sizes' => $packSizes,
                      'meal_picture' => asset('storage/uploads/mealmenu/'. $meal->meal_picture)
                    );

                  $k++;

                }          


                $modifiers = array();

                foreach ($mealTag->foodModifier as $foodModifier) {
                    $modifiers[] =  array('id'=> $foodModifier->pivot->id, 'name' => $foodModifier->name, 'price' => $foodModifier->pivot->price);
                }
                $tag[$j]['modifiers'] = $modifiers;

                $j++;

              }

              $category[$i]['category_items'] = $tag;

              $mealMenu = $category;

              $i++;
            }

            $name->meal_menu = $mealMenu;          

          unset($name->meal_menu_id);
          unset($name->meal_pack_size_id);
          unset($name->modifiers);
          unset($name->order_type_id);
          unset($name->order_status_id);
          unset($name->user_id);
          unset($name->branches_id);

          return $name;
        });          

        return response()->json([
            'status' => 'success',
            'status_code' => '1001',
            'status_msg' => 'User Orders Successfully retrieved',
            'orders' => $orders
        ]);

    }    

    public function CancelOrder($orderNo){
        try{

            DB::beginTransaction();

            $cancelled = OrderStatus::where('name', '=', 'Cancelled')->first();

            Order::where('order_no', $orderNo)->update(['order_status_id' => $cancelled->id]);
            
            DB::commit();

         return response()->json([
            'status' => 'success',
            'status_code' => '1001',
            'status_msg' => 'Order Successfully canceled',
        ]);   

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'status_code' => '1002',
                'status_msg' => 'unknown order no'
            ]);             
        } 

      
    }

    public function getOrderStatus(){
        $orderStatus = OrderStatus::all();
        return response()->json([
            'status' => 'success',
            'status_code' => '1001',
            'status_msg' => 'Orders Statuses Successfully retrieved',
            'status_extra' => $orderStatus
        ]);      
    }

    public function getOrderType(){
        $orderType = OrderType::all();
        return response()->json([
            'status' => 'success',
            'status_code' => '1001',
            'status_msg' => 'Orders Types Successfully retrieved',
            'status_extra' => $orderType
        ]);       
    }
}
