<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Order;
use App\Branches;
use App\Employee;
use App\OrderStatus;
use App\JobRole;
use App\OrderType;
use App\PackSize;
use App\MealMenu;
use App\MealCategory;
use App\MealTag;
use App\MealAttribute;
use App\MealPackSize;
use App\OrdersModifier;
use App\FoodModifier;

use DataTables;
use Log;
use DB;
use Carbon\Carbon;
use App\Exceptions\Handler;
use Validator;

class AdminModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $branches = Branches::pluck('name','id')->prepend('All Locations', 'all');

        return view('admin.index')->withBranches($branches);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dashboardWidgets($type, $period = null, Request $request){
      $input = $request->all();

      $cancelled = OrderStatus::where('name', '=', 'Cancelled')->first();

      $orders = Order::with(['orderStatus', 'orderType'])->where('order_status_id', '!=', $cancelled->id);

      $location = !empty($input['location']) && strtolower($input['location']) != 'all' ? $input['location'] : null;


      if(!empty($location)){

          $orders->whereHas('branches', function ($query) use ($location) {
              $query->whereId($location);
          });         
      }      

      switch ($type) {
        case 1:
            $miniwidget = [];

            $miniwidget['Total Sales Today'] =  'GHS '. with(clone $orders)->whereDate('created_at', Carbon::now()->format('Y-m-d'))->sum('total_amt');

            $miniwidget['Total Revenue Today'] =  'GHS 10';

            $miniwidget['Total Order Today'] =  with(clone $orders)->whereDate('created_at', Carbon::now()->format('Y-m-d'))->distinct('order_no')->count('order_no');


            $miniwidget['Average Time Food Takes in Kitchen'] =  with(clone $orders)->whereDate('created_at', Carbon::now()->format('Y-m-d'))->distinct('order_no')->count('order_no');



            return view('widgets.mini-widget')->withMiniWidgets($miniwidget)->withCol(2);
        break;
        case 2:

            $chart = array();

            $chartName = 'SALES AND REVENUE STATISTICS';
         
            $category = array();

            $query = Order::where('order_status_id', '!=', $cancelled->id);  


            switch ($period) {
              case 1:

                    $date =Carbon::now()->startOfDay();
                    $end = Carbon::now()->endOfDay();
                    $period = array();

                    while ($date <= $end) {

                        $period[] = $date->copy()->format('H:i');

                        $date->addHours(1);
                    }


                  foreach ($period  as $labels) {
                      $chart['labels'][] = $labels;


                      $category[] = $labels;
                  }                    
                 

                for ($i=0; $i < count($category); $i++) { 

                  $chart['dataset']['Sales'][] = with(clone $query)
                    ->where(DB::raw('HOUR(created_at)'), '=', $category[$i])
                    ->whereDate('created_at', Carbon::now())
                    ->sum('total_amt'); 

/*                  $chart['dataset']['Revenues'][] = with(clone $query)
                    ->where(DB::raw('HOUR(created_at)'), '=', $category[$i])
                    ->sum('total_amt'); */                     
                } 

                $chartTitle = 'Today Revenue and Sales';
                $chartxAxes = 'Hourly Revenue and Sales';              

                break;

              case 2:
                $getLabels = $orders->select(DB::raw('DATE_FORMAT(created_at, "%d %b %y") as labels'))       
                ->orderBy('labels')
                ->groupBy('labels')->whereBetween('created_at', [Carbon::now()->subDays(13), Carbon::now()]); 


                $data = $getLabels->pluck('labels');


              foreach ($data as $labels) {
                $chart['labels'][] = Carbon::parse($labels)->copy()->format('d M');


                $category[] = Carbon::parse($labels)->format('Y-m-d');
              }  

                for ($i=0; $i < count($category); $i++) { 

                  $chart['dataset']['Sales'][] = with(clone $query)
                    ->whereDate('created_at', $category[$i])
                    ->sum('total_amt'); 

                  // $chart['dataset']['Revenue'][] = with(clone $query)
                  //   ->whereDate('created_at', $category[$i])
                  //   ->sum('total_amt');                      
                }  

                $chartTitle = 'Daily Revenue and Sales for the Last 14 days';
                $chartxAxes = 'Daily Revenue and Sales';
                                               

              break;

              case 3:
                $getLabels = $orders->select(DB::raw('DATE_FORMAT(created_at, "%M %y") as labels'))
                ->orderBy('labels')
                ->groupBy('labels');

                $data = $getLabels->pluck('labels');

                foreach ($data as $labels) {

                  $chart['labels'][] = Carbon::parse($labels)->copy()->format('M-Y');


                  $category[] = Carbon::parse($labels)->format('Y-m-d');

                }

                for ($i=0; $i < count($category); $i++) { 

                  $chart['dataset']['Sales'][] = with(clone $query)
                  ->whereMonth('created_at', '=', Carbon::parse($category[$i])->copy()->format('m'))
                  ->whereYear('created_at', '=', Carbon::parse($category[$i])->copy()->format('Y'))
                  ->sum('total_amt');

/*                  $chart['dataset']['Revenues'][] = with(clone $query)
                  ->whereMonth('created_at', '=', Carbon::parse($category[$i])->copy()->format('m'))
                  ->whereYear('created_at', '=', Carbon::parse($category[$i])->copy()->format('Y'))
                  ->sum('total_amt');  */                   
                }  

                $chartTitle = 'Monthly Sales and Revenue';
                $chartxAxes = 'Sales This Month';
                $chartyAxes = 'Monthly Revenue and Sale';

              break;

              default:
                $getLabels = $orders->select(DB::raw('DATE_FORMAT(created_at, "%d %b %y") as labels'))
                ->orderBy('labels')
                ->groupBy('labels')->whereBetween('created_at', [Carbon::now()->subDays(13), Carbon::now()]); 

                $data = $getLabels->pluck('labels');


              foreach ($data as $labels) {
                $chart['labels'][] = Carbon::parse($labels)->copy()->format('d M');


                $category[] = Carbon::parse($labels)->format('Y-m-d');
              }  

                for ($i=0; $i < count($category); $i++) { 

                  $chart['dataset']['Sales'][] = with(clone $query)
                    ->whereDate('created_at', $category[$i])
                    ->sum('total_amt'); 

                  // $chart['dataset']['Revenue'][] = with(clone $query)
                  //   ->whereDate('created_at', $category[$i])
                  //   ->sum('total_amt');                      
                }  

                $chartTitle = 'Daily Revenue and Sales for the Last 14 days';
                $chartxAxes = 'Daily Revenue and Sales';
                
                break;              
            }

              $chartyAxes = 'Amount in Ghs';
              $current = 'Sales';
              //$previous = "Revenues";               

              if(!empty($chart['dataset']) && count($chart['dataset']) > 0){
                  $chart['type'] = 'line';

                  $chart["datasetColor"][$current]["bgcolor"] = '#FBBC2D';

                  $chart["datasetColor"][$current]["bordercolor"] = '#FBBC2D'; 


                  // $chart["datasetColor"][$previous]["bgcolor"] = '#4F67D1';

                  // $chart["datasetColor"][$previous]["bordercolor"] = '#4F67D1';                               
                  
                  $chart['config']['title_display'] = true;
                  $chart['config']['title'] = $chartTitle;

                  $chart['config']['legend_display'] = true;
                  $chart['config']['legend_position'] = 'bottom';

                  $chart['config']['scales_xAxes']['display'] = true;
                  $chart['config']['scales_xAxes']['label'] = $chartxAxes;

                  $chart['config']['scales_yAxes']['display'] = false;
                  $chart['config']['scales_yAxes']['label'] = $chartyAxes;                                
              }

            return view('widgets.chart-widget')->withChart($chart)->withChartName($chartName);       
        break;


        case 3:

            $chart = array();

            $chartName = 'SALES COMPARISON BY FOOD CATEGORY';


            $category = array();

            $query = Order::where('order_status_id', '!=', $cancelled->id);            

            switch ($period) {
              case 1:
                $getLabels = $orders->select('meal_menu_id as labels')
                  ->groupBy('labels')->whereBetween('created_at', [Carbon::yesterday()->format('Y-m-d'),  Carbon::now()->format('Y-m-d')]);

                $data = $getLabels->pluck('labels');

                $data = MealMenu::whereIn('id', $data)->get();



                foreach ($data as $label) {

                  if(!empty($label->meal_attributes_id)){

                    $meal = MealAttribute::whereId($label->meal_attributes_id)->where('meal_tag_id', '=', $label->meal_tag_id)->first();
                    $chart['labels'][] = $meal->name;

                  }else{
                    $meal = MealTag::whereId($label->meal_tag_id)->first();

                    $chart['labels'][] = $meal->name;
                  }

                  $category[] = $label->id;
                } 


                for ($i=0; $i < count($category); $i++) { 

                  $chart['dataset']['Today'][] = with(clone $query)
                    ->where( 'meal_menu_id', $category[$i])
                    ->whereDate('created_at', '=', Carbon::now()->format('Y-m-d'))
                    ->count(); 

                  $chart['dataset']['Yesterday'][] = with(clone $query)
                    ->where( 'meal_menu_id', $category[$i])
                    ->whereDate('created_at', '=', Carbon::yesterday()->format('Y-m-d'))
                    ->count();                     
                } 

                $chartTitle = 'Sales By Food Category as compared to Yesterday';
                $chartxAxes = 'Sales Today';
                $chartyAxes = 'Sales Yesterday';
                $current = 'Today';
                $previous = "Yesterday";                 

                break;

              case 2:
                $getLabels = $orders->select('meal_menu_id as labels')
                  ->groupBy('labels')->whereBetween('created_at', [Carbon::now()->subWeek()->startOfWeek(), Carbon::now()->endOfWeek()]); 

                $data = $getLabels->pluck('labels');

                $data = MealMenu::whereIn('id', $data)->get();


                foreach ($data as $label) {

                  if(!empty($label->meal_attributes_id)){
                    
                    $meal = MealAttribute::whereId($label->meal_attributes_id)->where('meal_tag_id', '=', $label->meal_tag_id)->first();
                    $chart['labels'][] = $meal->name;

                  }else{
                    $meal = MealTag::whereId($label->meal_tag_id)->first();

                    $chart['labels'][] = $meal->name;
                  }

                  $category[] = $label->id;
                }  

                for ($i=0; $i < count($category); $i++) { 

                  $chart['dataset']['This Week'][] = with(clone $query)
                    ->where( 'meal_menu_id', $category[$i])
                    ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                    ->count(); 

                  $chart['dataset']['Last Week'][] = with(clone $query)
                    ->where( 'meal_menu_id', $category[$i])
                    ->whereBetween('created_at', [Carbon::now()->subWeek()->startOfWeek(), Carbon::now()->subWeek()->endOfWeek()])
                    ->count();                     
                }  

                $chartTitle = 'Sales By Food Category as compared to Last Week';
                $chartxAxes = 'Sales This Week';
                $chartyAxes = 'Sales Last Week';
                $current = 'This Week';
                $previous = "Last Week";                                 

              break;

              case 3:
                $getLabels = $orders->select('meal_menu_id as labels')
                  ->groupBy('labels')->whereBetween('created_at', [Carbon::now()->subMonth()->startOfMonth(), Carbon::now()->endOfMonth()]);

                $data = $getLabels->pluck('labels');

                $data = MealTag::whereIn('id', $data)->pluck('name', 'id');


                foreach ($data as $labelId => $labelName) {
                  $chart['labels'][] = $labelName;


                  $category[] = $labelId;
                }   

                for ($i=0; $i < count($category); $i++) { 

                  $chart['dataset']['This Month'][] = with(clone $query)
                    ->where( 'meal_menu_id', $category[$i])
                    ->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
                    ->count(); 

                  $chart['dataset']['Last Month'][] = with(clone $query)
                    ->where( 'meal_menu_id', $category[$i])
                    ->whereBetween('created_at', [Carbon::now()->subMonth()->startOfMonth(), Carbon::now()->subMonth()->endOfMonth()])
                    ->count();                     
                }  

                $chartTitle = 'Sales By Food Category as compared to Last Month';
                $chartxAxes = 'Sales This Month';
                $chartyAxes = 'Sales Last Month';
                $current = 'This Month';
                $previous = "Last Month"; 

              break;

              default:
                $getLabels = $orders->select('meal_menu_id as labels')
                  ->groupBy('labels')->whereBetween('created_at', [Carbon::now()->subWeek()->startOfWeek(), Carbon::now()->endOfWeek()]); 

                $data = $getLabels->pluck('labels');

                $data = MealMenu::whereIn('id', $data)->get();

                foreach ($data as $label) {

                  if(!empty($label->meal_attributes_id)){
                    
                    $meal = MealAttribute::whereId($label->meal_attributes_id)->where('meal_tag_id', '=', $label->meal_tag_id)->first();
                    $chart['labels'][] = $meal->name;

                  }else{
                    $meal = MealTag::whereId($label->meal_tag_id)->first();

                    $chart['labels'][] = $meal->name;
                  }

                  $category[] = $label->id;
                }   

                for ($i=0; $i < count($category); $i++) { 

                  $chart['dataset']['This Week'][] = with(clone $query)
                    ->where( 'meal_menu_id', $category[$i])
                    ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                    ->count(); 

                  $chart['dataset']['Last Week'][] = with(clone $query)
                    ->where( 'meal_menu_id', $category[$i])
                    ->whereBetween('created_at', [Carbon::now()->subWeek()->startOfWeek(), Carbon::now()->subWeek()->endOfWeek()])
                    ->count();                     
                }  

                $chartTitle = 'Sales By Food Category as compared to Last Week';
                $chartxAxes = 'Sales This Week';
                $chartyAxes = 'Sales Last Week';
                $current = 'This Week';
                $previous = "Last Week";                

                break;              
            }


              if(!empty($chart['dataset']) && count($chart['dataset']) > 0){
                  $chart['type'] = 'bar';

                  $chart["datasetColor"][$current]["bgcolor"] = '#FBBC2D';

                  $chart["datasetColor"][$current]["bordercolor"] = '#FBBC2D'; 


                  $chart["datasetColor"][$previous]["bgcolor"] = '#DDDDDD';

                  $chart["datasetColor"][$previous]["bordercolor"] = '#DDDDDD';                               
                  
                  $chart['config']['title_display'] = true;
                  $chart['config']['title'] = $chartTitle;

                  $chart['config']['legend_display'] = true;
                  $chart['config']['legend_position'] = 'bottom';

                  $chart['config']['scales_xAxes']['display'] = true;
                  $chart['config']['scales_xAxes']['label'] = $chartxAxes;

                  $chart['config']['scales_yAxes']['display'] = false;
                  $chart['config']['scales_yAxes']['label'] = $chartyAxes;                                
              }

            return view('widgets.chart-widget')->withChart($chart)->withChartName($chartName);          
        break;

        case 4:

            $chart = array();

            $chartName = 'Items Solds';

            $getLabels = $orders->select(DB::raw('DATE_FORMAT(created_at, "%d %b %y") as labels'))
              ->orderBy(DB::raw('labels'))
              ->groupBy('labels')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);

              $data = $getLabels->pluck('labels');

              $category = array();

              foreach ($data as $labels) {
                $chart['labels'][] = Carbon::parse($labels)->copy()->format('l');


                $category[] = Carbon::parse($labels)->format('Y-m-d');
              }   



              $query = Order::groupBy('order_no')->where('order_status_id', '!=', $cancelled->id);


              for ($i=0; $i < count($category); $i++) { 

                $chart['dataset']['Orders'][] = with(clone $query)
                  ->whereDate('created_at', '=', $category[$i])
                    ->sum('total_amt');

              } 


              if(!empty($chart['dataset']) && count($chart['dataset']) > 0){
                  $chart['type'] = 'line';

                  $chart["datasetColor"]['Orders']["bgcolor"] = 'rgba(79, 103, 209,0.2)';

                  $chart["datasetColor"]['Orders']["bordercolor"] = 'rgba(79, 103, 209,1)';             
                  
                  $chart['config']['title_display'] = true;
                  $chart['config']['title'] = 'Number of Items Sold This week';

                  $chart['config']['legend_display'] = false;
                  $chart['config']['legend_position'] = 'bottom';

                  $chart['config']['scales_xAxes']['display'] = true;
                  $chart['config']['scales_xAxes']['label'] = 'Days of the Week';

                  $chart['config']['scales_yAxes']['display'] = true;
                  $chart['config']['scales_yAxes']['label'] = 'Number of Items';                                
              }                                            

            return view('widgets.chart-widget')->withChart($chart)->withChartName($chartName);
          break;

        case 5:

        $orders->select(DB::raw('SUM(qty) as qty, SUM(total_amt) as total_amt, meal_menu_id'))->groupBy('meal_menu_id')->orderBy('total_amt', 'desc')->orderBy('qty', 'desc')->limit(5);

        return DataTables::collection($orders->get())
                ->addColumn('Food Item', function ($model) {

                    $meal = MealMenu::whereId($model->meal_menu_id)->first();
                    if(!empty($meal->meal_attributes_id)){
                      $meal = MealAttribute::whereId($meal->meal_attributes_id)->where('meal_tag_id', '=', $meal->meal_tag_id)->first();

                    }else{
                      $meal = MealTag::whereId($meal->meal_tag_id)->first();
                    }
                    return $meal->name;

                })
                ->addColumn('Qty Sold', function ($model) {
                    return $model->qty;

                })
                ->addColumn('Sales', function ($model) {
                    return 'Ghs'. $model->total_amt;

                }) 
                ->escapeColumns([])->smart(true)->make(true);
          break;
        
        default:
          # code...
          break;
      }

    }

    public function salesOrder(){
        $branches = Branches::pluck('name','id')->prepend('All Locations', 'all');
        return view('admin.sales-order')->withBranches($branches);
    }

    public function widget($type, $widgetType, Request $request){
        Log::debug($request);

        $input = $request->all();

        Log::debug($input);


        switch ($type) {
            case 'sales':
                    $startDate = isset($input['from_date']) ? $input['from_date'] : '1972-01-01';
                    $endDate = isset($input['to_date']) ? $input['to_date'] : Carbon::now()->format('Y-m-d');
                    

                    $orders = Order::whereBetween('created_at',[$startDate, $endDate]);

                    

                    $location = !empty($input['location']) && strtolower($input['location']) != 'all' ? $input['location'] : null;

                    if(!empty($location)){

                        $orders->whereHas('branches', function ($query) use ($location) {
                            $query->whereId($location);
                        });         
                    }

                    

                    switch ($widgetType) {
                        case 1:
                            $miniwidget = [];
                            $miniwidget['Total Sales'] = 'Ghs '. with(clone $orders)->distinct('order_no')->count('order_no');


                            $delayed = OrderStatus::where('name', '=', 'Delayed')->first();

                            $miniwidget['Total Profit From Sales'] = 'Ghs '. with(clone $orders)->distinct('order_no')->where('order_status_id', '=', $delayed->id)->count('order_no');


                            return view('widgets.mini-widget')->withMiniWidgets($miniwidget)->withCol(2);

                            break;
                        
                        case 2:
                            $i = 1;
                            return DataTables::collection($orders->groupBy('meal_menu_id')->get())
                                    ->addColumn('No', function ($order) use(&$i) {
                                        return $i++;

                                    }) 
                                    ->addColumn('Sales Item', function ($order) {

                                        $meal = MealMenu::whereId($order->meal_menu_id)->first();

                                        if(!empty($meal->meal_attributes_id)){
                                          $meal = MealAttribute::whereId($meal->meal_attributes_id)->where('meal_tag_id', '=', $meal->meal_tag_id)->first();

                                        }else{
                                          $meal = MealTag::whereId($meal->meal_tag_id)->first();
                                        }
                                        return $meal->name;


                                    })
                                    ->addColumn('Date of Sales', function ($order) {
                                        return Carbon::parse($order->created_at)->format('d/m/Y - h:i a ');

                                    }) 
                                    ->addColumn('Qty Sold', function ($order) {

                                        return $order->sum('qty');

                                    })
                                    ->addColumn('Amount', function ($order) {

                                        return $order->sum('total_amt');

                                    })                              

                                    ->escapeColumns([])->smart(true)->make(true);
                                                break;
                                        }

                        case 3:
                            $cancelled = OrderStatus::where('name', '=', 'Cancelled')->first();
                            $delayed = OrderStatus::where('name', '=', 'Delayed')->first();

                            $chart = array();

                            $chartName = 'Sales' ;

                            $getLabels = $orders->select(DB::raw('DATE_FORMAT(created_at, "%d %b %y") as labels'))
                              ->orderBy(DB::raw('labels'))
                              ->groupBy('labels');

                              $data = $getLabels->pluck('labels');

                              $category = array();

                              foreach ($data as $labels) {
                                $chart['labels'][] = $labels;


                                $category[] = Carbon::parse($labels)->format('Y-m-d');
                              }   

                              $query = Order::groupBy('order_no');


                              for ($i=0; $i < count($category); $i++) { 

                                $chart['dataset']['Orders'][] = with(clone $query)
                                  ->whereDate('created_at', '=', $category[$i])
                                    ->sum('total_amt');

                                $chart['dataset']['Delayed Order'][] = with(clone $query)
                                  ->whereDate('created_at', '=', $category[$i])
                                  ->where('order_status_id', '=', $delayed->id)
                                  ->sum('total_amt');

                              } 


                              if(!empty($chart['dataset']) && count($chart['dataset']) > 0){
                                  $chart['type'] = 'line';

                                  $chart["datasetColor"]['Orders']["bgcolor"] = 'rgba(79, 103, 209,0.2)';

                                  $chart["datasetColor"]['Orders']["bordercolor"] = 'rgba(79, 103, 209,1)';

                                  $chart["datasetColor"]['Delayed Order']["bgcolor"] = 'rgba(251, 188, 45,0.2)';
                                  
                                  $chart["datasetColor"]['Delayed Order']["bordercolor"] = 'rgba(251, 188, 45,1)'; 
                               
                                  
                                  $chart['config']['legend_display'] = true;
                                  $chart['config']['title'] = 'Daily and Revenue Sales for the Last 12 days';

                                  $chart['config']['legend_position'] = 'bottom';

                                  $chart['config']['scales_xAxes']['display'] = true;
                                  $chart['config']['scales_xAxes']['label'] = 'Date';

                                  $chart['config']['scales_yAxes']['display'] = true;
                                  $chart['config']['scales_yAxes']['label'] = 'Amount in Ghs';                                
                              }                                             

                            return view('widgets.chart-widget')->withChart($chart)->withChartName($chartName);
                            
                            break;                                        

                break;

            case 'orders':

                    $startDate = isset($input['from_date']) ? $input['from_date'] : '1972-01-01';
                    $endDate = isset($input['to_date']) ? $input['to_date'] : Carbon::now()->format('Y-m-d');
                    

                    $orders = Order::whereBetween('created_at',[$startDate, $endDate]);

                    $location = !empty($input['location']) && strtolower($input['location']) != 'all' ? $input['location'] : null;

                    if(!empty($location)){

                        $orders->whereHas('branches', function ($query) use ($location) {
                            $query->whereId($location);
                        });         
                    }

                    

                    switch ($widgetType) {
                        case 1:
                            $miniwidget = [];
                            $miniwidget['Total Orders'] =  with(clone $orders)->distinct('order_no')->count('order_no');


                            $delayed = OrderStatus::where('name', '=', 'Delayed')->first();

                            $miniwidget['Delayed Order'] = with(clone $orders)->distinct('order_no')->where('order_status_id', '=', $delayed->id)->count('order_no');

                            $cancelled = OrderStatus::where('name', '=', 'Cancelled')->first();

                            $miniwidget['Cancelled Order'] = with(clone $orders)->distinct('order_no')->where('order_status_id', '=', $cancelled->id)->count('order_no');

                            return view('widgets.mini-widget')->withMiniWidgets($miniwidget)->withCol(2);

                            break;
                        
                        case 2:
                            $i = 1;
                            return DataTables::collection($orders->get())
                                    ->addColumn('No', function ($order) use(&$i) {
                                        return $i++;

                                    }) 
                                    ->addColumn('Order Item', function ($order) {
                                        $meal = MealMenu::whereId($order->meal_menu_id)->first();

                                        if(!empty($meal->meal_attributes_id)){
                                          $meal = MealAttribute::whereId($meal->meal_attributes_id)->where('meal_tag_id', '=', $meal->meal_tag_id)->first();

                                        }else{
                                          $meal = MealTag::whereId($meal->meal_tag_id)->first();
                                        }
                                        return $meal->name;                                        

                                    })
                                    ->addColumn('Date of Order', function ($order) {
                                        return Carbon::parse($order->created_at)->format('d/m/Y - h:i a ');

                                    }) 
                                    ->addColumn('Order Status', function ($order) {

                                        return "<span style='background:".$order->orderStatus->color_indicator."' class='table-indicator'>". $order->orderStatus->name ."</span>";

                                    })
                                    ->addColumn('Waiter Name', function ($order) {
                                        $fullname = $order->user->other_name . ' '. $order->user->surname;

                                        return $fullname;

                                    }) 

                                    ->addColumn('Order', function ($order) {
                                        return $order->order_no;

                                    })                               

                                    ->escapeColumns([])->smart(true)->make(true);
                                
                            break;


                        case 3:
                            $cancelled = OrderStatus::where('name', '=', 'Cancelled')->first();
                            $delayed = OrderStatus::where('name', '=', 'Delayed')->first();

                            $chart = array();

                            $chartName = 'Orders' ;

                            $getLabels = $orders->select(DB::raw('DATE_FORMAT(created_at, "%d %b %y") as labels'))
                              ->orderBy(DB::raw('labels'))
                              ->groupBy('labels');

                              $data = $getLabels->pluck('labels');

                              $category = array();

                              foreach ($data as $labels) {
                                $chart['labels'][] = $labels;


                                $category[] = Carbon::parse($labels)->format('Y-m-d');
                              }   

                              $query = Order::groupBy('order_no');


                              for ($i=0; $i < count($category); $i++) { 

                                $chart['dataset']['Orders'][] = with(clone $query)
                                  ->whereDate('created_at', '=', $category[$i])
                                    ->sum('total_amt');

                                $chart['dataset']['Delayed Order'][] = with(clone $query)
                                  ->whereDate('created_at', '=', $category[$i])
                                  ->where('order_status_id', '=', $delayed->id)
                                  ->sum('total_amt');

                                $chart['dataset']['Cancelled Order'][] = with(clone $query)
                                  ->whereDate('created_at', '=', $category[$i])
                                  ->where('order_status_id', '=', $cancelled->id)
                                  ->sum('total_amt');

                              } 


                              if(!empty($chart['dataset']) && count($chart['dataset']) > 0){
                                  $chart['type'] = 'line';

                                  $chart["datasetColor"]['Orders']["bgcolor"] = 'rgba(79, 103, 209,0.2)';

                                  $chart["datasetColor"]['Orders']["bordercolor"] = 'rgba(79, 103, 209,1)';

                                  $chart["datasetColor"]['Delayed Order']["bgcolor"] = 'rgba(251, 188, 45,0.2)';
                                  
                                  $chart["datasetColor"]['Delayed Order']["bordercolor"] = 'rgba(251, 188, 45,1)'; 


                                  $chart["datasetColor"]['Cancelled Order']["bgcolor"] = 'rgba(238, 92, 144,0.2)';
                                  
                                  $chart["datasetColor"]['Cancelled Order']["bordercolor"] = 'rgba(238, 92, 144,1)';                               
                                  
                                  $chart['config']['legend_display'] = true;
                                  $chart['config']['title'] = 'Daily and Revenue Sales for the Last 12 days';

                                  $chart['config']['legend_position'] = 'bottom';

                                  $chart['config']['scales_xAxes']['display'] = true;
                                  $chart['config']['scales_xAxes']['label'] = 'Date';

                                  $chart['config']['scales_yAxes']['display'] = true;
                                  $chart['config']['scales_yAxes']['label'] = 'Amount in Ghs';                                
                              }                                             

                            return view('widgets.chart-widget')->withChart($chart)->withChartName($chartName);

                            break;

                        }
                        
                        break;                      

                break;

            default:
                # code...
                break;
        }
    }  

    public function getBranches(){
        $branches = Branches::all();
        return response()->json([
            'status' => 'success',
            'status_code' => '1001',
            'status_msg' => 'Branches Successfully retrieved',
            'status_extra' => $branches
        ]);         
    }

    public function getJobs(){
        $jobs = JobRole::all();
        return response()->json([
            'status' => 'success',
            'status_code' => '1001',
            'status_msg' => 'Jobs Successfully retrieved',
            'status_extra' => $jobs
        ]);         
    } 

    public function getPackages(){
      $packages = PackSize::all();
        return response()->json([
            'status' => 'success',
            'status_code' => '1001',
            'status_msg' => 'packages Successfully retrieved',
            'status_extra' => $packages
        ]); 
    }  


    public function adminReport(){

        $branches = Branches::pluck('name','id')->prepend('All Locations', 'all');

        return view('admin.report')->withBranches($branches);
    }

    public function adminReportPartial($reportType, $type, Request $request){
      $input = $request->all();

      $startDate = isset($input['from_date']) ? Carbon::parse($input['from_date']) : Carbon::now()->startOfMonth();
      $endDate = isset($input['to_date']) ? Carbon::parse($input['to_date']) : Carbon::now();

      $location = !empty($input['location']) && strtolower($input['location']) != 'all' ? $input['location'] : null;     

        switch ($reportType) {
            case 'sales':
                $cancelled = OrderStatus::where('name', '=', 'Cancelled')->first();

                $query = Order::where('order_status_id', '!=', $cancelled->id);

                if(!empty($location)){

                  $query->whereHas('branches', function ($query) use ($location) {
                      $query->whereId($location);
                  });         
                }                 

                 switch ($type) {
                   case 1:
                      $chart = array();

                      $chartName = 'Sales Overview';


                      $category = array();

                      $period = array();
                      while ($startDate <= $endDate) {

                          $period[] = $startDate->copy()->format('Y-m-d');

                          $startDate->addDays(1);
                      }                    

                      foreach ($period  as $labels) {
                        $chart['labels'][] = Carbon::parse($labels)->copy()->format('d M y');


                        $category[] = Carbon::parse($labels)->format('Y-m-d');
                      } 

                      for ($i=0; $i < count($category); $i++) { 

                        $chart['dataset']['Sales - Ghs '][] = with(clone $query)
                          ->whereDate('created_at', '=', $category[$i])
                          ->sum('total_amt'); 
                    
                      } 

                      $chartTitle = 'Sales';
                      $chartxAxes = 'Sales Today';
                      $chartyAxes = 'Amount in Ghs';  



                      if(!empty($chart['dataset']) && count($chart['dataset']) > 0){
                          $chart['type'] = 'line';

                          $chart["datasetColor"]['Sales - Ghs ']["bgcolor"] = 'rgba(79, 103, 209, 0.1)';

                          $chart["datasetColor"]['Sales - Ghs ']["bordercolor"] = '#4F67D1'; 
                               
                          
                          $chart['config']['title_display'] = false;
                          $chart['config']['title'] = $chartTitle;

                          $chart['config']['legend_display'] = false;
                          $chart['config']['legend_position'] = 'bottom';

                          $chart['config']['scales_xAxes']['display'] = false;
                          $chart['config']['scales_xAxes']['label'] = $chartxAxes;

                          $chart['config']['scales_yAxes']['display'] = false;
                          $chart['config']['scales_yAxes']['label'] = $chartyAxes;                                
                      }

                      return view('widgets.chart-widget')->withChart($chart)->withChartName($chartName);

                     break;
                   
                   case 2:
                      $chart = array();

                      $chartName = 'Sales for Days of the week';

                      $period = array();

                      $date =Carbon::now()->startOfWeek();
                      $end = Carbon::now()->endOfWeek();
                      while ($date <= $end) {

                          $chart['labels'][] = $date->copy()->format('D');

                          $date->addDays(1);
                      }                      

                      for ($i=0; $i < 7; $i++) { 

                        $chart['dataset']['Sales - Ghs '][] = with(clone $query)
                          ->where(DB::raw('WEEKDAY(created_at)'), '=', $i)
                          ->whereBetween('created_at', [$startDate, $endDate])
                          ->sum('total_amt'); 
                    
                      }                    


                      $chartTitle = 'Sales for Days of the week';
                      $chartxAxes = 'Days of the Week';
                      $chartyAxes = 'Amount in Ghs';  



                      if(!empty($chart['dataset']) && count($chart['dataset']) > 0){
                          $chart['type'] = 'bar';

                          $chart["datasetColor"]['Sales - Ghs ']["bgcolor"] = '#FBBC2D';

                          $chart["datasetColor"]['Sales - Ghs ']["bordercolor"] = '#FBBC2D'; 
                               
                          
                          $chart['config']['title_display'] = true;
                          $chart['config']['title'] = $chartTitle;

                          $chart['config']['legend_display'] = false;
                          $chart['config']['legend_position'] = 'bottom';

                          $chart['config']['scales_xAxes']['display'] = true;
                          $chart['config']['scales_xAxes']['label'] = $chartxAxes;

                          $chart['config']['scales_yAxes']['display'] = true;
                          $chart['config']['scales_yAxes']['label'] = $chartyAxes;                                
                      }

                      return view('widgets.chart-widget')->withChart($chart)->withChartName($chartName);
                     break;

                   case 3:
                      $chart = array();

                      $chartName = 'Time of the day with high sales';

                      $date =Carbon::now()->startOfDay();
                      $end = Carbon::now()->endOfDay();
                      $period = array();

                      while ($date <= $end) {

                          $period[] = $date->copy()->format('H:i');

                          $date->addHours(1);
                      }


                      foreach ($period  as $labels) {
                          $chart['labels'][] = $labels;
                      }                    

                      for ($i=0; $i < count($chart['labels']); $i++) { 

                          $chart['dataset']['Sales - Ghs '][] = with(clone $query)
                            ->where(DB::raw('HOUR(created_at)'), '=', $chart['labels'][$i])
                            ->whereBetween('created_at', [$startDate, $endDate])
                            ->sum('total_amt');                      
                      } 

                      $chartTitle = 'Time of the day with high sales';
                      $chartxAxes = 'Days of the Week';
                      $chartyAxes = 'Amount in Ghs';  



                      if(!empty($chart['dataset']) && count($chart['dataset']) > 0){
                          $chart['type'] = 'bar';

                          $chart["datasetColor"]['Sales - Ghs ']["bgcolor"] = '#FBBC2D';

                          $chart["datasetColor"]['Sales - Ghs ']["bordercolor"] = '#FBBC2D'; 
                               
                          
                          $chart['config']['title_display'] = true;
                          $chart['config']['title'] = $chartTitle;

                          $chart['config']['legend_display'] = false;
                          $chart['config']['legend_position'] = 'bottom';

                          $chart['config']['scales_xAxes']['display'] = false;
                          $chart['config']['scales_xAxes']['label'] = $chartxAxes;

                          $chart['config']['scales_yAxes']['display'] = true;
                          $chart['config']['scales_yAxes']['label'] = $chartyAxes;                                
                      }

                      return view('widgets.chart-widget')->withChart($chart)->withChartName($chartName);
                     break; 

                   case 4:
                      $table = array();

                      $orders = $query
                          ->whereBetween('created_at', [$startDate, $endDate])->select(DB::raw('COUNT(refund) as refund, SUM(total_amt) as total_amt, branches_id'))->groupBy('branches_id')->with('branches')->get(); 

                      $table['head'] = ['Location', 'Gross Sales', 'Refund', 'Net Sales'];

                      $refund = $total = $netGross = 0;

                      foreach ($orders as $order) {

                        $refund += $order->refund;
                        $total += $order->total_amt;
                        $netGross += $order->refund;

                        $table['body'][] = (object) [$order->branches->name, 'Ghs '. $order->refund, 'Ghs '. $order->total_amt, 'Ghs '.$order->refund ];
                      }  

                      $table['body'][] = array(
                          'Sub Total',
                          'Ghs '. $refund,
                          'Ghs '. $total,
                          'Ghs '. $netGross,
                        ); 

                      $table['total'] =  $refund + $total + $netGross;           

                      return view('admin.partial.table')->withTable($table);
                     break;                                         
                 }
                break;
            
            case 'order':

                $query = Order::groupBy('order_no');

                if(!empty($location)){

                  $query->whereHas('branches', function ($query) use ($location) {
                      $query->whereId($location);
                  });         
                }     

                $cancelled = OrderStatus::where('name', '=', 'Cancelled')->first();
                $delayed = OrderStatus::where('name', '=', 'Delayed')->first();             

                 switch ($type) {
                   case 1:
                      $chart = array();

                      $chartName = 'Order Overview';


                      $category = array();

                      $period = array();
                      while ($startDate <= $endDate) {

                          $period[] = $startDate->copy()->format('Y-m-d');

                          $startDate->addDays(1);
                      }                    

                      foreach ($period  as $labels) {
                        $chart['labels'][] = Carbon::parse($labels)->copy()->format('d M y');


                        $category[] = Carbon::parse($labels)->format('Y-m-d');
                      } 

                      for ($i=0; $i < count($category); $i++) { 

                        $chart['dataset']['Orders - '][] = with(clone $query)
                          ->whereDate('created_at', '=', $category[$i])
                          ->sum('total_amt'); 

                        $chart['dataset']['Delayed Order - '][] = with(clone $query)
                          ->where('order_status_id', '!=', $delayed->id)
                          ->whereDate('created_at', '=', $category[$i])
                          ->sum('total_amt'); 

                        $chart['dataset']['Cancelled Order - '][] = with(clone $query)
                          ->where('order_status_id', '!=', $cancelled->id)
                          ->whereDate('created_at', '=', $category[$i])
                          ->sum('total_amt');                                               
                      } 

                      $chartTitle = 'Order';
                      $chartxAxes = 'Date';
                      $chartyAxes = 'Amount in Ghs';  



                      if(!empty($chart['dataset']) && count($chart['dataset']) > 0){
                          $chart['type'] = 'line';

                          $chart["datasetColor"]['Orders - ']["bgcolor"] = 'rgba(79, 103, 209, 0.1)';

                          $chart["datasetColor"]['Orders - ']["bordercolor"] = '#4F67D1'; 

 
                           $chart["datasetColor"]['Delayed Order - ']["bgcolor"] = 'rgba(251, 188, 45, 0.1)';

                          $chart["datasetColor"]['Delayed Order - ']["bordercolor"] = '#FBBC2D'; 

                          $chart["datasetColor"]['Cancelled Order - ']["bgcolor"] = 'rgba(238, 92, 144, 0.1)';

                          $chart["datasetColor"]['Cancelled Order - ']["bordercolor"] = '#EE5C90';                                                         
                          
                          $chart['config']['title_display'] = false;
                          $chart['config']['title'] = $chartTitle;

                          $chart['config']['legend_display'] = false;
                          $chart['config']['legend_position'] = 'bottom';

                          $chart['config']['scales_xAxes']['display'] = false;
                          $chart['config']['scales_xAxes']['label'] = $chartxAxes;

                          $chart['config']['scales_yAxes']['display'] = false;
                          $chart['config']['scales_yAxes']['label'] = $chartyAxes;                                
                      }

                      return view('widgets.chart-widget')->withChart($chart)->withChartName($chartName);

                     break;
                   
                   case 2:
                      $chart = array();

                      $chartName = 'Order for Days of the week';

                      $period = array();

                      $date =Carbon::now()->startOfWeek();
                      $end = Carbon::now()->endOfWeek();
                      while ($date <= $end) {

                          $chart['labels'][] = $date->copy()->format('D');

                          $date->addDays(1);
                      }                      

                      for ($i=0; $i < 7; $i++) { 

                        $chart['dataset']['Sales - Ghs '][] = with(clone $query)
                          ->where(DB::raw('WEEKDAY(created_at)'), '=', $i)
                          ->whereBetween('created_at', [$startDate, $endDate])
                          ->sum('total_amt'); 

                        $chart['dataset']['Delayed Order - '][] = with(clone $query)
                          ->where(DB::raw('WEEKDAY(created_at)'), '=', $i)
                          ->where('order_status_id', '!=', $delayed->id)
                          ->whereBetween('created_at', [$startDate, $endDate])
                          ->sum('total_amt'); 

                        $chart['dataset']['Cancelled Order - '][] = with(clone $query)
                          ->where(DB::raw('WEEKDAY(created_at)'), '=', $i)
                          ->where('order_status_id', '!=', $cancelled->id)
                          ->whereBetween('created_at', [$startDate, $endDate])
                          ->sum('total_amt');                                               
                      }                    


                      $chartTitle = 'Order for Days of the week';
                      $chartxAxes = 'Days of the Week';
                      $chartyAxes = 'Amount in Ghs';  



                      if(!empty($chart['dataset']) && count($chart['dataset']) > 0){
                          $chart['type'] = 'bar';

                          $chart["datasetColor"]['Orders - ']["bgcolor"] = 'rgba(79, 103, 209, 0.1)';

                          $chart["datasetColor"]['Orders - ']["bordercolor"] = '#4F67D1'; 
                          
 
                           $chart["datasetColor"]['Delayed Order - ']["bgcolor"] = 'rgba(251, 188, 45, 0.1)';

                          $chart["datasetColor"]['Delayed Order - ']["bordercolor"] = '#FBBC2D'; 

                          $chart["datasetColor"]['Cancelled Order - ']["bgcolor"] = 'rgba(238, 92, 144, 0.1)';

                          $chart["datasetColor"]['Cancelled Order - ']["bordercolor"] = '#EE5C90';
                          
                          $chart['config']['title_display'] = true;
                          $chart['config']['title'] = $chartTitle;

                          $chart['config']['legend_display'] = false;
                          $chart['config']['legend_position'] = 'bottom';

                          $chart['config']['scales_xAxes']['display'] = true;
                          $chart['config']['scales_xAxes']['label'] = $chartxAxes;

                          $chart['config']['scales_yAxes']['display'] = true;
                          $chart['config']['scales_yAxes']['label'] = $chartyAxes;                                
                      }

                      return view('widgets.chart-widget')->withChart($chart)->withChartName($chartName);
                     break;

                   case 3:
                      $chart = array();

                      $chartName = 'Time of the day with high orders';

                      $date =Carbon::now()->startOfDay();
                      $end = Carbon::now()->endOfDay();
                      $period = array();

                      while ($date <= $end) {

                          $period[] = $date->copy()->format('H:i');

                          $date->addHours(1);
                      }


                      foreach ($period  as $labels) {
                          $chart['labels'][] = $labels;
                      }                    

                      for ($i=0; $i < count($chart['labels']); $i++) { 

                          $chart['dataset']['Orders '][] = with(clone $query)
                            ->where(DB::raw('HOUR(created_at)'), '=', $chart['labels'][$i])
                            ->whereBetween('created_at', [$startDate, $endDate])
                            ->count();                      
                      } 

                      $chartTitle = 'Time of the day with high orders';
                      $chartxAxes = 'Days of the Week';
                      $chartyAxes = 'Number of Orders';  



                      if(!empty($chart['dataset']) && count($chart['dataset']) > 0){
                          $chart['type'] = 'bar';

                          $chart["datasetColor"]['Orders ']["bgcolor"] = '#FBBC2D';

                          $chart["datasetColor"]['Orders ']["bordercolor"] = '#FBBC2D'; 
                               
                          
                          $chart['config']['title_display'] = true;
                          $chart['config']['title'] = $chartTitle;

                          $chart['config']['legend_display'] = false;
                          $chart['config']['legend_position'] = 'bottom';

                          $chart['config']['scales_xAxes']['display'] = false;
                          $chart['config']['scales_xAxes']['label'] = $chartxAxes;

                          $chart['config']['scales_yAxes']['display'] = true;
                          $chart['config']['scales_yAxes']['label'] = $chartyAxes;                                
                      }

                      return view('widgets.chart-widget')->withChart($chart)->withChartName($chartName);
                     break; 

                   case 4:
                 //      $table = array();

                 //      $orders = $query
                 //          ->whereBetween('created_at', [$startDate, $endDate])->select(DB::raw('SUM(total_amt) as orders, SUM(total_amt) as total_amt, branches_id'))->groupBy('branches_id')->with('branches')->get(); 

                 //      $table['head'] = ['Location', 'Gross Sales', 'Refund', 'Net Sales'];

                 //      $orderAmt = $total = $netGross = 0;

                 //      foreach ($orders as $order) {

                 //        $$orderAmt += $order->orders;
                 //        $total += $order->total_amt;
                 //        $netGross += $order->refund;

                 //        $table['body'][] = (object) [$order->branches->name, 'Ghs '. $order->refund, 'Ghs '. $order->total_amt, 'Ghs '.$order->refund ];
                 //      }  

                 //      $table['body'][] = array(
                 //          'Total',
                 //          'Ghs '. $orderAmt,
                 //          'Ghs '. $delayed,
                 //          'Ghs '. $cancelled
                 //        );         

                 //      return view('admin.partial.table')->withTable($table);
                      break;                                         
                 }
                break;
        }
    } 


}
