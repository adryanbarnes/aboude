<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Schema;

class Module extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'description', 'route_segment','type_id','minimun_auth_level'];


    public function privileges()
    {
    	return $this->belongsToMany('App\Privilege','modules_privileges');
    }


    public function createModulePrivilegeColumn()
    {
    	$columnName = 'module_'.$this->id;
    	if (!Schema::hasColumn("users_modules_privileges", $columnName)) {

    		Schema::table('users_modules_privileges', function($table) use ($columnName) {
    			$table->integer($columnName)->nullable();
    		});

    	}

    }

    public function dropColumn()
    {
    	$columnName = 'module_'.$this->id;
    	if (!Schema::hasColumn("users_modules_privileges", $columnName)) {

    		Schema::table('users_modules_privileges', function($table) use ($columnName) {
    			$table->dropColumn($columnName);
    		});

    	}
    }
}
