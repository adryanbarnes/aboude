<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MealMenu extends Model
{
	use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];    

    protected $guarded = [];

    public function mealCategories(){
    	return $this->belongsTo('App\MealCategory');
    }

    public function mealTag(){
    	return $this->belongsTo('App\MealTag');
    }
    public function mealAttribute(){
        return $this->belongsTo('App\MealAttribute');
    }

    public function packSize(){
    	return $this->belongsToMany('App\PackSize', 'meal_pack_sizes', 'meal_menu_id', 'pack_size_id')->withPivot('price','id');
    } 


}
