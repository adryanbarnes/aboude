<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MealCategory extends Model
{
	use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function mealMenu(){
    	return $this->hasMany('App\MealMenu', 'meal_categories_id');
    }  

    public function mealTag(){
        return $this->hasMany('App\MealTag', 'meal_categories_id');
    }

    public function mealAttribute(){
        return $this->hasMany('App\MealAttribute', 'meal_categories_id');
    }    

}
