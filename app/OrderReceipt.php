<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderReceipt extends Model
{	
	protected $guarded = [];

    public function vatNumber(){
    	return $this->belongsTo('App\VatNumber');
    }

    public function order(){
    	return $this->hasMany('App\Order', 'order_no', 'order_no_id');
    }

    public function user(){
    	return $this->hasMany('App\User');
    }

    public function branches(){
    	return $this->hasMany('App\User');
    }
}
