<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
	use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];    

    protected $guarded = [];

    public function orderType(){
    	return $this->belongsTo('App\OrderType');
    }

    public function orderStatus(){
    	return $this->belongsTo('App\OrderStatus');
    }    

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function branches(){
    	return $this->belongsTo('App\Branches');
    }

    public function mealMenu(){
        return $this->belongsTo('App\MealMenu');
    }

}
