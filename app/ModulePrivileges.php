<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModulePrivileges extends Model
{
    protected $table = "users_modules_privileges";

    protected $guarded = ['id'];


}
