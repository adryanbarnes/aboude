<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeTracker extends Model
{
    protected $guarded = [];
}
