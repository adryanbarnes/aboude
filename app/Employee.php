<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{	
	use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];    

    protected $guarded = [];

    public function jobRole(){
    	return $this->belongsTo('App\JobRole');
    }

    public function branches(){
    	return $this->belongsTo('App\Branches');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }      

}
