<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrder extends Model
{
	use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];    

    protected $guarded = [];

    public function purchaseItem(){
    	return $this->belongsTo('App\PurchaseItem');
    }

    public function taxTerm(){
    	return $this->belongsTo('App\TaxTerm');
    } 

    public function branches(){
        return $this->belongsTo('App\Branches');
    } 

    public function statuses(){
        return $this->belongsTo('App\Status');
    }
}
