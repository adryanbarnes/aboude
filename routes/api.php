<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('kitchen/order-status', 'KitchenController@getOrderStatus');
Route::get('kitchen/order-type', 'KitchenController@getOrderType');
Route::get('kitchen/get-menu', 'KitchenController@getMealMenu');
Route::post('kitchen/save-menu', 'KitchenController@store');
Route::post('kitchen/order', 'KitchenController@placeOrder');
Route::get('kitchen/all-orders', 'KitchenController@getOrders');
Route::get('kitchen/user-orders/{id}', 'KitchenController@getUserOrders');
Route::post('kitchen/{order_no}/edit', 'KitchenController@getOrders');
Route::get('kitchen/{order_no}/cancel', 'KitchenController@CancelOrder');

Route::get('get-branches', 'AdminModuleController@getBranches');
Route::get('get-jobs', 'AdminModuleController@getJobs');
Route::get('get-packages', 'AdminModuleController@getPackages');