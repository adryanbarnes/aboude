<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


Auth::routes();

Route::group(['middleware' => ['auth']], function () {


	Route::get('admin/sales-order', 'AdminModuleController@salesOrder')->name('sales-order');

	Route::any('admin/sale-order-widget/{type}/{widgetType}', 'AdminModuleController@widget')->name('sale-order.widget');

	Route::any('admin/dashboard/{type}/{period?}', 'AdminModuleController@dashboardWidgets')->name('admin.widget');


	Route::get('admin/report', 'AdminModuleController@adminReport')->name('admin.report');	

	Route::any('admin/report/{reportType}/{type}', 'AdminModuleController@adminReportPartial')->name('admin.report.partial');

	Route::get('/', 'AdminModuleController@index')->name('homepage');

	Route::get('/home', 'AdminModuleController@index')->name('home');

	Route::resource('admin', 'AdminModuleController');


	Route::post('employee/update/{id}', 'EmployeeController@update')->name('updateEmployee');
	Route::get('employee/pin-reset/{id}', 'EmployeeController@resetPincode')->name('uemployee.resetpincode');

	Route::get('employees/list/partial/{id}/{edit?}', 'EmployeeController@employeePartial')->name('employees.partial');
	Route::get('employees/list', 'EmployeeController@getEmployees')->name('getEmployees');

	Route::resource('employees', 'EmployeeController');

	Route::get('finance/expenses/lists', 'FinanceController@expensesList')->name('expenses.list');
	Route::get('finance/expenses/{id?}', 'FinanceController@expenses')->name('expenses');
	Route::get('finance/expenses/partial/{id}/{edit?}', 'FinanceController@expensePartial')->name('expenses.partial');

	Route::get('finance/purchase-order', 'FinanceController@purchaseOrder')->name('purchase-order.index');

	Route::get('finance/purchase-order/create', 'FinanceController@createPurchaseOrder')->name('purchase-order.create');

	Route::post('finance/purchase-order/', 'FinanceController@storePurchaseOrder')->name('purchase-order.store');

	Route::delete('finance/purchase-order/{id}', 'FinanceController@destroyPurchaseOrder')->name('purchase.destroy');
	Route::get('finance/purchase-order/list', 'FinanceController@getPurchaseOrders')->name('getPurchaseOrders');

	Route::get('finance/pay-roll', 'FinanceController@showPayroll')->name('payroll.index');
	Route::get('finance/pay-roll/list', 'FinanceController@getEmployeesPayroll')->name('getEmployeesPayroll');

	Route::get('finance/widgets/{type}/{period?}', 'FinanceController@financePartial')->name('finance.partial');

	Route::get('finance/report', 'FinanceController@financeReport')->name('finance.report');	

	Route::get('finance/report/{reportType}/{type}', 'FinanceController@reportPartial')->name('finance.report.partial');	

	Route::resource('finance', 'FinanceController');
	Route::resource('kitchen', 'KitchenController');
	

	Route::get('/delete/{route}/{msg}/{callbackfunction}/{params?}', 'HomeController@deleteModal')->name('delete');
	

});
