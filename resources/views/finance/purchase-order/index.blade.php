@extends('layouts.app')

@section('title', 'Purchase Order')

@section('page-name', 'Dashboard')

@section('breadcrumbs', 'Finance Module / Purchase Invoice')


@section('content')
	<div class="col-md-12">

		<div class="col">
		  	<div class="widget-wrap">

		  	<div class="d-flex">
		  		<div class="mr-auto">
		  			<h3>Purchase Order</h3>
		  		</div>
		  		
		  		<div>
		  			<a href="{{route('purchase-order.create')}}"><button type="button" class="btn btn-primary btn-blue"><i class="fas fa-plus"></i> Add New Purchase Invoice</button></a>
		  		</div>	
		  	</div>

	  	
		  		
		  		<div class="d-flex">
		  			<table id="purchase" class="display" style="width:100%">
		  				<thead>
		  					<th>No</th>
		  					<th>Bill Number</th>
		  					<th>Date Raised</th>
		  					<th>Supplier</th>
		  					<th>Tax</th>
		  					<th>Amount Paid</th>
		  					<th>Status</th>
		  					<th>Action</th>
		  				</thead>

		  				<tbody>
		  						
		  				</tbody>
		  			</table>
		  		</div>

		  		
		  	</div>				
		</div>	

	</div> 

<!-- Modal -->

<div class="modal fade" id="contentModal" tabindex="-1" role="dialog" aria-labelledby="contentModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div id="purchase-form" class="modal-content">

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){

	$('#finance-menu').addClass('active-module');
	$('#finance-purchase').addClass('ext-menu-active');	
	
	$(document).on('click', '.delete-purchase', function(){

		$('#contentModal').modal('show');
		$('#contentModal .modal-dialog');

		id = $(this).data('deleteid');

		$( "#purchase-form" ).load( '{{ route("delete", ["purchase-destroy","purchase order","table_refresh"])}}' +'/' + id , function(response) {
		  
		});	

	});

    var dataTable = $('#purchase').DataTable({
        //"paging":   false,
        "ordering": false,
        "info":     false,
        "stripeClasses": [],
        "dom": '<"dataTable-head d-flex">rtip',

		"language": {
		    "paginate": {
		      "previous": '<i class="fas fa-angle-left"></i>',
		      "next": '<i class="fas fa-angle-right"></i>',
		    },  
		},
		"scrollY": "50vh",

        processing: true,
        serverSide: true,
	    pageLength: 10,
	    searching:true,
	    bLengthChange:false,        
	    ajax: {
	        url: '{{ route("getPurchaseOrders")}}',
	        data: function (d) {
	        	d.location = $('select[name=purchase_table_location]').val();
	        	d.from_date = $('input[name=from_date]').val();
	        	d.to_date = $('input[name=to_date]').val();        	
	        },            
	    },                  
        "order": [[ 1, 'asc' ]],
        columns: [
        	{data: 'No', name: 'No'},
            {data: 'Bill Number', name: 'Bill Number'},
            {data: 'Date Raised', name: 'Date Raised'},
            {data: 'Supplier', name: 'Supplier'},
            {data: 'Tax', name: 'Tax'},
            {data: 'Amount Paid', name: 'Amount Paid'},
            {data: 'Status', name: 'Status'},
            {data: 'Action', name: 'Action'},
        ]        
           
    });   

	var dataHead = '<div class="form-inline mr-auto"> <div class="input-group"> <div class="input-group-prepend"><div class="input-group-text input-group-text-trans"><i class="fa fa-search"></i></div></div><input id="search-purchase" type="text" class="form-control mr-sm-2" aria-controls="orders-table" placeholder="Search here"></div> {{Form::select("purchase_table_location",$branches, null, array("class" => "form-control mr-sm-2", "id" => "purchase_table-location"))}} </div> <div class="date-range-wrap"><div class="date-input-ranger"> <i class="far fa-calendar-alt"></i><input class="dateinput" type="text" id="from_date" name="from_date" placeholder="Begin data" autocomplete="off"><i class="fas fa-long-arrow-alt-right" class="date-divider"></i><i class="far fa-calendar-alt"></i><input class="dateinput" type="text" id="to_date" name="to_date" placeholder="End date" autocomplete="off"></div></div>';

	$("div.dataTable-head.d-flex").html(dataHead); 

	$('#search-purchase').on( 'keyup', function () {
	    dataTable.search( this.value ).draw();
	} );

      $('#purchase_table-location').on('change', function(e) {
         dataTable.draw();
      }); 

    window.table_refresh = function(){
    	dataTable.draw();
    }  
	$('.dateinput').on('change', function(e) {
	    dataTable.draw();
	});

	$('.dateinput').datepicker({
		format: 'yyyy-mm-dd',		
	});	   

})


		
</script>
@endsection