@extends('layouts.app')

@section('title', 'Add Purchase Order')

@section('page-name', 'Dashboard')

@section('breadcrumbs', 'Finance Module / Purchase Invoice / New Purchase Order')


@section('content')
	<div class="col-md-12">

	  	<div class="widget-wrap purchase-spreadsheet">

		  	<div class="d-flex">
		  		<div class="mr-auto">
		  			<h3>Add Purchase Order</h3>
		  		</div>

		  		<div>
		  			<button type="button" class="btn btn-primary btn-blue" id="run-payroll"><i class="fas fa-plus"></i> Mark as billed</button>
		  		</div>
		  		
		  	</div>
	  		{!! Form::open(['route' => 'purchase-order.store', 'files' => true, 'id' => 'purchase-create']) !!}
	  		<div class="col grey-light-bg">
			  <div class="form-row">
			    <div class="form-group col">
			      <label for="inputEmail4">Supplier</label>
			      <input type="text" class="form-control" id="inpuSupplier" name="supplier" placeholder="Darko Farms">
			    </div>

			    <div class="form-group col">
			      <label for="inputPassword4">Date of Purchase</label>
			      <input type="text" class="form-control dateinput" name="purchase_date" id="inputDate" placeholder="DD/MM/YYYY">
			    </div>

			    <div class="form-group col">
			      <label for="inputPassword4">Order Number</label>
			      <input type="text" class="form-control" id="inputOrderNo" placeholder="{{$purchaseOrderNo}}" value="{{$purchaseOrderNo}}" disabled>
			      <input type="hidden" name="purchase_no" value="{{$purchaseOrderNo}}">
			    </div>

			  </div>

			  <hr/>

			  <div class="col-md-4 zero-left-pad">
				<div class="input-group mb-3">
				  <div class="input-group-prepend">
				    <span class="input-group-text" id="amount-tax">Amounts are</span>
				  </div>

				  {{Form::select("tax_term", $taxTerms, null, array("class" => "form-control amount-tax", 'id' => 'tax-term'))}}
				</div>		  	
			  </div>
	  		</div>

	  		<div class="purchase-spreadsheet">
	  			<table class="purchase-order">
	  				<thead>
	  					<th></th>
	  					<th>Item</th>
	  					<th>Description</th>
	  					<th>Qty</th>
	  					<th>Unit Price</th>
	  					<th>Tax Amount</th>
	  					<th>Amount</th>
	  					<th></th>
	  				</thead>

	  				<tbody>
	  					<tr>
	  						<td><input type="hidden" name="id[]"></td>
	  						<td >
	  							<input type="text" class="form-control" name="item[]">
	  						</td>

	  						<td >
	  							<input type="text" class="form-control" name="description[]">
	  						</td>
	  						<td >
	  							<input type="number" class="form-control" name="qty[]">
	  						</td>

	  						<td >
	  							<input type="number" class="form-control" name="unit_price[]">
	  						</td>

	  						<td >
	  							<input type="number" class="form-control" name="tax_amt[]">
	  						</td>

	  						<td>
	  							<input type="hidden" class="form-control" name="amt[]">
	  							<span class="row-amt"></span>
	  						</td>

	  						<td>
	  							<a href="#" class="delete-purchase"><i class="far fa-trash-alt"></i></a>
	  						</td>

	  					</tr>
	  				</tbody>
	  			</table>



				  <div class="m-3 ml-4">
					  <button id="add-row" type="button" class="btn btn-primary btn-blue split"> Add New Row <span class="chevron-down"><i class="fas fa-chevron-down"></i></span>
					  </button>				  	
				  </div>

			  	<div class="col clearfix">

			  		<div class="col-4 float-right mr-4 mb-3 zero-right-pad">


			  			<div class="grey-layer purchase-summary mt-3">
			  				<table style="width: 100%;">
			  					<tr>
			  						<td align="left">Sub Total</td>
			  						<td align="right">Ghs <span id="sub-total">0.00</span></td>
			  					</tr>

			  					<tr>
			  						<td align="left">Total</td>
			  						<td align="right">Ghs <span id="total">0.00</span></td>
			  					</tr>
			  				</table>
			  			</div>		  			
			  		</div>
			  	</div>	

			  	<div class="col clearfix">
					<div class="col-6 float-right text-right zero-right-pad">
						<a href="{{route('purchase-order.index')}}"><button type="button" class="btn btn-secondary btn-cancel">Cancel</button></a>
						<button type="submit" class="btn btn-primary btn-blue split"> Save &amp; Approve<span class="chevron-down"><i class="fas fa-chevron-down"></i></span>
				  </button>					
					</div>		  		
			  	</div>			  		  			
	  		</div>
  		{!! Form::close() !!}
	  		
	  	</div>	

	</div> 
	



@endsection

@section('scripts')

<script type="text/javascript">
$(document).ready(function(){  

	$('#finance-menu').addClass('extended-side-menu active-module');
	$('#finance-purchase').addClass('ext-menu-active');	
	
	$('.dateinput').datepicker({
		format: 'yyyy-mm-dd',		
	});		

	var row = '<tr> <td><input type="hidden" name="id[]"></td><td > <input type="text" class="form-control" name="item[]"> </td><td > <input type="text" class="form-control" name="description[]"> </td><td > <input type="number" class="form-control" name="qty[]"> </td><td > <input type="number" class="form-control" name="unit_price[]"> </td><td > <input type="number" class="form-control" name="tax_amt[]"> </td><td > <input type="hidden" class="form-control" name="amt[]"> <span class="row-amt"></span></td><td> <a href="#" class="delete-purchase"><i class="far fa-trash-alt"></i></a> </td></tr>';

	

	$('#add-row').on('click', function(){
		$('.purchase-order tbody').append(row);
	});

	$(document).on('click', '.delete-purchase', function(){
		$(this).parents('tr').remove();
		if(!$('.purchase-order tbody tr').length > 0){
			$('.purchase-order tbody').append(row);
		}
	});


	var calAmt = function(selector){
		qty = $(selector).parents('tr').find('input[name*="qty"]').val();
		unitPrice = $(selector).parents('tr').find('input[name*="unit_price"]').val();

		qty = !isEmpty(qty) ? qty : 0;
		unitPrice = !isEmpty(unitPrice) ? unitPrice : 0;

		// console.log(qty);
		// console.log(unitPrice);

		amt = qty * unitPrice;

		parseFloat(amt).toFixed(2)

		$(selector).parents('tr').find('input[name="amt[]"]').val(amt);
		$(selector).parents('tr').find('.row-amt').html(amt);

		calSubTotal();		
	}


	var calSubTotal = function(){
		subTotal = $('input[name="amt[]"]').map(function () {
		    return parseFloat(this.value).toFixed(2);
		}).get();

		subTotal = Math.sum(subTotal);
		
		$('#sub-total').html(subTotal);

		calTotal(subTotal);
		//console.log(subTotal);		
	}

	var calTotal = function(subTotal){
		taxAmt = $('input[name="tax_amt[]"]').map(function () {
		    return !isEmpty(this.value) ? parseFloat(this.value).toFixed(2) : 0;
		}).get();

		taxAmt = Math.sum(taxAmt);

		total = $('#tax-term option:selected').val() == 1 ? subTotal + taxAmt : subTotal - taxAmt;


		$('#total').html(total);
		
		//console.log(total);		
	}


	$(document).on('change','input[name*="qty"]', function(){

		calAmt(this);
	});

	$(document).on('change','input[name*="tax_amt"]', function(){

		calAmt(this);
	});	

	$(document).on('change','input[name*="unit_price"]', function(){
		calAmt(this);

	});	
	

})

		
</script>
@endsection