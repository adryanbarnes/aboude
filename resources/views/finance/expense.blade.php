@extends('layouts.app')

@section('title', 'Expenses')

@section('page-name', 'Dashboard')

@section('breadcrumbs', 'Finance Module / Expenses')


@section('content')
	<div class="col-md-12">

		<div class="col">
		  	<div class="widget-wrap">

		  	<div class="d-flex">
		  		<div class="mr-auto">
		  			<h3>Expenses</h3>
		  		</div>
		  		
		  		<div>
		  			<button type="button" class="btn btn-primary btn-blue" id="add-expense"><i class="fas fa-plus"></i> Add Expense</button>
		  		</div>	
		  	</div>

	  	
		  		
		  		<div class="d-flex">
		  			<table id="expenses" class="display expense-table" style="width:100%; min-height: 30vh;">
		  				<thead>
		  					<th>No</th>
		  					<th>Item</th>
		  					<th>Spent On</th>
		  					<th>Category</th>
		  					<th>Tax</th>
		  					<th>Amount Paid</th>
		  					<th>Status</th>
		  					<th>Action</th>
		  				</thead>

		  				<tbody>
		  						
		  				</tbody>
		  			</table>
		  		</div>

		  		
		  	</div>

		  	<div class="row">
		  		<div class="col-7">
					<div id="week-expense" class="chart-widget">
					</div>		  			
		  		</div>

		  		<div class="col-4 float-right">
		  			<div class="grey-layer expense-summary">
		  				<h3>Expense Increase as compared to Last Month</h3>
		  				<h2>21% <img src="{{asset('images/increase.png')}}"></h2>
		  				<p>You spent <span class="dark-pink">GHs 12,000</span> more than last month.</p>
		  			</div>

		  			<div class="grey-layer expense-summary mt-3">
		  				<h3>Top 3 Expenses</h3>
		  				<table style="width: 100%;">
		  				@foreach($topExpenses as $expense)
		  					<tr>
		  						<td align="left">{{$expense->name}}</td>
		  						<td align="right">Ghs {{$expense->tax_paid}}</td>
		  					</tr>
		  				@endforeach
		  				</table>
		  			</div>		  			
		  		</div>
		  	</div>				
		</div>	

	</div> 

<!-- Modal -->
<div class="modal fade" id="contentModal" tabindex="-1" role="dialog" aria-labelledby="contentModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div id="expense-form" class="modal-content">

    </div>
  </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){

	$('#finance-menu').addClass('active-module');
	$('#finance-expense').addClass('ext-menu-active');	
	
    var loadWeekExpense = function(){

		$('#week-expense').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');

    	$('#week-expense').load('{{ route("expenses", 1)}}', function(){

    	})
    }

    loadWeekExpense();

    var expenseTable = $('#expenses').DataTable({
        //"paging":   false,
        "ordering": false,
        "info":     false,
        "stripeClasses": [],
        "dom": '<"dataTable-head d-flex">rtip',

		"language": {
		    "paginate": {
		      "previous": '<i class="fas fa-angle-left"></i>',
		      "next": '<i class="fas fa-angle-right"></i>',
		    },  
		},

	      serverSide: true,
	      processing: true,
	      pageLength: 10,
	      searching:true,
	      bLengthChange:false,
	      "scrollY": "50vh",
	      ajax: {
	        url: '{{ route("expenses.list")}}',
	        data: function (d) {
	        	d.location = $('select[name=order_table_location]').val();
	            d.from_date = $('input[name=order_table_from_date]').val();
	            d.to_date = $('input[name=order_table_to_date]').val();
	        },            
	      },         
        "order": [[ 1, 'asc' ]],
        columns: [
        	{data: 'No', name: 'No'},
            {data: 'Item', name: 'Item'},
            {data: 'Spent On', name: 'Spent On'},
            {data: 'Category', name: 'Category'},
            {data: 'Tax', name: 'Tax'},
            {data: 'Amount Paid', name: 'Amount Paid'},
            {data: 'Status', name: 'Status'},
            {data: 'Action', name: 'Action'},
        ] 		        
            
    });    

	var dataHead = '<div class="form-inline mr-auto"> <div class="input-group"> <div class="input-group-prepend"><div class="input-group-text input-group-text-trans"><i class="fa fa-search"></i></div></div><input id="search-expenses" type="text" class="form-control mr-sm-2" aria-controls="expenses-table" placeholder="Search here"></div> {{Form::select("expense_location",$branches, null, array("class" => "form-control mr-sm-2", "id" => "expense-location"))}} </div> <div class="date-range-wrap"><div class="date-input-ranger"> <i class="far fa-calendar-alt"></i><input class="dateinput" type="text" id="from_date" name="from_date" placeholder="Begin data" autocomplete="off"><i class="fas fa-long-arrow-alt-right" class="date-divider"></i><i class="far fa-calendar-alt"></i><input class="dateinput" type="text" id="to_date" name="to_date" placeholder="End date" autocomplete="off"></div></div>';

	$("div.dataTable-head.d-flex").html(dataHead);  


	$('#search-expenses').on( 'keyup', function () {
	    expenseTable.search( this.value ).draw();
	} );

    $('#expense-location').on('change', function(e) {
        expenseTable.draw();
    }); 


    window.table_refresh = function(){
    	expenseTable.draw();
    }


	$('.dateinput').on('change', function(e) {
	    expenseTable.draw();
	});

	$('.dateinput').datepicker({
		format: 'yyyy-mm-dd',		
	});		

	$('#add-expense').on('click', function(){

		$('#contentModal').modal('show');	

		$( "#expense-form" ).load( '{{ route("expenses.partial", 1)}}', function(response) {
		  
		});	

	});

	$(document).on('click', '.edit-expense', function(){

		$('#contentModal').modal('show');

		id = $(this).data('editid');

		$( "#expense-form" ).load( '{{ route("expenses.partial", 2)}}' +'/' + id , function(response) {
		  
		});	

	});

	$(document).on('click', '.delete-expense', function(){

		$('#contentModal').modal('show');

		id = $(this).data('deleteid');

		$( "#expense-form" ).load( '{{ route("delete", ["finance-destroy","expense","table_refresh"])}}' +'/' + id , function(response) {
		  
		});	

	});

})
		
</script>
@endsection