@extends('layouts.app')

@section('title', 'Finance Overview')

@section('page-name', 'Overview')

@section('breadcrumbs', 'Finance Module / Overview')


@section('content')
	<div class="col-md-12">

		<div id="overview-mini-widget" class="row mini-widget"></div>

		<div class="row">
		  	<div class="widget-wrap">
		  		<h4 class="widget-title">Income, Expenses and Sales</h4>

		  		<div class="d-flex">
		  			<div class="flex-row mr-auto">
						{{ Form::open(array('class' => 'form-inline mt-2 mt-md-0')) }}
						    {{Form::select('inc-exp-sale-location', $branches, null, array('class' => 'form-control mr-sm-2', 'id' => 'inc-exp-sale-location'))}}
						{{ Form::close() }}	 				
		  				
		  			</div>

	  				<ul class="flex-wrap mb-3 period">
	  					<li class="period-item" data-period='1'>
	  						<a class="period-link" role="tab" aria-controls="pills-profile" aria-selected="false">Today</a>
	  					</li>
	  					<li class="active-period" data-period='2'>
	  						<a class="period-link" role="tab" aria-controls="pills-profile" aria-selected="true">Daily</a>
	  					</li>
	  					<li data-period='3'>
	  						<a class="period-link" role="tab" aria-controls="pills-profile" aria-selected="false">Month</a>
	  					</li>
	  				</ul>

		  			
		  		</div>

		  		<div id="inc-exp-sale-stats" class="chart-widget"></div>

		  		
		  	</div>
		</div> 

		<div class="row">
		  	<div class="widget-wrap">
		  		<h4 class="widget-title">Expenses By Bill Category</h4>

		  		<div class="d-flex">
		  			<div class="flex-row mr-auto">
						{{ Form::open(array('class' => 'form-inline mt-2 mt-md-0')) }}
						    {{Form::select('expense_cat_location', $branches, null, array('class' => 'form-control mr-sm-2'))}}
						{{ Form::close() }}	  				
		  				
		  			</div>

				    <div class="date-range-wrap">
				        <div class="date-input-ranger">
				            <i class="far fa-calendar-alt"></i>
				            <input class="dateinput expense_cat-date" type="text" id="expense_cat_from_date" name="expense_cat_from_date" placeholder="Begin data" autocomplete="off">

				            <i class="fas fa-long-arrow-alt-right" class="date-divider"></i>

				            <i class="far fa-calendar-alt"></i>

				            <input class="dateinput expense_cat-date" type="text" id="expense_cat_to_date" name="expense_cat_to_date" placeholder="End date" autocomplete="off">
				        </div>                                
				    </div>

		  			
		  		</div>

		  		<div id="expense_cat-chart" class="widget-wrap chart-widget"></div>

		  		
		  	</div>
		</div>

		<div class="row">

			<div class="col">
			  	<div class="widget-wrap">
			  		<h4 class="widget-title">purchase history</h4>

			  		
			  		<div class="d-flex">
			  			<table id="purchase" class="display" style="width:100%">
			  				<thead>
			  					<th>No</th>
			  					<th>Item</th>
			  					<th>Bill Number</th>
			  					<th>Date of Purchase</th>
			  					<th>Supplier</th>
			  					<th>Qty</th>
			  					<th>Tax</th>
			  					<th>Amount Paid</th>
			  				</thead>

			  				<tbody>			  							  					
			  				</tbody>
			  			</table>
			  		</div>

			  		
			  	</div>				
			</div>			
		</div>
	</div> 	
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){

	$('#finance-menu').addClass('active-module');
	$('#finance-overview').addClass('ext-menu-active');

	var loadOverview = function(){
	    $('#overview-mini-widget').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');

		$( "#overview-mini-widget" ).load( '{{ route("finance.partial", 1)}}', function(response) {
		  
		});		
	}

	loadOverview();

	var expenseCat = function(){
		expense_cat_location = $('select[name=expense_cat_location]').val();
	    from_date = !isEmpty($('input[name=expense_cat_from_date]').val()) ? $('input[name=expense_cat_from_date]').val() : '';
	    to_date = !isEmpty($('input[name=expense_cat_to_date]').val()) ? $('input[name=expense_cat_to_date]').val() : '';


	    $('#expense_cat-chart').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');

		$( "#expense_cat-chart" ).load( '{{ route("finance.partial", 3)}}' + '/?location='+ expense_cat_location + '&from_date='+ from_date + '&to_date='+to_date, function(response) {
		  
		});


	}

    var loadIncExpSale = function(){
    	period = $('.period li.active-period').data('period');

    	//console.log(period);
		$('#inc-exp-sale-stats').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');

    	$('#inc-exp-sale-stats').load('{{ route("finance.partial", 2)}}' + '/' + period + '?location=' +$('select[name=inc-exp-sale-location]').val(), function(){

    	})
    }

    $('#inc-exp-sale-location').on('change', function(){
    	loadIncExpSale();
    }) 

    loadIncExpSale(); 	

	$('.period li').on('click', function (e) {
	  e.preventDefault()
	  $(this).parent().children().removeClass('active-period');
	  $(this).addClass('active-period');

	  loadIncExpSale(); 

	})	

	expenseCat();

	$('.expense_cat-date').on('change', function(e) {
	    expenseCat();
	});		


    var dataTable = $('#purchase').DataTable({
        //"paging":   false,
        "ordering": false,
        "info":     false,
        "stripeClasses": [],
        "pageLength": 5,
        "dom": '<"dataTable-head d-flex">rtip',

		"language": {
		    "paginate": {
		      "previous": '<i class="fas fa-angle-left"></i>',
		      "next": '<i class="fas fa-angle-right"></i>',
		    },  
		},
		"scrollY": "16vh",

        processing: true,
        serverSide: true,
	    pageLength: 10,
	    searching:true,
	    bLengthChange:false,        
	    ajax: {
	        url: '{{ route("finance.partial", 4)}}',
	        data: function (d) {
	        	d.location = $('select[name=purchase_table_location]').val();
	        	d.from_date = $('input[name=from_date]').val();
	        	d.to_date = $('input[name=to_date]').val();
	        },            
	    },                  
        "order": [[ 1, 'asc' ]],
        columns: [
        	{data: 'No', name: 'No'},
        	{data: 'Item', name: 'Item'},
            {data: 'Bill Number', name: 'Bill Number'},
            {data: 'Date of Purchase', name: 'Date of Purchase'},
            {data: 'Supplier', name: 'Supplier'},
            {data: 'Qty', name: 'Qty'},
            {data: 'Tax', name: 'Tax'},
            {data: 'Amount Paid', name: 'Amount Paid'},
        ]        
           
    });   

	var dataHead = '<div class="form-inline mr-auto"> <div class="input-group"> <div class="input-group-prepend"><div class="input-group-text input-group-text-trans"><i class="fa fa-search"></i></div></div><input id="search-purchase" type="text" class="form-control mr-sm-2" aria-controls="orders-table" placeholder="Search here"></div> {{Form::select("purchase_table_location",$branches, null, array("class" => "form-control mr-sm-2", "id" => "purchase_table-location"))}} </div> <div class="date-range-wrap"><div class="date-input-ranger"> <i class="far fa-calendar-alt"></i><input class="dateinput purchase-date" type="text" id="from_date" name="from_date" placeholder="Begin data" autocomplete="off"><i class="fas fa-long-arrow-alt-right" class="date-divider"></i><i class="far fa-calendar-alt"></i><input class="dateinput purchase-date" type="text" id="to_date" name="to_date" placeholder="End date" autocomplete="off"></div></div>';

	$("div.dataTable-head.d-flex").html(dataHead); 

	$('#search-purchase').on( 'keyup', function () {
	    dataTable.search( this.value ).draw();
	} );

      $('#purchase_table-location').on('change', function(e) {
         dataTable.draw();
      }); 

	$('.purchase-date').on('change', function(e) {
	    dataTable.draw();
	});

	$('.dateinput').datepicker({
		format: 'yyyy-mm-dd',		
	});		


})



		
</script>
@endsection