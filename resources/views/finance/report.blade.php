@extends('layouts.app')

@section('title', 'Reports Purchase Order')

@section('page-name', 'Dashboard')

@section('breadcrumbs', 'Finance Module / Reports')


@section('content')
	<div class="col-md-12">

		<div class="col">

		  	<div class="mr-auto">
		  		<div class="form-inline">
		  		  <label class="sr-only" for="inputReportType">Report Type</label>
				  <div class="input-group mb-2 mr-sm-2">
				    <div class="input-group-prepend">
				      <div class="input-group-text">Report Type</div>
				    </div>
				    <select class="form-control" id="inputReportType">
				    	<option value="sales">Sales Summary</option>
				    	<option value="order">Order Summary</option>
				    </select>
				  </div>		  			
		  		</div>
		  	</div>

		  	<div class="form-inline mr-auto">
				{{ Form::open(array('class' => 'form-inline mt-2 mt-md-0')) }}
				    {{Form::select('sale_main_location', $branches, null, array('class' => 'form-control mr-sm-2', "id" => "sale_main-location"))}}
				{{ Form::close() }}

				<div class="date-range-wrap">
					<div class="date-input-ranger">
						<i class="far fa-calendar-alt"></i>
						<input class="dateinput" type="text" id="from_date" name="from_date" placeholder="Begin data" autocomplete="off"><i class="fas fa-long-arrow-alt-right"></i><i class="far fa-calendar-alt"></i>
						<input class="dateinput" type="text" id="to_date" name="to_date" placeholder="End date" autocomplete="off">
					</div>
				</div>						
		  	</div>

	  	
		  		
	  		<div class="d-flex">
	  			<table id="purchase" class="display" style="width:100%">
	  				<thead>
	  					<th>No</th>
	  					<th>Bill Number</th>
	  					<th>Date Raised</th>
	  					<th>Supplier</th>
	  					<th>Tax</th>
	  					<th>Amount Paid</th>
	  					<th>Status</th>
	  					<th>Action</th>
	  				</thead>

	  				<tbody>
	  					<tr>
	  						<td>1</td>
	  						<td>PO/3434/18</td>
	  						<td>12/07/2018</td>
	  						<td>Nestcafe</td>
	  						<td>Ghs 120</td>
	  						<td>Ghs 2110</td>
	  						<td><span class="new">Approved</span></td>
	  						<td><a href="#" data-toggle="modal" data-target="#editModal"><i class="fas fa-edit"></i></a> <a href="#" data-toggle="modal" data-target="#deleteModal"><i class="far fa-trash-alt"></i></a></td>
	  					</tr>

	  					<tr>
	  						<td>2</td>
	  						<td>PO/3455/18</td>
	  						<td>12/07/2018</td>
	  						<td>Cal Care</td>
	  						<td>Ghs 23</td>
	  						<td>Ghs 434</td>
	  						<td><span class="draft">Draft</span></td>
	  						<td><a href="#" data-toggle="modal" data-target="#editModal"><i class="fas fa-edit"></i></a> <a href="#" data-toggle="modal" data-target="#deleteModal"><i class="far fa-trash-alt"></i></a></td>
	  					</tr>	


	  					<tr>
	  						<td>3</td>
	  						<td>PO/3445/18</td>
	  						<td>12/07/2018</td>
	  						<td>Utilities</td>
	  						<td>Ghs 90</td>
	  						<td>Ghs 3640</td>
	  						<td><span class="paid">Billed</span></td>
	  						<td><a href="#" data-toggle="modal" data-target="#editModal"><i class="fas fa-edit"></i></a> <a href="#" data-toggle="modal" data-target="#deleteModal"><i class="far fa-trash-alt"></i></a></td>
	  					</tr>	

	  					<tr>
	  						<td>4</td>
	  						<td>Fuel</td>
	  						<td>12/07/2018</td>
	  						<td>Utilities</td>
	  						<td>Ghs 0</td>
	  						<td>Ghs 3640</td>
	  						<td><span class="paid">Billed</span></td>
	  						<td><a href="#" data-toggle="modal" data-target="#editModal"><i class="fas fa-edit"></i></a> <a href="#" data-toggle="modal" data-target="#deleteModal"><i class="far fa-trash-alt"></i></a></td>
	  					</tr>			  							  						  					
	  					<tr>
	  						<td>5</td>
	  						<td>Coffe Machine</td>
	  						<td>11/07/2018</td>
	  						<td>Darko Farms</td>
	  						<td>Ghs 120</td>
	  						<td>Ghs 2210</td>
	  						<td><span class="new">Approved</span></td>
	  						<td><a href="#" data-toggle="modal" data-target="#editModal"><i class="fas fa-edit"></i></a> <a href="#" data-toggle="modal" data-target="#deleteModal"><i class="far fa-trash-alt"></i></a></td>
	  					</tr>
  						
	  				</tbody>
	  			</table>
	  		</div>			
		</div>	

	</div> 

@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){

	$('.dateinput').datepicker({
		format: 'yyyy-mm-dd',		
	});	  

    $('#purchase').DataTable({
        //"paging":   false,
        "ordering": false,
        "info":     false,
        "stripeClasses": [],
        "searching":false,
        "bLengthChange": false,

		"language": {
		    "paginate": {
		      "previous": '<i class="fas fa-angle-left"></i>',
		      "next": '<i class="fas fa-angle-right"></i>',
		    },  
		}        
            
    });
  

})



// $('#myInput').on( 'keyup', function () {
//     table.search( this.value ).draw();
// } );


		
</script>
@endsection