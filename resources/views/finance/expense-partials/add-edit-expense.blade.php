@if($create)

{!! Form::open(['route' => 'finance.store', 'files' => true, 'id' => 'expense-submit']) !!}

@else
{!! Form::open(['method'=>'PUT', 'route' => ['finance.update', $editId], 'files' => true, 'id' => 'expense-submit']) !!}
@endif

<div class="modal-header">
	<h5 class="modal-title" id="addExpensesModalLabel">{{ $create ? 'Add Expenses' : 'Edit Expenses'}}</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  <span aria-hidden="true">&times;</span>
	</button>
</div>
<div class="modal-body">
	<div class="upload-btn-wrapper">


	  <div class="upload-frame">
	  	<div class="upload-receipt">
	  		<img id="uploaded-receipt" src="{{ !$create && $editExpense->receipt ?  asset('storage/uploads/receipts/'.$editExpense->receipt) :  asset('images/upload.png') }}">
	  		<p>Upload receipt</p>
	  	</div>
	  </div>
	  <input type="file" name="receipt" onchange="readURL(this, '#uploaded-receipt');"/>
	</div>

  <div class="form-group">
    <label for="inputItem">Item Name</label>
    <input type="text" class="form-control" name="name" id="inputItem" placeholder="eg. Fuel" value="{{ !$create ? $editExpense->name : ''}}">
  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputExpense">Date of Expense</label>
      

      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text"><i class="far fa-calendar"></i></div>
        </div>
        <input type="text" class="form-control dateinput" name="date" id="inputExpense" placeholder="DD/MM/YYYY" value="{{ !$create ? $editExpense->date : ''}}">
      </div>		      
    </div>

    <div class="form-group col-md-6">
      <label for="inputCategory">Expense Category</label>

      {{Form::select("category", $category, !$create ? $editExpense->expense_categories_id : null, array("class" => "form-control", 'id' => 'inputCategory'))}}
    </div>

  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputTax">Tax</label>

      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">GHS</div>
        </div>
        <input type="text" class="form-control" name="tax" id="inputTax" placeholder="0.00" value="{{ !$create ? $editExpense->tax : ''}}">
      </div>

    </div>

    <div class="form-group col-md-6">
      <label for="inputTaxPaid">Total Amount Paid with tax</label>

      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">GHS</div>
        </div>
        <input type="text" class="form-control" name="tax_paid" id="inputTaxPaid" placeholder="0.00" value="{{ !$create ? $editExpense->tax_paid : ''}}">
      </div>

    </div>

  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputStatus">Status</label>

      {{Form::select("status", $status, !$create ? $editExpense->statuses_id : null, array("class" => "form-control", 'id' => 'inputStatus'))}}

    </div>

    <div class="form-group col-md-6">
      <label for="inputComment">Comments</label>
      
      <input type="text" class="form-control" name="comment" id="inputComment"  placeholder="Please add a short comment here." value="{{ !$create ? $editExpense->comment : ''}}">
    </div>

  </div>		  

</div>
<div class="modal-footer">
	<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">Cancel</button>
	<button type="submit" class="btn btn-primary"> {{ $create ? 'Save Expenses' : 'Update'}}</button>
</div>

{!! Form::close() !!}

<script type="text/javascript">
	$(document).ready(function(){
		$('.dateinput').datepicker({
			format: 'yyyy-mm-dd',		
		});	

	$("#expense-submit").submit(function(e) {
	    e.preventDefault();
	    var formData = new FormData(this);    

	    $.ajax({
	        url: $(this).attr("action"),
	        type: 'POST',
	        data: formData,
	        success: function (data) {
	            if(data.status_code == '1001'){
                table_refresh();
	            	$('#contentModal').modal('hide');
	            	toastr.success(data.status_msg)

	            }else{
	            	toastr.error(data.status_msg)
	            }
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	});

	})
</script>