@extends('layouts.app')

@section('title', 'Payroll')

@section('page-name', 'Payroll')

@section('breadcrumbs', 'Finance Module / Payroll')


@section('content')
	<div class="col-md-12">

		<div class="col">
		  	<div class="widget-wrap">

				<div class="d-flex">
			  		<div class="mr-auto">
			  			<h3>Payroll</h3>
			  		</div>

				</div>		  	
		  		
		  		<div class="d-flex">
		  			<table id="employee" class="display" style="width:100%">
		  				<thead>
		  					<th>No</th>
		  					<th>Employee</th>
		  					<th>Month</th>
		  					<th>Total Salaries Paid</th>
		  					<th>Total Allowance</th>
		  					<th>Total Deductions</th>
		  				</thead>

		  				<tbody>
		  						
		  				</tbody>
		  			</table>
		  		</div>

		  		
		  	</div>				
		</div>	

		<div class="row">
			<div >
				
			</div>
		</div>

	</div> 

<!-- Modal -->

<div class="modal fade" id="contentModal" tabindex="-1" role="dialog" aria-labelledby="contentModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div id="employee-form" class="modal-content">

    </div>
  </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
$(document).ready(function(){

	$('#finance-menu').addClass('active-module');
	$('#finance-payroll').addClass('ext-menu-active');

	$(document).on('click','#add-employee', function(){

		$('#contentModal').modal('show');	

		$('#contentModal .modal-dialog').addClass('modal-lg');

		$( "#employee-form" ).load( '{{ route("employees.partial", 1)}}', function(response) {
		  
		});	

	});

	$(document).on('click', '.edit-employee', function(){

		$('#contentModal').modal('show');
		$('#contentModal .modal-dialog').addClass('modal-lg');

		id = $(this).data('editid');

		$( "#employee-form" ).load( '{{ route("employees.partial", 2)}}' +'/' + id , function(response) {
		  
		});	

	});

	$(document).on('click', '.delete-employee', function(){

		$('#contentModal').modal('show');
		$('#contentModal .modal-dialog').removeClass('modal-lg');

		id = $(this).data('deleteid');

		$( "#employee-form" ).load( '{{ route("delete", ["employees-destroy","employee","table_refresh"])}}' +'/' + id , function(response) {
		  
		});	

	});

    var dataTable = $('#employee').DataTable({
        //"paging":   false,
        "ordering": false,
        "info":     false,
        "stripeClasses": [],
        "dom": '<"dataTable-head d-flex">rtip',

		"language": {
		    "paginate": {
		      "previous": '<i class="fas fa-angle-left"></i>',
		      "next": '<i class="fas fa-angle-right"></i>',
		    },  
		},
		"scrollY": "50vh",

        processing: true,
        serverSide: true,
	    pageLength: 10,
	    searching:true,
	    bLengthChange:false,        
	    ajax: {
	        url: '{{ route("getEmployeesPayroll")}}',
	        data: function (d) {
	        	d.location = $('select[name=employee_table_location]').val();
	        },            
	    },                  
        "order": [[ 1, 'asc' ]],
        columns: [
        	{data: 'No', name: 'No'},
            {data: 'Employee', name: 'Employee'},
            {data: 'Month', name: 'Month'},
            {data: 'Total Salaries Paid', name: 'Total Salaries Paid'},
            {data: 'Total Allowance', name: 'Total Allowance'},
            {data: 'Total Deductions', name: 'Total Deductions'},
        ]        
           
    });   

	var dataHead = '<div class="form-inline mr-auto"> <div class="input-group"> <div class="input-group-prepend"><div class="input-group-text input-group-text-trans"><i class="fa fa-search"></i></div></div><input id="search-employee" type="text" class="form-control mr-sm-2" aria-controls="orders-table" placeholder="Search here"></div> {{Form::select("employee_table_location",$branches, null, array("class" => "form-control mr-sm-2", "id" => "employee_table-location"))}} </div> <div><button type="button" class="btn btn-primary btn-blue" id="run-payroll"><i class="fas fa-plus"></i> Run Payroll</button></div></div>';

	$("div.dataTable-head.d-flex").html(dataHead); 

	$('#search-employee').on( 'keyup', function () {
	    dataTable.search( this.value ).draw();
	} );

      $('#employee_table-location').on('change', function(e) {
         dataTable.draw();
      }); 

    window.table_refresh = function(){
    	dataTable.draw();
    }      


})

		
</script>
@endsection