<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">   

        {{ Html::style("css/app.css") }}
        {{ Html::style("//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }} 

        {{ Html::style("//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css") }}

        {{ Html::style("css/main.css") }}          

        <title>Aboude - @yield('title')</title>
    </head>
    <body>

        <!-- Bootstrap NavBar -->
        <nav class="navbar navbar-expand-md nav-theme fixed-top">

                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto nav-name">
                      <li class="nav-item active">
                        <a class="nav-link" href="#"><img src="{{asset('images/dashboard.png')}}">@yield('page-name') <span class="sr-only">(current)</span></a>
                      </li>
                    </ul>

                    <div class="account-details">
                        <p class="notify-icon"><i class="far fa-bell"></i></p>
                        <div class="profile-name">
                        <span class="user-initial">{{acronym(Auth::user()->other_name . ' '. Auth::user()->surname)}}</span>
                            <p class="user-name mt-2">{{ Auth::user()->other_name . ' '. Auth::user()->surname }}<span class="user-level">{{Auth::user()->authLevel()->first()->name}}</span></p>

                        </div>

                        <p class="caret-drop"><i class="fas fa-chevron-down"></i></p>                       
                    </div>
                </div>              
        </nav>
        <!-- NavBar END -->


        <!-- Bootstrap row -->
        <div class="row aboude-body" id="body-row">
            <!-- Sidebar -->
            <div id="sidebar-container" class="sidebar-expanded d-md-block zero-left-pad">
                <!-- Bootstrap List Group -->
                <div class="p-2">
                    <div class="sticky-top sticky-offset">
                        <div class="side-menu">
                            <div class="d-flex align-items-end flex-column align-items-stretch">
                                <div class="p-1 logo-img">
                                    <div class="img" style="background-image: url('/assets/images/menu/usaid-logo.png');"></div>
                                </div>
                                <div class="p-1">
                                    <div class="menu-label">
                                        <img src="{{asset('images/menu/admin-module.png')}}">
                                        <span class="menu-title">Admin</span>                                        
                                    </div>


                                    <div id="admin-menu" class="extended-side-menu">
                                        <div class="d-flex align-items-end flex-column align-items-stretch">
                                            <div class="p-1 logo-img">
                                                <h1>Administrator Module</h1>
                                            </div>


                                            <ul class="extended-menu-list">
                                                <li id="admin-dash"><a href="{{route('admin.index')}}"><img src="{{asset('images/menu/dashboard.png')}}"> <span> Dashboard</span></a></li>

                                                <li id="admin-sales"><a href="{{route('sales-order')}}"> <img src="{{asset('images/menu/sales-order.png')}}"> <span>Sales &amp; Order</span></a></li>

                                                <li id="admin-employee"><a href="{{route('employees.index')}}"><img src="{{asset('images/menu/employees.png')}}"> <span>Employees</span></a></li>

                                                <li id="admin-report"><a href="{{route('admin.report')}}"><img src="{{asset('images/menu/report.png')}}"> <span>Reports</span></a></li>

                                                <li><a href="#"><img src="{{asset('images/menu/device-setup.png')}}"> <span>Device setup</span></a></li>

                                                <li><a href="#"><img src="{{asset('images/menu/settings.png')}}"> <span>Settings</span></a></li>
                                            </ul>


                                        </div>
                                    </div>                            
                                    
                                </div>

                                <div class="p-1">
                                    <div class="menu-label">
                                        <img src="{{asset('images/menu/finance-module.png')}}">
                                        <span class="menu-title">Finance</span>
                                    </div>

                                    <div id="finance-menu" class="extended-side-menu">
                                        <div class="d-flex align-items-end flex-column align-items-stretch">
                                            <div class="p-1 logo-img">
                                                <h1>Finance Module</h1>
                                            </div>


                                            <ul class="extended-menu-list">
                                                <li id="finance-overview"><a href="{{route('finance.index')}}"><img src="{{asset('images/menu/overview.png')}}"> <span> Overview</span></a></li>

                                                <li id="finance-expense"><a href="{{route('expenses')}}"> <img src="{{asset('images/menu/expenses.png')}}"> <span>Expenses</span></a></li>

                                                <li id="finance-purchase"><a href="{{route('purchase-order.index')}}"><img src="{{asset('images/menu/purchase-order.png')}}"> <span>Purchase Order</span></a></li>

                                                <li id="finance-payroll"><a href="{{route('payroll.index')}}"><img src="{{asset('images/menu/payroll.png')}}"> <span>Payroll</span></a></li>

                                                <li><a href="{{route('finance.report')}}"><img src="{{asset('images/menu/device-setup.png')}}"> <span>Reports</span></a></li>

                                                <li><a href="#"><img src="{{asset('images/menu/settings.png')}}"> <span>Settings</span></a></li>
                                            </ul>


                                        </div>
                                    </div>                             
                                </div>

                                <div class="p-1" >
                                    <div class="menu-label">
                                        <img src="{{asset('images/menu/inventory-module.png')}}">
                                        <span class="menu-title">Inventory</span>
                                    </div>
                                    
                                </div>

                                <div class="p-1" >
                                    <div class="menu-label">
                                        <img src="{{asset('images/menu/staff-module.png')}}">
                                        <span class="menu-title">Staff</span>
                                    </div>
                                    
                                </div>                        

                            </div>
                        </div>
                    </div>
                </div>                
                <!-- List Group END-->
            </div>
            <!-- sidebar-container END -->

            <!-- MAIN -->
            <div class="col pb-3 pt-5 mt-2">
                    <div class="d-flex breadcrumbs pb-3">@yield('breadcrumbs')</div>

                    <div class="d-flex flex-column">
                        @yield('content') 
                    </div>

            </div>
            <!-- Main Col END -->

        </div>
        <!-- body-row END -->            

    <!-- Scripts -->
        <script src="/js/app.js"></script>
        <script src="/js/function.js"></script>
        <!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        {!! Toastr::render() !!}  
        {!! Toastr::clear() !!}      

        <script type="text/javascript">
            $(function () {
              $('[data-toggle="popover"]').popover({
                container: 'main'
              })

        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });           
            })   


        $('.menu-label').on('click', function (e) {
          e.preventDefault()
          
          if($(this).next().length > 0 && $(this).next().hasClass('extended-side-menu')){
                $('.extended-side-menu').removeClass('active-module');
                $(this).parent().children('.extended-side-menu').addClass('active-module');
            }

        })


        $.extend( true, $.fn.dataTable.defaults, {
            'language':{ 
               "loadingRecords": "<img src='{{asset('images/loader.gif')}}' />",
               "processing": "<img src='{{asset('images/loader.gif')}}' />"
            },
            "ScrollCollapse": true,
        } );

        </script>
    <!-- Scripts -->

    <!-- Blade Specific Scripts-->

        @yield('scripts')

     <!-- Blade Specific Scripts-->

    </body>
</html>