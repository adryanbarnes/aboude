@if(!empty($chart['dataset']) && count($chart['dataset']) > 0)

<div class="d-flex chart" style=" height: 40vh; max-height: 300px;">
	<canvas id="canvas-{{snake_case($chartName)}}"></canvas>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		var config_{{snake_case($chartName)}} = {
			type: "{{$chart['type']}}",
			data: {
				labels: [
				@foreach($chart['labels'] as $label)
					{!! "\"" . $label . "\","  !!}
				@endforeach ],

	              datasets: [
	              	@foreach($chart['dataset'] as $key => $value)
	              	{
	                  label: '{{$key}}',
	                  data: [{{implode(',',$value)}}],
	                  backgroundColor:'{{$chart["datasetColor"][$key]["bgcolor"]}}',
	                  borderColor:'{{$chart["datasetColor"][$key]["bordercolor"]}}',
	                  borderWidth: 1,
	                  pointBorderWidth:3,
	                  //fill: false,
	                  lineTension: 0,
	                },

	               @endforeach

	              ]
			},
			options: {
                reponsive: true,
                maintainAspectRatio: false,

				elements: { 
					point: { 
						radius: 0,
						hitRadius: 5,
					}
				 },                				
				title: {
					text: "{{$chart['config']['title']}}",
					display: {{ isset($chart['config']['title_display']) && $chart['config']['title_display'] == true ? 'true': 'false'}},
				},
		        legend: {
		            display: {{ $chart['config']['legend_display'] ? 'true': 'false'}},
		            position: '{{ $chart["config"]["legend_position"] == "bottom" ? "bottom" : "top"}}',
		            labels: {
		                fontColor: 'rgb(85, 85, 85)',
		                fontStyle: '600',
		                usePointStyle: 'true',
		                boxWidth: 12,
		                fill: "#f00",
		                fontFamily: "'proxima_nova_ltsemibold', sans-serif"
		            }
		        },				
				scales: {
					xAxes: [{
						gridLines: {
							drawBorder: false
						},						
						scaleLabel: {
							display: {{$chart['config']['scales_xAxes']['display'] ? 'true' : 'false'}},
							labelString: "{{$chart['config']['scales_xAxes']['label']}}"
						},
                      ticks: {
                          beginAtZero:true
                      }						
					}],
					yAxes: [{
						gridLines: {
							drawBorder: false
						},
						scaleLabel: {
							display: {{$chart['config']['scales_yAxes']['display'] ? 'true' : 'false'}},
							labelString: "{{$chart['config']['scales_yAxes']['label']}}"
						}
					}]
				},

			    tooltips: {
			      mode:'index',
			      intersect:true,
			      backgroundColor: '#FFF',
			      titleFontSize: 12,
			      titleFontFamily: "'proxima_nova_ltsemibold', sans-serif",
			      titleFontColor: '#444444',
			      titleSpacing: 4,
			      titleMarginBottom: 12,
			      bodyFontColor: '#6C7695',
			      bodyFontSize: 10,
			      bodyFontFamily: "'proxima_nova_ltsemibold', sans-serif",
			      bodySpacing: 8,
			      xPadding: 10,
			      yPadding: 15,
			      cornerRadius: 0,
			      caretSize: 10,
			    }				
			}
		};

		var ctx_{{snake_case($chartName)}} = document.getElementById('canvas-{{snake_case($chartName)}}').getContext('2d');
		window.myLine_{{snake_case($chartName)}} = new Chart(ctx_{{snake_case($chartName)}}, config_{{snake_case($chartName)}});			
	})
</script>
@else 
	<div class="d-flex justify-content-center" style=" height: 40vh;">
		<p class="no-result">No data available</p>
	</div>
	
@endif
