@if(isset($miniWidgets))
	<?php $i = 1; ?>
	@foreach($miniWidgets as $title => $value)

		  <div class="p{{ $i = 0 ? 'l-0': 'x-4' }} widgets">
		  	<dl>
		  		<dt>{{$title}}</dt>
		  		<dd>{{$value}}</dd>
		  	</dl>

		  </div>
	<?php $i++; ?>		  
	@endforeach		  
@endif