{!! Form::open(['method' => 'delete', 'route' => [$route,$param], 'id' => 'delete-item']) !!}

<div class="modal-header modal-header-icon">
  <div class="modal-title-icon text-center">
 <div class='icon'><i class="far fa-trash-alt"></i></div>        	
  </div>
</div>
<div class="modal-body d-flex justify-content-center">
<p>Are you sure you want to delete this {{$msg}}?</p>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-secondary btn-cancel mr-auto" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary">Save changes</button>
</div>

{!! Form::close() !!}

<script type="text/javascript">
  $(document).ready(function(){

  $("#delete-item").submit(function(e) {
      e.preventDefault();
      var formData = new FormData(this);    

      $.ajax({
          url: $(this).attr("action"),
          type: 'POST',
          data: formData,
          success: function (data) {
              if(data.status_code == '1001'){
                $('#contentModal').modal('hide');
                toastr.success(data.status_msg);

                {{$callback}}()

              }else{
                toastr.error(data.status_msg)
              }
          },
          cache: false,
          contentType: false,
          processData: false
      });
  });

  })
</script>