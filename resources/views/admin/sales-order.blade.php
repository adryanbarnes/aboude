@extends('layouts.app')

@section('title', 'Sales & Order')

@section('page-name', 'Sales & Order')

@section('breadcrumbs', 'Administrator Module / Sales & Order')


@section('content')
	<div class="col-md-12">
		<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
		  <li class="nav-item">
		    <a class="nav-link active" id="pills-sales-tab" data-toggle="pill" href="#pills-sales" role="tab" aria-controls="pills-sales" aria-selected="true">Sales</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" id="pills-orders-tab" data-toggle="pill" href="#pills-orders" role="tab" aria-controls="pills-orders" aria-selected="false">Orders</a>
		  </li>
		</ul>
		<div class="tab-content" id="pills-tabContent">
		  <div class="tab-pane fade show active" id="pills-sales" role="tabpanel" aria-labelledby="pills-sales-tab">

	  		<div class="d-flex range-selector">
	  			<div class="flex-row mr-auto">
					{{ Form::open(array('class' => 'form-inline mt-2 mt-md-0')) }}
					    {{Form::select('sale_main_location', $branches, null, array('class' => 'form-control mr-sm-2', "id" => "sale_main-location"))}}
					{{ Form::close() }}	  				
	  				
	  			</div>

			    <div class="date-range-wrap">
			        <div class="date-input-ranger">
			            <i class="far fa-calendar-alt"></i>
			            <input class="dateinput sale-main-date" type="text" id="sale_main_from_date" name="sale_main_from_date" placeholder="Begin data" autocomplete="off">

			            <i class="fas fa-long-arrow-alt-right" class="date-divider"></i>

			            <i class="far fa-calendar-alt"></i>

			            <input class="dateinput sale-main-date" type="text" id="order_main_to_date" name="sale_main_to_date" placeholder="End date" autocomplete="off">
			        </div>                                
			    </div>

	  			
	  		</div>

	  		<div id="sale-mini-widget" class="row mini-widget"></div>

			<div class="row">
			  	<div id="sale-chart" class="widget-wrap chart-widget"></div>
			</div> 

		<div class="row">

			<div class="col">
			  	<div class="widget-wrap">
			  		<h4 class="widget-title">Sales Table</h4>

			  		
			  		<div class="d-flex">
			  			<table id="sales-table" class="display" style="width:100%">
			  				<thead>
			  					<th>No</th>
			  					<th>Sales Item</th>
			  					<th>Date of Sale</th>
			  					<th>Qty Sold</th>
			  					<th>Amount</th>
			  				</thead>

			  				<tbody>
			  							  					
			  				</tbody>
			  			</table>
			  		</div>

			  		
			  	</div>				
			</div>			
			  	
		</div>

		</div>


		  <div class="tab-pane fade" id="pills-orders" role="tabpanel" aria-labelledby="pills-orders-tab">
	  		<div class="d-flex range-selector">
	  			<div class="flex-row mr-auto">
					{{ Form::open(array('class' => 'form-inline mt-2 mt-md-0')) }}
					    {{Form::select('order_main_location', $branches, null, array('class' => 'form-control mr-sm-2', "id" => "order_main-location"))}}
					{{ Form::close() }}	  				
	  				
	  			</div>

			    <div class="date-range-wrap">
			        <div class="date-input-ranger">
			            <i class="far fa-calendar-alt"></i>
			            <input class="dateinput order-main-date" type="text" id="order_main_from_date" name="order_main_from_date" placeholder="Begin data" autocomplete="off">

			            <i class="fas fa-long-arrow-alt-right" class="date-divider"></i>

			            <i class="far fa-calendar-alt"></i>

			            <input class="dateinput order-main-date" type="text" id="order_main_to_date" name="order_main_to_date" placeholder="End date" autocomplete="off">
			        </div>                                
			    </div>

	  			
	  		</div>

	  		<div id="order-mini-widget" class="row mini-widget"></div>

			<div class="row">
			  	<div id="order-chart" class="widget-wrap chart-widget"></div>
			</div> 

			<div class="row">

				<div class="col">
				  	<div class="widget-wrap">
				  		<h4 class="widget-title">Sales Table</h4>

				  		
				  		<div class="d-flex">
				  			<table id="order-table" class="display" style="width:100%">
				  				<thead>
				  					<th>No</th>
				  					<th>Order Item</th>
				  					<th>Date of Order</th>
				  					<th>Order Status</th>
				  					<th>Waiter Name</th>
				  					<th>Order</th>
				  				</thead>

				  				<tbody>
				  							  					
				  				</tbody>
				  			</table>
				  		</div>

				  		
				  	</div>				
				</div>			
				  	
			</div>		  	
		  </div>
		</div>

	</div> 	
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){

	$('#admin-menu').addClass('active-module');
	$('#admin-sales').addClass('ext-menu-active');

    var sale_table = $('#sales-table').DataTable({
        //"paging":   false,
        "ordering": false,
        "info":     false,
        "stripeClasses": [],
        "dom": '<"dataTable-head d-flex">rtip',

		"language": {
		    "paginate": {
		      "previous": '<i class="fas fa-angle-left"></i>',
		      "next": '<i class="fas fa-angle-right"></i>',
		    },  
		},

	      serverSide: true,
	      processing: true,
	      pageLength: 10,
	      searching:true,
	      bLengthChange:false,
	      ajax: {
	        url: '{{ route("sale-order.widget", ["sales",2])}}',
	        data: function (d) {
	        	d.location = $('select[name=sales_table_location]').val();
	            d.from_date = $('input[name=sales_table_from_date]').val();
	            d.to_date = $('input[name=sales_table_to_date]').val();
	        },            
	      },         
        "order": [[ 1, 'asc' ]],
        columns: [
        	{data: 'No', name: 'No'},
            {data: 'Sales Item', name: 'Sales Item'},
            {data: 'Date of Sales', name: 'Date of Sales'},
            {data: 'Qty Sold', name: 'Qty Sold'},
            {data: 'Amount', name: 'Amount'},
        ]       
    });

	var dataHead = '<div class="form-inline mr-auto"> <div class="input-group"> <div class="input-group-prepend"><div class="input-group-text input-group-text-trans"><i class="fa fa-search"></i></div></div><input id="search-sales" type="text" class="form-control mr-sm-2" aria-controls="sales-table" placeholder="Search here"></div> {{Form::select("sales_table_location",$branches, null, array("class" => "form-control mr-sm-2", "id" => "sales_table-location"))}} </div> <div class="date-range-wrap"><div class="date-input-ranger"> <i class="far fa-calendar-alt"></i><input class="dateinput date-table" type="text" id="sales_table_from_date" name="sales_table_from_date" placeholder="Begin data" autocomplete="off"><i class="fas fa-long-arrow-alt-right" class="date-divider"></i><i class="far fa-calendar-alt"></i><input class="dateinput" type="text" id="sales_table_to_date" name="sales_table_to_date" placeholder="End date" autocomplete="off"></div></div>';

		

	$("#pills-sales div.dataTable-head.d-flex").html(dataHead); 

	$('#search-sales').on( 'keyup', function () {
	    sale_table.search( this.value ).draw();
	} );

      $('#sales_table-location').on('change', function(e) {
         sale_table.draw();
      }); 


      $('#pills-sales .dateinput').on('change', function(e) {
         sale_table.draw();
      });

	$('#pills-sales .dateinput').datepicker({
		format: 'yyyy-mm-dd',		
	});	             		 

    var order_table = $('#order-table').DataTable({
        //"paging":   false,
        "ordering": false,
        "info":     false,
        "stripeClasses": [],
        "dom": '<"dataTable-head d-flex">rtip',

		"language": {
		    "paginate": {
		      "previous": '<i class="fas fa-angle-left"></i>',
		      "next": '<i class="fas fa-angle-right"></i>',
		    },  
		},

	      serverSide: true,
	      processing: true,
	      pageLength: 10,
	      searching:true,
	      bLengthChange:false,
	      ajax: {
	        url: '{{ route("sale-order.widget", ["orders",2])}}',
	        data: function (d) {
	        	d.location = $('select[name=order_table_location]').val();
	            d.from_date = $('input[name=order_table_from_date]').val();
	            d.to_date = $('input[name=order_table_to_date]').val();
	        },            
	      },         
        "order": [[ 1, 'asc' ]],
        columns: [
        	{data: 'No', name: 'No'},
            {data: 'Order Item', name: 'Order Item'},
            {data: 'Date of Order', name: 'Date of Order'},
            {data: 'Order Status', name: 'Order Status'},
            {data: 'Waiter Name', name: 'Waiter Name'},
            {data: 'Order', name: 'Order'},
        ]       
    });

	var dataHead = '<div class="form-inline mr-auto"> <div class="input-group"> <div class="input-group-prepend"><div class="input-group-text input-group-text-trans"><i class="fa fa-search"></i></div></div><input id="search-orders" type="text" class="form-control mr-sm-2" aria-controls="orders-table" placeholder="Search here"></div> {{Form::select("order_table_location",$branches, null, array("class" => "form-control mr-sm-2", "id" => "order_table-location"))}} </div> <div class="date-range-wrap"><div class="date-input-ranger"> <i class="far fa-calendar-alt"></i><input class="dateinput date-table" type="text" id="order_table_from_date" name="order_table_from_date" placeholder="Begin data" autocomplete="off"><i class="fas fa-long-arrow-alt-right" class="date-divider"></i><i class="far fa-calendar-alt"></i><input class="dateinput" type="text" id="order_table_to_date" name="order_table_to_date" placeholder="End date" autocomplete="off"></div></div>';

		

	$("#pills-orders div.dataTable-head.d-flex").html(dataHead); 

	$('#search-orders').on( 'keyup', function () {
	    order_table.search( this.value ).draw();
	} );

      $('#order_table-location').on('change', function(e) {
         order_table.draw();
      }); 


      $('#pills-orders .date-table').on('change', function(e) {
         order_table.draw();
      });

	$('#pills-orders .dateinput').datepicker({
		format: 'yyyy-mm-dd',		
	});	  

})

var salesMain = function(){
	sale_location = $('select[name=sale_main_location]').val();
    from_date = !isEmpty($('input[name=sale_main_from_date]').val()) ? $('input[name=sale_main_from_date]').val() : '';
    to_date = !isEmpty($('input[name=sale_main_to_date]').val()) ? $('input[name=sale_main_to_date]').val() : '';


    $('#sale-mini-widget').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');

	$( "#sale-mini-widget" ).load( '{{ route("sale-order.widget", ["sales",1])}}' + '/?location='+ sale_location + '&from_date='+ from_date + '&to_date='+to_date, function(response) {
	  
	});


	$('#sale-chart').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');

	$( "#sale-chart" ).load( '{{ route("sale-order.widget", ["sales",3])}}' + '/?location='+ sale_location + '&from_date='+ from_date + '&to_date='+to_date, function(response) {
	  
	});	


}


  $('#sale_main-location').on('change', function(e) {
     salesMain();
  }); 


  $('#pills-sales .sale-main-date').on('change', function(e) {
     salesMain();
  });


var orderMain = function(){
	order_location = $('select[name=order_main_location]').val();
    from_date = !isEmpty($('input[name=order_main_from_date]').val()) ? $('input[name=order_main_from_date]').val() : '';
    to_date = !isEmpty($('input[name=order_main_to_date]').val()) ? $('input[name=order_main_to_date]').val() : '';

    $('#order-mini-widget').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');

	$( "#order-mini-widget" ).load( '{{ route("sale-order.widget", ["orders",1])}}' + '/?location='+ order_location + '&from_date='+ from_date + '&to_date='+to_date, function(response) {
	  
	});

	$('#order-chart').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');

	$( "#order-chart" ).load( '{{ route("sale-order.widget", ["orders",3])}}' + '/?location='+ order_location + '&from_date='+ from_date + '&to_date='+to_date, function(response) {
	  
	});	


}


  $('#order_main-location').on('change', function(e) {
     orderMain();
  }); 


  $('#pills-orders .order-main-date').on('change', function(e) {
     orderMain();
  });  

salesMain();
orderMain();

		
</script>
@endsection