@if(!empty($table['body']) && count($table['body']) > 0)
<table class="report-table">
	<thead>
		
		<tr>
			@foreach($table['head'] as $head)
			<th>{{$head}}</th>
			@endforeach
		</tr>
		
	</thead>

	<tbody>
		@foreach($table['body'] as $body)
		<tr>
			@foreach($body as $content)

			<td>{{$content}}</td>
			@endforeach
		</tr>
		@endforeach

	</tbody>

@if(!empty($table['total']))
		<tfoot>
			<tr>
				<td>&nbsp;</td><td>&nbsp;</td><td>Total </td><td>{{$table['total']}}</td>
			</tr>
			
		</tfoot>
@endif	
</table>


@else 
	<div class="d-flex justify-content-center" style=" height: 40vh;">
		<p class="no-result">No data available</p>
	</div>
	
@endif