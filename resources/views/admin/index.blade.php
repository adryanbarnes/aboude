@extends('layouts.app')

@section('title', 'Admin Dashboard')

@section('page-name', 'Dashboard')

@section('breadcrumbs', 'Administrator Module / Dashboard')


@section('content')
	<div class="col-md-12">
		<div id="summary-mini-widget" class="row"></div>

		<div class="row">
		  	<div class="widget-wrap">
		  		<h4 class="widget-title">Sales and Revenue Statistics</h4>

		  		<div class="d-flex">
		  			<div class="flex-row mr-auto">
						{{ Form::open(array('class' => 'form-inline mt-2 mt-md-0')) }}
						    {{Form::select('sales-revenue-stats-location', $branches, null, array('class' => 'form-control mr-sm-2', 'id' => 'sales-revenue-stats-location'))}}
						{{ Form::close() }}	  				
		  				
		  			</div>

	  				<ul class="flex-wrap mb-3 period sales-revenue">
	  					<li class="period-item" data-period='1'>
	  						<a class="period-link" role="tab" aria-controls="pills-profile" aria-selected="false">Today</a>
	  					</li>
	  					<li class="active-period" data-period='2'>
	  						<a class="period-link" role="tab" aria-controls="pills-profile" aria-selected="true">Daily</a>
	  					</li>
	  					<li data-period='3'>
	  						<a class="period-link" role="tab" aria-controls="pills-profile" aria-selected="false">Monthly</a>
	  					</li>
	  				</ul>

		  			
		  		</div>

		  		<div id="sales-revenue-stats" class="chart-widget"></div>

		  		
		  	</div>
		</div> 

		<div class="row">
		  	<div class="widget-wrap">
		  		<h4 class="widget-title">Sales Comparison by food category</h4>

		  		<div class="d-flex">
		  			<div class="flex-row mr-auto">
						{{ Form::open(array('class' => 'form-inline mt-2 mt-md-0')) }}
						    {{Form::select('sales-comparison-location', $branches, null, array('class' => 'form-control mr-sm-2', 'id' => 'sales-comparison-location'))}}
						{{ Form::close() }}	  				
		  				
		  			</div>

	  				<ul class="flex-wrap mb-3 period sales-comparison">
	  					<li class="period-item" data-period='1'>
	  						<a class="period-link" role="tab" aria-controls="pills-profile" aria-selected="false">Today</a>
	  					</li>
	  					<li class="active-period" data-period='2'>
	  						<a class="period-link" role="tab" aria-controls="pills-profile" aria-selected="false">This Week</a>
	  					</li>
	  					<li data-period='3'>
	  						<a class="period-link" role="tab" aria-controls="pills-profile" aria-selected="false">This Month</a>
	  					</li>
	  				</ul>

		  			
		  		</div>

		  		<div id="sales-comparison" class="chart-widget"></div>

		  		
		  	</div>
		</div>

		<div class="row">
			<div class="col zero-pad">
			  	<div class="widget-wrap">
			  		<h4 class="widget-title">Items Sold</h4>

			  		<div class="d-flex">
			  			<div class="flex-row mr-auto">
							{{ Form::open(array('class' => 'form-inline mt-2 mt-md-0')) }}
							    {{Form::select('sold-items-location', $branches, null, array('class' => 'form-control mr-sm-2', 'id' => 'sold-items-location'))}}
							{{ Form::close() }}	  				
			  				
			  			</div>
			  			
			  		</div>

			  		<div id="sold-items" class="chart-widget"></div>

			  		
			  	</div>				
			</div>

			<div class="col zero-pad">
			  	<div class="widget-wrap">
			  		<h4 class="widget-title">Best Selling Food</h4>

			  		<div class="d-flex">
			  			<div class="flex-row mr-auto">
							<div class= 'form-inline mt-2 mt-md-0'>						
							    {{Form::select('best-food-location', $branches, null, array('class' => 'form-control mr-sm-2' , 'id' => 'best-food-location'))}}

							</div>				  				
			  			</div>
			  			
			  		</div>

			  		<div class="d-flex">
			  			<table id="best-selling-food" class="display" style="width:100%">
			  				<thead>
			  					<th>Food Item</th>
			  					<th>Qty Sold</th>
			  					<th>Sales</th>
			  				</thead>

			  				<tbody></tbody>
			  			</table>
			  		</div>

			  		
			  	</div>				
			</div>			
		</div>
	</div> 	
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){

	$('#admin-menu').addClass('active-module');
	$('#admin-dash').addClass('ext-menu-active');	

    var bestSellingTable =  $('#best-selling-food').DataTable({
    	"searching": false,
        "paging":   false,
        "ordering": false,
        "info":     false,
        "stripeClasses": [],
        "scrollY": "10vh",
	      serverSide: true,
	      processing: true,
	      pageLength: 10,
	      bLengthChange:false,
	      ajax: {
	        url: '{{ route("admin.widget", 5)}}',
	        data: function (d) {
	        	d.location = $('select[name=best-food-location]').val();
	        },            
	      },         
        columns: [
        	{data: 'Food Item', name: 'Food Item'},
            {data: 'Qty Sold', name: 'Qty'},
            {data: 'Sales', name: 'Sales'},
        ] 

    });   


    $('#best-food-location').on('change', function(e) {
        bestSellingTable.draw();
    });  

    var loadItemSold = function(){
    	$('#sold-items').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');
    	$('#sold-items').load('{{ route("admin.widget", 4)}}' + '?location=' +$('select[name=sold-items-location]').val(), function(){

    	})
    }

    $('#sold-items-location').on('change', function(){
    	loadItemSold();
    }) 

    loadItemSold(); 

    var loadSalesComparison = function(){
    	period =  $('.sales-comparison li.active-period').data('period');
    	//console.log(period);

		$('#sales-comparison').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');

    	$('#sales-comparison').load('{{ route("admin.widget", 3)}}' + '/' + period + '?location=' +$('select[name=sales-comparison-location]').val() , function(){

    	})
    }

    $('#sales-comparison-location').on('change', function(){
    	loadSalesComparison();
    }) 

    loadSalesComparison();     

    var loadSalesRevenue = function(){
    	period = $('.sales-revenue li.active-period').data('period');

    	//console.log(period);
		$('#sales-revenue-stats').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');

    	$('#sales-revenue-stats').load('{{ route("admin.widget", 2)}}' + '/' + period + '?location=' +$('select[name=sales-revenue-stats-location]').val(), function(){

    	})
    }

    $('#sales-revenue-stats-location').on('change', function(){
    	loadSalesRevenue();
    }) 

    loadSalesRevenue(); 
    
	var summary = function(){

		$( "#summary-mini-widget" ).load( '{{ route("admin.widget", 1)}}', function(response) {
		  
		});

	}

	summary();


	$('.period li').on('click', function (e) {
	  e.preventDefault()
	  $(this).parent().children().removeClass('active-period');
	  $(this).addClass('active-period');

	  if($(this).parent().hasClass('sales-comparison')){
	  	loadSalesComparison();
	  }else if($(this).parent().hasClass('sales-revenue')){
	  	loadSalesRevenue()
	  }

	})


})



		
</script>
@endsection