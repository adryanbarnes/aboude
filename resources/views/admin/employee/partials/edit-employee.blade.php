{!! Form::open(['route' => ['updateEmployee', $editId], 'files' => true, 'id' => 'employee-create']) !!}

  <div class="modal-header">
    <h5 class="modal-title" id="addEmployeeModalLabel">Edit Employee</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>

  <div class="modal-body">

		  <div class="form-row">
		    <div class="form-group col-md-12">
		      

			<div class="upload-btn-wrapper">
			<div class="img-frame">{!! $employee->profile_picture ? "<img id='uploaded-receipt' src='". asset('storage/uploads/employees/'. $employee->profile_picture) ."'>" : acronym($employee->other_name . ' '. $employee->surname) !!}</div>	
			  <input type="file" name="receipt" onchange="readURL(this, '#uploaded-receipt');"/>
			</div>

		    </div>

		  </div> 			

		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="inputFirstName">First Name</label>
		      <input type="text" class="form-control" name="other_name" id="inputFirstName" placeholder="eg. Kwame" value="{{$employee->other_name }}">		      
		    </div>

		    <div class="form-group col-md-6">
		      <label for="inputSurname">Surname</label>
		      <input type="text" class="form-control" name="surname" id="inputSurname" placeholder="eg. Mensah" value="{{$employee->surname }}">		      
		    </div>

		  </div> 

		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="inputDOB">Date of Birth</label>
		      

		      <div class="input-group">
		        <div class="input-group-prepend">
		          <div class="input-group-text input-group-text-trans"><i class="far fa-calendar"></i></div>
		        </div>
		        <input type="text" class="form-control date-picker" name="dob" id="inputDOB" placeholder="YYYY-MM-DD" value="{{$employee->dob }}">
		      </div>		      
		    </div>

		    <div class="form-group col-md-6">
		      <label for="inputGender">Gender</label>
		      <select class="form-control" id="inputGender" name="gender">
		      	<option>Please select a gender</option>
		      	<option value="M">Male</option>
		      	<option value="F">Female</option>
		      </select>
		    </div>

		  </div>


		  <div class="form-group">
		    <label for="inputPhone">Phone Number</label>
		    <input type="text" class="form-control" name="phone_number" id="inputPhone" placeholder="eg. 0234432345" value="{{$employee->phone_number }}">
		  </div>

		  <div class="form-group">
		    <label for="inputEmail">Email (Optional)</label>
		    <input type="text" class="form-control" name="email" id="inputEmail" placeholder="eg. someone@aboude.com" value="{{$employee->email }}">
		  </div>			  			  

		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="inputDate">Start Date</label>
		      

		      <div class="input-group">
		        <div class="input-group-prepend">
		          <div class="input-group-text"><i class="far fa-calendar"></i></div>
		        </div>
		        <input type="text" class="form-control date-picker" name="start_date" id="inputDate" placeholder="YYYY-MM-DD" value="{{$employee->employee()->first()->start_date }}">
		      </div>		      
		    </div>

		    <div class="form-group col-md-6">
		      <label for="inputLocation">Location</label>
	      		{{Form::select("branch", $branches, $employee->employee()->first()->branches_id, array("class" => "form-control", 'id' => 'inputLocation'))}}	       
		    </div>

		  </div>

		  <div class="form-group">
		    <label for="inputJobRole">Job Role</label>
		    <input type="text" class="form-control" name="job_role" id="inputJobRole" placeholder="eg. Waiter" value=" {{ App\JobRole::find($employee->employee()->first()->job_role_id)->first()->title}}">
		  </div>		  

		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="inputSalaryType">Salary Type</label>

		        <select class="form-control" id="inputSalaryType" name="salary_type">
		        	<option value="Wages">Wages</option>
		        </select>

		    </div>

		    <div class="form-group col-md-6">
		      <label for="inputSalary">Salary</label>

		      <div class="input-group">
		        <div class="input-group-prepend">
		          <div class="input-group-text">GHS</div>
		        </div>
		        <input type="text" class="form-control" name="salary" id="inputSalary" placeholder="0.00" value="{{$employee->employee()->first()->salary }}">
		      </div>

		    </div>

		  </div> 

		  <div class="form-row">
		    <div class="form-group col-md-5">

		      <div class="input-group input-group-original">
		      	<input type="text" class="form-control pin-holder" value="* * * *" disabled>
		        <div class="input-group-append pin-reset">
		          <span class="input-group-text input-group-text-original">Reset Pincode</span>
		        </div>
		        
		      </div>	      

		    </div>

		  </div>  

  

	  <div class="modal-footer employee-footer">
	    <button type="button" class="btn btn-secondary mr-auto btn-cancel" data-dismiss="modal">Cancel</button>
	    <button type="submit" class="btn btn-primary">Update</button>
	  </div> 
  </div> 

{!! Form::close() !!}

<script type="text/javascript">
	$(document).ready(function(){
	$('.date-picker').datepicker({
		format: 'yyyy-mm-dd',		
	});	

	$("#employee-create").submit(function(e) {
	    e.preventDefault();
	    var formData = new FormData(this);    

	    $.ajax({
	        url: $(this).attr("action"),
	        type: 'POST',
	        data: formData,
	        success: function (data) {
	            if(data.status_code == '1001'){
	            	table_refresh();
	            	$('#contentModal').modal('hide');
	            	toastr.success(data.status_msg)

	            }else{
	            	toastr.error(data.status_msg)
	            }
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	});

	})
</script>      