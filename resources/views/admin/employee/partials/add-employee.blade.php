{!! Form::open(['route' => 'employees.store', 'files' => true, 'id' => 'employee-create']) !!}
  <div class="modal-header">
    <h5 class="modal-title" id="addEmployeeModalLabel">Add Employee</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>

		<div class="d-flex wizard-stage">
			<div class="pr-2 align-self-center">
		  <div class="progress-mark">
		    <input data-section="employee-details" type="checkbox" id="checkbox" checked />
		    <label for="checkbox"></label>
		  </div>  			
				<h4>Employee Details</h4>
			</div>

			<div class="px-2 align-self-center">
		  <div class="progress-mark">
		    <input data-section="employment-details" type="checkbox" id="checkbox" />
		    <label for="checkbox"></label>
		  </div>    			
				<h4>Employment Details</h4>
			</div> 

			<div class="align-self-center">
		  <div class="progress-mark">
		    <input data-section='account-setting' type="checkbox" id="checkbox" />
		    <label for="checkbox"></label>
		  </div>    			
				<h4>Account Settings</h4>
			</div>       			     			
		</div>

  <div class="modal-body">
		<div id="employee-details" class="employment-details employee-active-section">
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="inputFirstName">First Name</label>
		      <input type="text" class="form-control" name="other_name" id="inputFirstName" placeholder="eg. Kwame">		      
		    </div>

		    <div class="form-group col-md-6">
		      <label for="inputSurname">Surname</label>
		      <input type="text" class="form-control" name="surname" id="inputSurname" placeholder="eg. Mensah">		      
		    </div>

		  </div> 

		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="inputDOB">Date of Birth</label>
		      

		      <div class="input-group">
		        <div class="input-group-prepend">
		          <div class="input-group-text input-group-text-trans"><i class="far fa-calendar"></i></div>
		        </div>
		        <input type="text" class="form-control date-picker" name="dob" id="inputDOB" placeholder="YYYY-MM-DD">
		      </div>		      
		    </div>

		    <div class="form-group col-md-6">
		      <label for="inputGender">Gender</label>
		      <select class="form-control" id="inputGender" name="gender">
		      	<option>Please select a gender</option>
		      	<option value="M">Male</option>
		      	<option value="F">Female</option>
		      </select>
		    </div>

		  </div>


		  <div class="form-group">
		    <label for="inputPhone">Phone Number</label>
		    <input type="text" class="form-control" name="phone_number" id="inputPhone" placeholder="eg. 0234432345">
		  </div>

		  <div class="form-group">
		    <label for="inputEmail">Email (Optional)</label>
		    <input type="text" class="form-control" name="email" id="inputEmail" placeholder="eg. someone@aboude.com">
		  </div>			  			  

			<div class="upload-btn-wrapper">


			  <div class="upload-frame">
			  	<div class="upload-receipt">
			  		<img id="uploaded-receipt" src="{{asset('images/upload.png')}}">
			  		<p>Upload Profile Picture</p>
			  	</div>
			  </div>
			  <input type="file" name="profile_picture" onchange="readURL(this, '#uploaded-receipt');"/>
			</div>		  		   			
		</div>

  	<div id="employment-details" class="employment-details">
	  <div class="form-row">
	    <div class="form-group col-md-6">
	      <label for="inputDate">Start Date</label>
	      

	      <div class="input-group">
	        <div class="input-group-prepend">
	          <div class="input-group-text"><i class="far fa-calendar"></i></div>
	        </div>
	        <input type="text" class="form-control date-picker" name="start_date" id="inputDate" placeholder="YYYY-MM-DD">
	      </div>		      
	    </div>

	    <div class="form-group col-md-6">
	      <label for="inputLocation">Location</label>
      		{{Form::select("branch", $branches, null, array("class" => "form-control", 'id' => 'inputLocation'))}}	       
	    </div>

	  </div>

	  <div class="form-group">
	    <label for="inputJobRole">Job Role</label>
	    <input type="text" class="form-control" name="job_role" id="inputJobRole" placeholder="eg. Waiter">
	  </div>		  

	  <div class="form-row">
	    <div class="form-group col-md-6">
	      <label for="inputSalaryType">Salary Type</label>

	        <select class="form-control" id="inputSalaryType" name="salary_type">
	        	<option value="Wages">Wages</option>
	        </select>

	    </div>

	    <div class="form-group col-md-6">
	      <label for="inputSalary">Salary</label>

	      <div class="input-group">
	        <div class="input-group-prepend">
	          <div class="input-group-text">GHS</div>
	        </div>
	        <input type="text" class="form-control" name="salary" id="inputSalary" placeholder="0.00">
	      </div>

	    </div>

	  </div>      		
  	</div>

  	<div id="account-setting" class="employment-details">
  		<label>Employee Pincode</label>
  		<input type="hidden" name="pincode">

  		<div class="d-flex pin-board"></div>

  		<label>Employee Pincode will be sent to employee through SMS</label>
  	</div>
 
  <div class="modal-footer employee-footer">
    <button type="button" class="btn btn-secondary mr-auto btn-cancel" data-dismiss="modal">Cancel</button>
    <button id="previous-screen" type="button" class="btn btn-back" disabled>Back</button>
    <button id="next-screen" type="button" class="btn btn-primary">Next</button>

    <button id="employee-submit" type="submit" class="btn btn-primary" disabled="">Submit</button>
  </div>

  </div>

  {!! Form::close() !!}

<script type="text/javascript">
	$(document).ready(function(){
	$('.date-picker').datepicker({
		format: 'yyyy-mm-dd',		
	});	

	var employeeCode = genRandom(4);

	var pin = '';

	employeeCode.toString().split('').map(function(number){
		pin += '<div class="pin-digit text-center"><p>'+ number+'</p></div>'
	});
		
	$('.pin-board').append(pin);

	$('input[name="pincode"]').val(employeeCode);

    $('#next-screen').on('click', function (e) {
      e.preventDefault()
      
      if($('.employment-details.employee-active-section').next().length > 0 ){
             var current = $('.employment-details.employee-active-section');
             current.removeClass('employee-active-section');

            current.next().addClass('employee-active-section');

            var currentWizardStage = current.attr('id');
            var nextWizardStage = current.next().attr('id');

            $(".wizard-stage").find("input[data-section='" + currentWizardStage + "']").prop( "checked", false );

            $(".wizard-stage").find("input[data-section='" + nextWizardStage + "']").prop( "checked", true );

            $('#previous-screen').removeAttr('disabled');

			if (!$('.employment-details.employee-active-section').next().next().length  > 0){
	        	$('#next-screen').attr('disabled', 'disabled');
	        	$('#next-screen').hide();

	        	$('#employee-submit').removeAttr('disabled');
	        	$('#employee-submit').show();
       	
			}            

        }


    })	

    $('#previous-screen').on('click', function (e) {
      e.preventDefault()
      
      if($('.employment-details.employee-active-section').prev().length > 0 ){
             var current = $('.employment-details.employee-active-section');
             current.removeClass('employee-active-section');

            current.prev().addClass('employee-active-section');

            var currentWizardStage = current.attr('id');
            var previousWizardStage = current.prev().attr('id');

            $(".wizard-stage").find("input[data-section='" + currentWizardStage + "']").prop( "checked", false );

            $(".wizard-stage").find("input[data-section='" + previousWizardStage + "']").prop( "checked", true );

            $('#previous-screen').removeAttr('disabled'); 

        	$('#next-screen').removeAttr('disabled');
        	$('#next-screen').show();         	                  

        }

        if (!$('.employment-details.employee-active-section').prev().length  > 0){

        	$('#previous-screen').attr('disabled', 'disabled');
        	
        } 

    	$('#employee-submit').attr('disabled', 'disabled');
    	$('#employee-submit').hide();                

    })

	$("#employee-create").submit(function(e) {
	    e.preventDefault();
	    var formData = new FormData(this);    

	    $.ajax({
	        url: $(this).attr("action"),
	        type: 'POST',
	        data: formData,
	        success: function (data) {
	            if(data.status_code == '1001'){
	            	table_refresh();
	            	$('#contentModal').modal('hide');
	            	toastr.success(data.status_msg)

	            }else{
	            	toastr.error(data.status_msg)
	            }
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	});

	})
</script>      