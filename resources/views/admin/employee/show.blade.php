@extends('layouts.app')

@section('title', 'Employee')

@section('page-name', 'Employee')

@section('breadcrumbs', 'Administrator Module / Employees')


@section('content')
	<div class="col-md-12">
		<div class="row">
			<!-- <div class="col-3"> -->
				<div class="img-frame">{!! $employee->profile_picture ? "<img src='". asset('storage/uploads/employees/'. $employee->profile_picture) ."'>" : acronym($employee->other_name . ' '. $employee->surname) !!}</div>
			<!-- </div> -->

			<div class="col employee-highlight zero-left-pad">
				<h4>{{$employee->other_name . ' '. $employee->surname}}</h4>
				<p>{{ App\JobRole::find($employee->employee()->first()->job_role_id)->first()->title}}</p>
				<p>{{$employee->phone_number}}</p>
			</div>
			
		</div>	

		<div class="employee-full">		
			<div class="row">
			  <div class="col-md-2 pl-0">
			  	<dl>
			  		<dt>First Name</dt>
			  		<dd>{{$employee->other_name}}</dd>
			  	</dl>

			  </div>
			  <div class="col-md-2 px-4">
			  	<dl>
			  		<dt>Surname</dt>
			  		<dd>{{$employee->surname}}</dd>
			  	</dl>
			  </div>
			  <div class="col-md-2 px-4">
			  	<dl>
			  		<dt>Date of Birth</dt>
			  		<dd>{{ Carbon\Carbon::parse($employee->dob)->format('d / m / Y')}}</dd>
			  	</dl>	  	
			  </div>

			  <div class="col-md-2 px-4">
			  	<dl>
			  		<dt>Gender</dt>
			  		<dd>{{ $employee->gender == 'M' ? 'Male': 'Female' }}</dd>
			  	</dl>	  	
			  </div>


			</div>

			<div class="row">
			  <div class="col-md-2 pl-0">
			  	<dl>
			  		<dt>Email</dt>
			  		<dd>{{$employee->email}}</dd>
			  	</dl>

			  </div>
			  <div class="col-md-2 px-4">
			  	<dl>
			  		<dt>Start Date</dt>
			  		<dd>{{ Carbon\Carbon::parse($employee->employee()->first()->start_date)->format('d / m / Y')}}</dd>
			  	</dl>
			  </div>
			  <div class="col-md-2 px-4">
			  	<dl>
			  		<dt>Location</dt>
			  		<dd>{{App\Branches::find($employee->employee()->first()->branches_id)->first()->name}}</dd>
			  	</dl>	  	
			  </div>

			  <div class="col-md-2 px-4">
			  	<dl>
			  		<dt>Job Role</dt>
			  		<dd>{{ App\JobRole::find($employee->employee()->first()->job_role_id)->first()->title }}</dd>
			  	</dl>	  	
			  </div>


			</div>

		</div>

	</div> 


@endsection

@section('scripts')

	<script type="text/javascript">
		$(document).ready(function(){
			$('#admin-menu').addClass('active-module');
			$('#admin-employee').addClass('ext-menu-active');				
		})
	</script>	
@endsection