@extends('layouts.app')

@section('title', 'Reports')

@section('page-name', 'Dashboard')

@section('breadcrumbs', 'Admininistrator Module / Reports')


@section('content')
	<div class="col-md-12">
		<div class="row">
		  	<div class="col-md-4">
		  		<div class="form-inline">
		  		  <label class="sr-only" for="inputReportType">Report Type</label>
				  <div class="input-group mb-2 mr-sm-2">
				    <div class="input-group-prepend">
				      <div class="input-group-text">Report Type</div>
				    </div>
				    <select class="form-control" id="inputReportType">
				    	<option value="sales">Sales Summary</option>
				    	<option value="order">Order Summary</option>
				    </select>
				  </div>		  			
		  		</div>
		  	</div>
		  	<div class="col-md-8">
		  		
		  	
			  	<div class="row text-right">
			  		<div class="col">
					
					    {{Form::select('sale_main_location', $branches, null, array('class' => 'form-control mr-sm-2', "id" => "sale-main-location"))}}
			  		</div>

			  		<div class="col">
						<div class="date-range-wrap">
							<div class="date-input-ranger">
								<i class="far fa-calendar-alt"></i>
								<input class="dateinput" type="text" id="from_date" name="from_date" placeholder="Begin data" autocomplete="off"><i class="fas fa-long-arrow-alt-right"></i><i class="far fa-calendar-alt"></i>
								<input class="dateinput" type="text" id="to_date" name="to_date" placeholder="End date" autocomplete="off">
							</div>
						</div>			  			
			  		</div>
						
			  	</div>	
		  	</div>	
		</div>


  	
	  	<div id="sales-overview-chart" class="widget-wrap chart-widget"></div>

	  	<div class="row">
		  	<div class="col zero-left-pad">
		  		<div id="sales-days-chart" class="widget-wrap chart-widget"></div>
		  	</div>

		  	<div class="col zero-left-pad">
		  		<div id="sales-time-chart" class="widget-wrap chart-widget"></div>
		  	</div>		  		
	  	</div>

	  	<div class="row">
	  		<div id="sales-table" class="widget-wrap chart-widget"></div>
	  	</div>
  	

	</div> 

@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){

	$('#admin-menu').addClass('active-module');
	$('#admin-report').addClass('ext-menu-active');	

	$('.dateinput').datepicker({
		format: 'yyyy-mm-dd',		
	});	  

	var loadWidgets = function(){
		sale_main_location = $('select[name=sale_main_location]').val();
	    from_date = !isEmpty($('input[name=from_date]').val()) ? $('input[name=from_date]').val() : '';
	    to_date = !isEmpty($('input[name=to_date]').val()) ? $('input[name=to_date]').val() : '';


	    $('#sales-overview-chart').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');

		$( "#sales-overview-chart" ).load( '{{ route("admin.report.partial", ["sales",1])}}' + '/?location='+ sale_main_location + '&from_date='+ from_date + '&to_date='+to_date, function(response) {
		  
		});

	    $('#sales-days-chart').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');

		$( "#sales-days-chart" ).load( '{{ route("admin.report.partial", ["sales",2])}}' + '/?location='+ sale_main_location + '&from_date='+ from_date + '&to_date='+to_date, function(response) {
		  
		});	

	    $('#sales-time-chart').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');

		$( "#sales-time-chart" ).load( '{{ route("admin.report.partial", ["sales",3])}}' + '/?location='+ sale_main_location + '&from_date='+ from_date + '&to_date='+to_date, function(response) {
		  
		});	

	    $('#sales-table').html('<div style="width:100%;height:100%" class="lds-rolling"><div>');

		$( "#sales-table" ).load( '{{ route("admin.report.partial", ["sales",4])}}' + '/?location='+ sale_main_location + '&from_date='+ from_date + '&to_date='+to_date, function(response) {
		  
		});						


	}

	$('#sale-main-location').on('change', function(e) {
	    loadWidgets();
	}); 

	$('.dateinput').on('change', function(e) {
	    loadWidgets();
	});	

	loadWidgets();

})

		
</script>
@endsection